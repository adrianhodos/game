#pragma once

class scene_graph;

class scene_graph_node_controller {
public :
    virtual ~scene_graph_node_controller() {}

    virtual void update(scene_graph* sg, float delta) = 0;
};