#include "pch_hdr.h"
#include "wavefront_loader.h"

namespace {
    
}

int32_t tools::mesh_loaders::wavefront_loader::read_int(char** lptr) {
    int32_t i = 0;
    char tmp_buff[64];
    while (**lptr && isdigit(**lptr)) {
        tmp_buff[i++] = **lptr;
        ++*lptr;
    }
    
    tmp_buff[i] = '\0';
    return atoi(tmp_buff);
}

uint32_t tools::mesh_loaders::wavefront_loader::read_floats(char* lptr, float* data, uint32_t max_cnt) {
    char* str = lptr;
    char* context = nullptr;
    const char* const kDelims = " ";
    uint32_t i = 0;
    for (char* tok = strtok_s(str, kDelims, &context);
         tok && i < max_cnt; str = nullptr, ++i, tok = strtok_s(str, kDelims, &context))
         data[i] = static_cast<float>(atof(tok));
         
    return i;
}

void tools::mesh_loaders::wavefront_loader::parse_material_decl(char* /*lptr*/) {
}

void tools::mesh_loaders::wavefront_loader::parse_face_decl(char* lptr) {
    lptr = skip_whitespaces(++lptr);
    
    enum Parser_State {
        PS_Ok,
        PS_Error,
        PS_Vertexid,
        PS_Texcoordid,
        PS_Normalid,
        PS_FirstSlash,
        PS_SecondSlash
    };

    const int offset_array[2][3] = { 
        { 0, 0, 0 }, 
        { groups_.top().vertex_count, groups_.top().texcoord_count, groups_.top().normal_count }
    };

    enum Element_Offset {
        EO_Vertex,
        EO_Texcoord,
        EO_Normal
    };

    bool negative_indexing = false;
    
    const obj_group_t& face_grp = groups_.top();
    obj_face_t face(face_grp.group_id);
    Parser_State state = PS_Ok;
    for (; state != PS_Error && face.element_count() < 4;) {
        if (!*lptr) {
            if (state == PS_Vertexid || state == PS_Texcoordid 
                || state == PS_Normalid)
                face.increment_element_count();
            break;
        }
            
        if (isdigit(*lptr) || (*lptr == '-')) {
            int32_t sign = 1;
            if (*lptr == '-') {
                sign = -1;
                negative_indexing = true;
                ++lptr;
            }

            if (state == PS_Ok) {
                state = PS_Vertexid;
                face.add_vertex(
                    face_grp.offset_vertex + read_int(&lptr) * sign 
                    + offset_array[negative_indexing][EO_Vertex]
                );
            } else if (state == PS_FirstSlash) {
                state = PS_Texcoordid;
                face.add_texcoord(
                    face_grp.offset_texcoord + read_int(&lptr) * sign
                    + offset_array[negative_indexing][EO_Texcoord]
                );
            } else if (state == PS_SecondSlash) {
                state = PS_Normalid;
                face.add_normal(
                    face_grp.offset_normals + read_int(&lptr) * sign
                    + offset_array[negative_indexing][EO_Normal]
                    );
            } else {
                set_error("digit/sign not allowed here");
                state = PS_Error;
                return;
            }
        } else if (isspace(*lptr)) {
            lptr = skip_whitespaces(lptr);
            state = PS_Ok;
            face.increment_element_count();
        } else if (*lptr == '/') {
            ++lptr;
            if (state == PS_Vertexid) {
                state = PS_FirstSlash;
            } else if (state == PS_FirstSlash || state == PS_Texcoordid) {
                state = PS_SecondSlash;
            } else {
                set_error("/ not allowed here");
                state = PS_Error;
                return;
            }
        } else {
            set_error("token %c not allowed in face declaration", *lptr);
            state = PS_Error;
            return;
        }
    }
    
    if (face.element_count() < 2) {
        set_error("too few elements for a face (%d)", face.element_count());
        return;
    }
    
    using namespace std;
    /*reverse(begin(face.indices), begin(face.indices) + face.elements);
    reverse(begin(face.indices) + 4, begin(face.indices) + 4 + face.elements);
    reverse(begin(face.indices) + 8, begin(face.indices) + 8 + face.elements);*/
    ++groups_.top().face_count;
    faces_.push_back(face);
}

void tools::mesh_loaders::wavefront_loader::parse_vertex_decl(char* lptr) {
    lptr += 1;
    
    if (*lptr == ' ') {
        vertex3F vertex;
        if (read_floats(++lptr, vertex.e_, 3) != 3) {
            set_error("incorrect vertex element count");
            return;
        }
        /*vertex.z_ *= -1.0f;*/
        vertices_.push_back(vertex);
        ++groups_.top().vertex_count;
    } else if (*lptr == 't') {
        vertex2F texc;
        if (read_floats(++lptr, texc.e_, 2) != 2) {
            set_error("incorrect texcoord element count");
            return;
        }
        texcoords_.push_back(texc);
        ++groups_.top().texcoord_count;
    } else if (*lptr == 'n') {
        vertex3F normal;
        if (read_floats(++lptr, normal.e_, 3) != 3) {
            set_error("incorrect normal element count");
            return;
        }
        /*normal.z_ *= -1.0f;*/
        normals_.push_back(normal);
        ++groups_.top().normal_count;
    } else {
        set_error("unknown token in vertex declaration");
    }
}

void tools::mesh_loaders::wavefront_loader::parse_group_decl(char* lptr) {
    lptr = skip_whitespaces(++lptr);
    if (!*lptr) {
        set_error("group directive must be followed by a name");
        return;
    }
    
    int32_t i = 0;
    char grp_name[256];
    while (*lptr && !isspace(*lptr))
        grp_name[i++] = *lptr++;
    grp_name[i] = '\0';

    groups_.top().group_name = grp_name;
    if (groups_.top().vertex_count) {
        obj_group_t new_group(
            "default", groups_.top().group_id + 1, 
            static_cast<int32_t>(vertices_.size()), 
            static_cast<int32_t>(texcoords_.size()), 
            static_cast<int32_t>(normals_.size())
            );
        groups_.push(new_group);
    }
}

void tools::mesh_loaders::wavefront_loader::parse_line(char* line) {
    char* p = skip_whitespaces(line);
    if (!*p)
        return;
        
    switch (*p) {
    case 'v' :
        parse_vertex_decl(p);
        break;
        
    case 'f' :
        parse_face_decl(p);
        break;
        
    case 'g' :
        parse_group_decl(p);
        break;
        
    case 'm' :
        parse_material_decl(p);
        break;
        
    default :
        break;
    }
}

void tools::mesh_loaders::wavefront_loader::parse_file(const char* file_name) {
    reset_state();
    
    std::unique_ptr<FILE, file_deleter_t> fp(fopen(file_name, "r"));
    if (!fp)
        return;
    
    groups_.push(obj_group_t());
    
    char buff_line[1024];
    while (fgets(buff_line, 1024, fp.get()) && !parsing_error_) {
        char* line_ptr = trim_whitespaces(buff_line);
        parse_line(line_ptr);
    }

    if (!parsing_error_ && !groups_.top().vertex_count)
        groups_.pop();

}
