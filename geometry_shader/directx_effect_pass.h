#pragma once

#include "directx_shader.h"

class renderer;

class effect_pass {
private :
    vertex_shader_t*    vertexshader_;
    geometry_shader_t*  geometryshader_;
    pixel_shader_t*     fragmentshader_;

public :
    effect_pass(
        vertex_shader_t* vxshader = nullptr, 
        pixel_shader_t* pxshader = nullptr, 
        geometry_shader_t* gsshader = nullptr
        );

    vertex_shader_t* get_vertex_shader() const {
        return vertexshader_;
    }

    void set_vertex_shader(vertex_shader_t* vsh) {
        vertexshader_ = vsh;
    }

    geometry_shader_t* get_geometry_shader() const {
        return geometryshader_;
    }

    void set_geometry_shader(geometry_shader_t* gsh) {
        geometryshader_ = gsh;
    }

    pixel_shader_t* get_pixel_shader() const {
        return fragmentshader_;
    }

    void set_pixel_shader(pixel_shader_t* psh) {
        fragmentshader_ = psh;
    }

    void bind_to_pipeline(const renderer* R);
};