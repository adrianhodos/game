#pragma once

#include <v8/math/camera.h>
#include <v8/math/matrix4X4.h>
#include <v8/math/vector2.h>
#include <v8/math/vector3.h>

#include "camera_controller.h"
#include "keyboard_event_receiver.h"
#include "mouse_event_receiver.h"

class scene_graph;

class camera_controller_spherical_coords 
    :   public camera_controller, public keyboard_event_receiver, 
        public mouse_event_receiver {

    float                       angle_phi_;
    float                       angle_theta_;
    float                       radius_;
    float                       zoom_speed_;
    float                       rotate_speed_;
    v8::math::vector3F          lookat_point_;
    mutable bool                updated_;
    v8::math::vector2<int>      last_mouse_pos_;

    static const float          kPHIMin;
    static const float          kPHIMax;
    static const float          kZoomMin;
    static const float          kZoomMax;

    void update_cam_data() {
        const v8::math::vector3F cam_pos = v8::math::point_from_spherical_coordinates(
            radius_, angle_phi_, angle_theta_);
        cam_ptr_->look_at(cam_pos, v8::math::vector3F::unit_y, lookat_point_);
        updated_ = true;
    }

public :
    camera_controller_spherical_coords(v8::math::camera* cam = nullptr) 
        : camera_controller(cam), 
          angle_phi_(30.0f), angle_theta_(0.0f), 
          radius_(5.0f), zoom_speed_(1.5f), rotate_speed_(1.5f), 
          lookat_point_(v8::math::vector3F::zero),
          updated_(false) {}

    camera_controller_spherical_coords(
        v8::math::camera* cam, float phi, float theta, float radius, float zoom_spd,
        float rotate_spd, const v8::math::vector3F& look_at
        )
        : camera_controller(cam),
          angle_phi_(phi), angle_theta_(theta), radius_(radius), 
          zoom_speed_(zoom_spd), rotate_speed_(rotate_spd), 
          lookat_point_(look_at),
          updated_(false) {}

    void set_zoom_speed(float spd) {
        zoom_speed_ = spd;
    }

    void set_rotate_speed(float spd) {
        rotate_speed_ = spd;
    }

    void rotate_y(bool counter_clockwise) {
        const float modifier[2] = { 1.0f, -1.0f };
        const float new_val = 
            angle_phi_ + rotate_speed_ * modifier[counter_clockwise];

        if (fabs(new_val) >= kPHIMin && fabs(new_val) <= kPHIMax) {
            angle_phi_ = new_val;
            updated_ = false;
        }
    }

    void rotate_z(bool counter_clockwise) {
        float modifier[2] = { -1.0f, 1.0f };
        angle_theta_ += rotate_speed_ * modifier[counter_clockwise];
        updated_ = false;
    }

    void zoom(bool inwards) {
        float modifier[2] = { +1.0f, -1.0f };
        
        const float new_val = radius_ + zoom_speed_ * modifier[inwards];
        if (fabs(new_val) >= kZoomMin && fabs(new_val) <= kZoomMax) {
            radius_ = new_val;
            updated_ = false;
        }
    }

    void set_camera(v8::math::camera* cam) {
        cam_ptr_ = cam;
    }

    void set_angle_phi(float phi) {
        angle_phi_ = phi;
        updated_ = false;
    }

    void set_angle_theta(float theta) {
        angle_theta_ = theta;
        updated_ = false;
    }

    bool key_press_event(int key_code);

    bool key_depress_event(int) { return false; }

    bool mouse_wheel_event(int rotations, int key_flags, int xpos, int ypos);

    bool left_button_press_event( int key_flags, int xpos, int ypos );

    bool left_button_depress_event( int key_flags, int xpos, int ypos );

    bool mouse_moved_event(int key_flags, int xpos, int ypos);

    void update(scene_graph*, float /*delta*/) {
        if (!updated_)
            update_cam_data();
    }

    bool initialize();
};
