#pragma once

#include <cassert>
#include <cstdint>
#include <utility>
#include <vector>
#include <rapidjson/rapidjson.h>
#include <v8/base/scoped_pointer.h>
#include <v8/math/light.h>
#include "scene_camera.h"
#include "scene_graph_node.h"
#include "scene_light.h"

class renderer;
class resource_manager;
class MainWindow;

struct object_data_t {
    std::string             mesh;
    v8::math::vector3F      position;
    bool                    has_scale;
    float                   scale_factor;

    object_data_t() : has_scale(false) {}
};

class scene_graph {
private :
    static const uint32_t kMaxLights = 8;
    scene_light                             scene_lights_[kMaxLights];
    v8::math::light                         active_scene_lights_[kMaxLights];
    uint32_t                                light_count_;
    uint32_t                                active_light_count_;
    v8::base::scoped_ptr<scene_graph_node>  scene_root_;
    renderer*                               renderer_;
    resource_manager*                       rsman_;
    scene_camera                            cam_;

    void create_objects(const std::vector<object_data_t>& objects);

    void read_lighs_section(const rapidjson::Value& lights_section);

    void read_objects_section(const rapidjson::Value& object_section, 
                              std::vector<object_data_t>* objects);

    bool read_scene_file(const char* file_name);

    void build_lights_list();

public :
    scene_graph();

    ~scene_graph();

    scene_graph_node* get_root() {
        return v8::base::scoped_pointer_get(scene_root_);
    }

    const scene_graph_node* get_root() const {
        return v8::base::scoped_pointer_get(scene_root_);
    }

    void set_root(scene_graph_node* node) {
        scene_root_ = node;
    }

    renderer* get_renderer() {
        return renderer_;
    }

    resource_manager* get_rsmgr() const {
        return rsman_;
    }

    void initialize(renderer* r, resource_manager* rsm, MainWindow* mw);

    void update(float delta);

    void draw();

    scene_camera* get_scene_cam() {
        return &cam_;
    }

    scene_light* add_scene_light(const v8::math::light& light) {
        if (light_count_ < kMaxLights) {
            ++light_count_;
            scene_light* sl = scene_lights_ + light_count_ - 1;
            sl->set_light(light);
            sl->set_on();
            return sl;
        }
        return nullptr;
    }

    scene_light* get_scene_light(uint32_t i) {
        assert(i < light_count_);
        return scene_lights_ + i;
    }

    const scene_light* get_scene_light(uint32_t i) const {
        assert(i < light_count_);
        return scene_lights_ + i;
    }

    std::tuple<uint32_t, v8::math::light*> get_active_lights() {
        return std::make_tuple(
            active_light_count_, 
            active_light_count_ ? active_scene_lights_ : nullptr
            );
    }
};