#include "pch_hdr.h"

#include <v8/base/count_of.h>

#include "directx_renderer.h"
#include "directx_utils.h"
#include "directx_sampler.h"
#include "sampler_descriptor.h"

bool directx_sampler::initialize(
    const graphics_render_t* R,
    const sampler_descriptor_t& smp_desc
    ) {
    if (smp_handle_)
        return true;

    const D3D11_FILTER kFilterMappings[] = {
        D3D11_FILTER_MIN_MAG_MIP_POINT,
        D3D11_FILTER_MIN_MAG_POINT_MIP_LINEAR,
        D3D11_FILTER_MIN_POINT_MAG_LINEAR_MIP_POINT,
        D3D11_FILTER_MIN_POINT_MAG_MIP_LINEAR,
        D3D11_FILTER_MIN_LINEAR_MAG_MIP_POINT,
        D3D11_FILTER_MIN_MAG_POINT_MIP_LINEAR,
        D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT,
        D3D11_FILTER_MIN_MAG_MIP_LINEAR,
        D3D11_FILTER_ANISOTROPIC
    };

    const D3D11_TEXTURE_ADDRESS_MODE kAddressModeMappings[] = {
        D3D11_TEXTURE_ADDRESS_WRAP,
        D3D11_TEXTURE_ADDRESS_MIRROR,
        D3D11_TEXTURE_ADDRESS_CLAMP,
        D3D11_TEXTURE_ADDRESS_BORDER,
        D3D11_TEXTURE_ADDRESS_MIRROR_ONCE
    };

    const D3D11_COMPARISON_FUNC kFuncCmpMappings[] = {
        D3D11_COMPARISON_NEVER,
        D3D11_COMPARISON_LESS,
        D3D11_COMPARISON_EQUAL,
        D3D11_COMPARISON_LESS_EQUAL,
        D3D11_COMPARISON_GREATER,
        D3D11_COMPARISON_NOT_EQUAL,
        D3D11_COMPARISON_GREATER_EQUAL,
        D3D11_COMPARISON_ALWAYS
    };

    D3D11_SAMPLER_DESC sampler_desc = {
        kFilterMappings[smp_desc.sdt_filter_type],
        kAddressModeMappings[smp_desc.sdt_texture_address_u],
        kAddressModeMappings[smp_desc.sdt_texture_address_v],
        kAddressModeMappings[smp_desc.sdt_texture_address_w],
        smp_desc.sdt_mip_lod_bias,
        smp_desc.sdt_max_anisotropy_level,
        kFuncCmpMappings[smp_desc.sdt_compare_func],
        { 
            smp_desc.sdt_border_color.r_, smp_desc.sdt_border_color.g_,
            smp_desc.sdt_border_color.b_, smp_desc.sdt_border_color.a_
        },
        smp_desc.sdt_min_lod,
        smp_desc.sdt_max_lod
    };

    HRESULT ret_code;
    CHECK_D3D(
        &ret_code,
        R->get_device()->CreateSamplerState(
            &sampler_desc, 
            v8::base::scoped_pointer_get_impl(smp_handle_)));

    return ret_code == S_OK;
}