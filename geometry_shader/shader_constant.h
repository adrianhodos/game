#pragma once

#include <cstdint>
#include <v8/math/matrix4X4.h>
#include <v8/math/vector2.h>
#include <v8/math/vector3.h>
#include <v8/math/vector4.h>

struct ID3D11SamplerState;
struct ID3D11ShaderResourceView;

enum Type {
    Type_Maxtrix4X4,
    Type_Float,
    Type_Vector2,
    Type_Vector3,
    Type_Vector4,
    Type_Sampler,
    Type_ShaderResource
};

template<typename T, int32_t val>
struct Type2Int;

#define MAP_TYPE_TO_INTEGER(Type, IntegerVal)   \
    template<>  \
    struct Type2Int<Type, IntegerVal> { \
        enum { \
            value = IntegerVal \
        }; \
    };

MAP_TYPE_TO_INTEGER(v8::math::matrix_4X4F, Type_Maxtrix4X4)
MAP_TYPE_TO_INTEGER(float, Type_Float)
MAP_TYPE_TO_INTEGER(v8::math::vector2F, Type_Vector2)
MAP_TYPE_TO_INTEGER(v8::math::vector3F, Type_Vector3)
MAP_TYPE_TO_INTEGER(v8::math::vector4F, Type_Vector4)
MAP_TYPE_TO_INTEGER(ID3D11SamplerState*, Type_Sampler)
MAP_TYPE_TO_INTEGER(ID3D11ShaderResourceView*, Type_ShaderResource)

enum Bind_Type {
    Bind_Vertex_Shader = 1 << 0,
    Bind_Pixel_Shader = 1 << 1,
    Bind_Geometry_Shader = 1 << 2
};

struct shader_constant {
    union {
        float                           u_mat4x4[16];
        float                           u_fscalar;
        float                           u_vector2[2];
        float                           u_vector3[3];
        float                           u_vector4[4];
        ID3D11SamplerState*             u_sampler;
        ID3D11ShaderResourceView*       u_shrsv;
    };

    Type        u_type;
    uint32_t    u_size;
    uint32_t    u_bindtype;
    char        u_name[32];

    shader_constant() {}

    void set_value(const v8::math::matrix_4X4F& mtx) {
        memcpy(u_mat4x4, &mtx, sizeof(mtx.elements_));
    }

    void set_value(float f) {
        u_fscalar = f;
    }

    void set_value(const v8::math::vector2F& v2f) {
        memcpy(u_vector2, v2f.elements_, sizeof(u_vector2));
    }

    void set_value(const v8::math::vector3F& v3f) {
        memcpy(u_vector3, v3f.elements_, sizeof(u_vector3));
    }

    void set_value(const v8::math::vector4F& v4f) {
        memcpy(u_vector4, v4f.elements_, sizeof(u_vector4));
    }

    void set_value(ID3D11SamplerState* sstate) {
        u_sampler = sstate;
    }

    void set_value(ID3D11ShaderResourceView* rsv) {
        u_shrsv = rsv;
    }

    template<typename T>
    void set_value(const T& cval) {
        u_type = Type2Int<T>::value;
        u_size = sizeof(T);
        set_value(cval);
    }    

    void set_name(const char* name) {
        strncpy_s(u_name, name, 32);
    }

    void set_bind_type(uint32_t bind_type) {
        u_bindtype = bind_type;
    }
};
