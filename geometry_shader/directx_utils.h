#pragma once

#include <d3d11.h>
#include <dxerr.h>

#include <v8/base/debug_helpers.h>
#include <v8/base/scoped_pointer.h>

#ifndef CHECK_D3D
#define CHECK_D3D(ret_code_ptr, call_and_args)  \
    do {                                         \
        *(ret_code_ptr) = (call_and_args);              \
        if (FAILED(*(ret_code_ptr))) {                  \
            const wchar_t* errString = ::DXGetErrorString(*(ret_code_ptr)); \
            v8::base::debug::output_debug_string(__WFILE__, __LINE__, \
                                L"Call %s failed, HRESULT %#08x," \
                                L"error string %s", \
                                L#call_and_args, *(ret_code_ptr), \
                                errString ? errString : L"no aditional info"); \
        } \
    } while (0)
#endif


v8::base::scoped_ptr<ID3D10Blob, v8::base::com_storage>
compile_shader_into_bytecode(
    const char* fileName, 
    const char* entryPoint,
    const char* targetProfile, 
    unsigned int compileFlags
    );