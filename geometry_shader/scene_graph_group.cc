#include "pch_hdr.h"
#include "scene_graph_group.h"

scene_graph_group::scene_graph_group(
    const std::string& name, 
    scene_graph_node* parent /* = nullptr */
    )
    : scene_graph_node(parent), name_(name) {}