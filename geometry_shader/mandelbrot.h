//#pragma once
//
//#include <d3d11.h>
//
//#include <v8/base/scoped_pointer.h>
//#include <v8/math/color.h>
//#include <v8/math/matrix3X3.h>
//
//#include "index_buffer.h"
//#include "vertex_buffer.h"
//#include "shader.h"
//
//class directx_renderer;
//struct draw_context_t;
//
//class fractal {
//private :
//    vertex_buffer                                                   vx_buffer_;
//    index_buffer16_t                                                idx_buffer_;
//    vertex_shader                                                   vx_shader_;
//    pixel_shader                                                    px_shader_;
//    v8::base::scoped_ptr<ID3D11InputLayout, v8::base::com_storage>  in_layout_;
//    v8::base::scoped_ptr<ID3D11DepthStencilState, v8::base::com_storage> dss_less_eq_;
//    v8::base::scoped_ptr<ID3D11Texture1D, v8::base::com_storage>    color_map_;
//    v8::base::scoped_ptr<ID3D11ShaderResourceView, v8::base::com_storage> rsview_colormap_;
//    float                                                           screen_width_;
//    float                                                           screen_height_;
//    unsigned                                                        iter_max_;
//    v8::math::matrix_3X3F                                           world_scale_matrix_;
//    float                                                           scale_factor_;                                  
//
//    bool initialize_color_map(const directx_renderer* rnd);
//
//    v8::math::color rgb_from_wavelength(float wave);
//
//public :
//    fractal();
//
//    bool initialize(const directx_renderer* rnd, float sw, float sh);
//
//    void draw(const draw_context_t* draw_context, const directx_renderer* rnd);
//
//    void update(float delta);
//
//    void set_width(float width) {
//        screen_width_ = width;
//    }
//
//    void set_height(float height) {
//        screen_height_ = height;
//    }
//
//    void set_max_iterations(unsigned iter_max) {
//        iter_max_ = iter_max;
//    }
//
//    void canvas_resized(float new_width, float new_height) {
//        screen_width_ = new_width;
//        screen_height_ = new_height;
//        world_scale_matrix_.make_scale(screen_width_ / 2, screen_height_ / 2, 1.0f);
//    }
//
//    void update_scale_factor(float mul_value) {
//        scale_factor_ *= mul_value;
//    }
//};