#pragma once

#include "geometric_object.h"
#include "vertex_formats.h"

class effect_pass_t;

class IFS_Mesh : public geometric_mesh<vertex_PN> {
private :
    typedef geometric_mesh<vertex_PN> base_t;
public :
    IFS_Mesh();

    virtual ~IFS_Mesh();

    virtual bool load_from_file(
        const graphics_render_t* R, 
        const char* file_name,
        effect_pass_t* effpass
        );
};