#pragma once

#include <string>
#include "scene_graph_node.h"

class scene_graph_group : public scene_graph_node {
private :
    std::string name_;
protected :
    void own_predraw(scene_graph*) {}

    void own_draw(scene_graph*) {};

    void own_post_draw(scene_graph*) {}

public :
    scene_graph_group(
        const std::string& name, 
        scene_graph_node* parent = nullptr
        );

    const std::string& get_name() const {
        return name_;
    }
};