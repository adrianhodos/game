#pragma once

#include <D3D11.h>
#include <hash_map>
#include <hash_set>
#include <string>
#include <v8/base/compiler_quirks.h>
#include <v8/base/scoped_pointer.h>

#include "app_specific.h"
#include "directx_shader.h"
#include "directx_rasterizer_state.h"
#include "sampler_descriptor.h"
#include "index_buffer.h"
#include "input_layout.h"
#include "material.h"
#include "mesh.h"
#include "spooky_hasher.h"
#include "vertex_buffer.h"

class renderer;
struct vertex_PN;
class effect_pass;

struct mtl_entry_t {
    ID3D11ShaderResourceView*       mtl_map;
    float                           mtl_specpow;

    mtl_entry_t(ID3D11ShaderResourceView* rsv = nullptr, float spec_pow = 0.0f) 
        : mtl_map(rsv), mtl_specpow(spec_pow) {}
};

namespace internals {
    template<typename com_interface>
    struct scoped_com_t {
        typedef v8::base::scoped_ptr<com_interface, v8::base::com_storage> type;
    };
}

class resource_manager {
private :
    NO_CC_ASSIGN(resource_manager);

    typedef v8::base::scoped_ptr<vertex_buffer>         scoped_vertexbuffer_t;
    typedef v8::base::scoped_ptr<index_buffer32_t>      scoped_ibuff32_t;

    struct shader_descriptor_t {
        int32_t     shader_type;
        uint32_t    compile_flags;
        int32_t     profile;
        std::string source_file;
        std::string entry_pt;
    };

    typedef v8::base::scoped_ptr<vertex_shader_t>       scoped_vsh_t;
    typedef v8::base::scoped_ptr<geometry_shader_t>     scoped_gsh_t;
    typedef v8::base::scoped_ptr<pixel_shader_t>        scoped_psh_t;

    typedef std::hash_map<std::string, scoped_vsh_t>    vertexshader_cache_t;
    typedef std::hash_map<std::string, scoped_gsh_t>    geometryshader_cache_t;
    typedef std::hash_map<std::string, scoped_psh_t>    pixelshader_cache_t;

    struct scoped_meshdata_t {
        scoped_vertexbuffer_t   vbuff_;
        scoped_ibuff32_t        ibuff_;

        scoped_meshdata_t(
            scoped_vertexbuffer_t&& rv_buff, 
            scoped_ibuff32_t&& rv_ibuff
            )
            :   vbuff_(std::move(rv_buff)),
                ibuff_(std::move(rv_ibuff)) {}                

        scoped_meshdata_t(scoped_meshdata_t&& rval)
            :   vbuff_(std::move(rval.vbuff_)),
                ibuff_(std::move(rval.ibuff_)) {}
    };

    struct stored_mesh_t {
        stored_mesh_t(vertex_buffer&& vb, index_buffer32_t&& ib)
            : vbuff(std::move(vb)), ibuff(std::move(ib)) {}

        vertex_buffer       vbuff;
        index_buffer32_t    ibuff;
    };

    typedef std::hash_map<std::string, scoped_meshdata_t>   mesh_cache_t;
    typedef mesh_cache_t::value_type                        cached_mesh_t;    

    typedef v8::base::scoped_ptr<input_layout>              scoped_layout_t;
    typedef std::hash_map<
        std::string, scoped_layout_t>                       layout_cache_t;
    typedef layout_cache_t::value_type                      cached_layout_data_t;

    //
    // materials/textures
    typedef std::hash_map<std::string, mtl_entry_t> mtl_hash_map_t;

    typedef internals::scoped_com_t<ID3D11ShaderResourceView>::type
        scoped_tex2D_t;

    typedef std::hash_map<std::string, scoped_tex2D_t>      texture2D_hash_map_t;
    typedef texture2D_hash_map_t::value_type                cached_tex2D_t;

    //
    // raster states
    typedef internals::scoped_com_t<ID3D11RasterizerState>::type
        scoped_rsstate_t;

    typedef std::hash_map<
        raster_state_info_t,
        scoped_rsstate_t,
        spooky_hasher_t<raster_state_info_t, std::less<raster_state_info_t>>
    > raster_states_table_t;

    typedef raster_states_table_t::value_type               cached_rsstate_t;

    //
    // Sampler states
    typedef internals::scoped_com_t<ID3D11SamplerState>::type
        scoped_sampler_state_t;

    typedef std::hash_map<
        sampler_descriptor_t,
        scoped_sampler_state_t,
        spooky_hasher_t<sampler_descriptor_t, std::less<sampler_descriptor_t>>
    > sampler_state_table_t;

    typedef sampler_state_table_t::value_type               cached_samplerstate_t;

    typedef v8::base::scoped_ptr<effect_pass>               scoped_effect_t;
    typedef std::hash_map<std::string, scoped_effect_t>     effect_cache_t;
    typedef effect_cache_t::value_type                      cached_effect_t;

    vertexshader_cache_t                                    vsh_cache_;
    geometryshader_cache_t                                  gsh_cache_;
    pixelshader_cache_t                                     psh_cache_;

    mesh_cache_t                                            meshes_;
    layout_cache_t                                          layouts_;

    mtl_hash_map_t                                          mtl_cache_;
    material_t                                              default_mtl_;
    texture2D_hash_map_t                                    tex2D_cache_;

    raster_states_table_t                                   rsstate_tbl_;
    sampler_state_table_t                                   sstate_tbl_;

    effect_cache_t                                          effect_tbl_;

    renderer*                                               renderer_;
    

    bool load_shader_resource_description(
        const char* shader_name, 
        shader_descriptor_t* shd
        );

    template<typename shader_type, typename res_cache>
    shader_type* create_and_cache_shader_resource(
        const char* name,
        const shader_descriptor_t& shd,
        res_cache* cache
        );

    template<typename shader_type, typename res_cache>
    shader_type* get_shader_resource(const char* name, res_cache* rc);

    mesh_info_t load_and_cache_obj_mesh(
        const char* mesh_name,
        const std::string& file_name
        );

    mesh_info_t load_and_cache_ifs_mesh(
        const char* mesh_name,
        const std::string& file_name
        );

    mesh_info_t create_mesh_entry(
        const char* mesh_name,
        const vertex_PN* vertices,
        uint32_t vertices_count,
        const uint32_t* indices,
        uint32_t indices_count
        );

    material_t load_and_cache_material(
        const std::string& mat_name
        );

    ID3D11ShaderResourceView* get_texture2D(
        const std::string& tex_name
        );

public :
    resource_manager();

    ~resource_manager();

    bool initialize(renderer* rd);

    mesh_info_t             get_mesh(const char* mesh_name);

    input_layout*           get_inputlayout(const char* name);

    vertex_shader_t*        get_vertexshader(const char* name) {
        return get_shader_resource<vertex_shader_t>(name, &vsh_cache_);
    }

    geometry_shader_t*      get_geometryshader(const char* name) {
        return get_shader_resource<geometry_shader_t>(name, &gsh_cache_);
    }

    pixel_shader_t*         get_pixelshader(const char* name) {
        return get_shader_resource<pixel_shader_t>(name, &psh_cache_);
    }

    material_t get_material(
        const std::string& mat_name
        );

    ID3D11RasterizerState* get_raster_state(
        const raster_state_info_t& rstate_params
        );

    ID3D11SamplerState* get_sampler_state(
        const sampler_descriptor_t& sstate_desc
        );

    effect_pass* get_effect(
        const std::string& effect_name
        );
};

template<typename shader_type, typename res_cache>
shader_type* resource_manager::get_shader_resource(
    const char* name, res_cache* rc
    ) {
    auto itr_res = rc->find(name);
    if (itr_res != std::end(*rc)) {
        return v8::base::scoped_pointer_get(itr_res->second);
    }

    shader_descriptor_t shader_description;
    if (!load_shader_resource_description(name, &shader_description))
        return nullptr;

    return create_and_cache_shader_resource<shader_type>(name, shader_description, rc);
}

template<typename shader_type, typename res_cache>
shader_type* resource_manager::create_and_cache_shader_resource(
    const char* name,
    const shader_descriptor_t& shd,
    res_cache* cache
    ) {
    v8::base::scoped_ptr<shader_type> new_shader(new shader_type(name));

    std::string src_file;
    string_vprintf(&src_file, "%s\\%s\\%s", app_dirs::kAppAssetsDir, 
                   app_dirs::kShaderSubdir, shd.source_file.c_str());

    if (!new_shader->create_shader_from_file(
        renderer_, src_file.c_str(), shd.entry_pt.c_str(), 
        shader_type::Shader_Model(shd.profile), shd.compile_flags))
        return nullptr;

    auto itr_inserted = cache->insert(
        typename res_cache::value_type(name, std::move(new_shader)));
    return v8::base::scoped_pointer_get(itr_inserted.first->second);
}
