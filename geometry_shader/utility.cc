#include "pch_hdr.h"
#include <v8/base/count_of.h>
#include <v8/base/crt_handle_policies.h>
#include <v8/base/debug_helpers.h>
#include <v8/base/scoped_handle.h>

#include "utility.h"

void string_vprintf(
    std::string* dst,
    const char* fmt,
    ...
    ) {
    std::vector<char> tmp_buff;
    tmp_buff.resize(256);
    va_list args_ptr;
    va_start(args_ptr, fmt);

    int32_t count = -1;
    for (;;) {
        count = v8::base::vsnprintf(&tmp_buff[0], tmp_buff.size(), fmt, args_ptr);
        if (count < 0) {
            break;
        }

        if (static_cast<uint32_t>(count) >= tmp_buff.size()) {
            tmp_buff.resize(count + 1);
            continue;
        }
        break;
    }
    if (count)
        *dst = static_cast<const char*>(&tmp_buff[0]);
}

struct find_file_handle_deleter {
    void operator()(HANDLE find_handle) const {
        if (find_handle != INVALID_HANDLE_VALUE)
            ::FindClose(find_handle);
    }
};

bool find_file(const char* pattern, file_info_t* file_info) {
    WIN32_FIND_DATAA find_data;
    std::unique_ptr<void, find_file_handle_deleter> fhandle(
        FindFirstFileA(pattern, &find_data));
    if (fhandle.get() == INVALID_HANDLE_VALUE)
        return false;

    file_info->fi_file_name.assign(find_data.cFileName);
    file_info->fi_file_size = 0;
    return true;
}

bool load_and_parse_config_file(
    const std::string& file_name,
    rapidjson::Document* cfg_file
    ) {
    using namespace v8::base;

    scoped_handle<crt_file_handle> fp(fopen(file_name.c_str(), "r"));
    if (!fp) {
        OUTPUT_DBG_MSGA("Failed to open config file %s", file_name.c_str());
        return false;
    }

    std::string file_contents;
    char tmp_buff[1024];
    while (fgets(tmp_buff, count_of_array(tmp_buff), scoped_handle_get(fp)))
        file_contents.append(tmp_buff);

    if (cfg_file->Parse<0>(file_contents.c_str()).HasParseError()) {
        OUTPUT_DBG_MSGA(
            "Failed to parse config file [%s], error [%s]",
            file_name.c_str(), cfg_file->GetParseError());
        return false;
    }

    return true;
}