#include "pch_hdr.h"
#include "renderer.h"
#include "resource_manager.h"
#include "scene_graph.h"

#include "app_globals_handler.h"

struct app_globals::private_impl_t {
    renderer            renderer_inst_;
    resource_manager    rsmgr_inst_;
    scene_graph         sgraph_inst_;

    private_impl_t() : renderer_inst_(), rsmgr_inst_()  {}
};

app_globals::app_globals() : pimpl_(new private_impl_t()) {}

app_globals::~app_globals() {}

renderer* app_globals::get_rnd() const {
    return &pimpl_->renderer_inst_;
}

resource_manager* app_globals::get_rsm() const {
    return &pimpl_->rsmgr_inst_;
}

scene_graph* app_globals::get_sgraph() const {
    return &pimpl_->sgraph_inst_;
}

app_globals* G_app;