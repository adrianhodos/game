#include "pch_hdr.h"
#include "scene_graph_node.h"

scene_graph_node::scene_graph_node(scene_graph_node* parent)
    : transform_dirty_flag_(false), parent_(parent) {}

scene_graph_node::~scene_graph_node() {}

void scene_graph_node::update(scene_graph* s, float delta_ms) {
    if (parent_ && parent_->transform_cache_dirty()) {
        transform_dirty_flag_ = true;
    }

    if (transform_cache_dirty()) {
        if (parent_)
            world_transform_ = parent_->get_world_transform() * local_transform_;
        else
            world_transform_ = local_transform_;
    }

    std::for_each(std::begin(children_), std::end(children_), 
                  [s, delta_ms](child_ptr_t& pchild) {
        pchild->update(s, delta_ms);
    });
    transform_dirty_flag_ = false;
}

void scene_graph_node::pre_draw(scene_graph* s) {
    own_predraw(s);
    std::for_each(std::begin(children_), std::end(children_), 
                  [s](child_ptr_t& pchild) {
        pchild->pre_draw(s);
    });
}

void scene_graph_node::draw(scene_graph* s) {
    own_draw(s);
    std::for_each(std::begin(children_), std::end(children_), 
                  [s](child_ptr_t& pchild) {
        pchild->draw(s);
    });
}

void scene_graph_node::post_draw(scene_graph* s) {
    own_post_draw(s);
    std::for_each(std::begin(children_), std::end(children_), 
                  [s](child_ptr_t& pchild) {
        pchild->post_draw(s);
    });
}

void scene_graph_node::remove_child(scene_graph_node* child) {
    using namespace std;
    auto child_itr = find(begin(children_), end(children_), child);
    if (child_itr != end(children_)) {
        children_.erase(child_itr);
    }
}

scene_graph_node* scene_graph_node::get_child_at(uint32_t i) {
    if (i >= children_.size())
        return nullptr;

    return v8::base::scoped_pointer_get(children_[i]);
}