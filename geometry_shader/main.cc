#include "pch_hdr.h"
#include "app_globals_handler.h"
#include "main_window.h"

int WINAPI wWinMain(
    __in HINSTANCE hInstance, 
    __in_opt HINSTANCE, 
    __in LPWSTR, 
    __in int) {
    {
        app_globals app_g;
        G_app = &app_g;

        MainWindow mainWindow(hInstance, 1024, 768);
        if (mainWindow.Initialize())
            mainWindow.message_loop();
    }

    return 0;
}