#pragma once

namespace app_dirs {
    const char* const kAppAssetsDir = "D:\\games\\mygame\\assets";
    const char* const kMeshesDir = "meshes";
    const char* const kTexturesDir = "textures";
    const char* const kLayoutDescDir = "descriptors";
    const char* const kConfigSubDir = "configs";
    const char* const kVertexLayoutDescSubdir = "descriptors\\layout";
    const char* const kShaderDescSubdir = "descriptors\\shaders";
    const char* const kMtlDescSubdir = "descriptors\\materials";
    const char* const kEffectDescSubdir = "descriptors\\effects";
    const char* const kObjectsDescSubdir = "descriptors\\objects";
    const char* const kShaderSubdir = "shaders";

}