#pragma once

#include <v8/math/color.h>
#include <v8/math/vector2.h>
#include <v8/math/vector3.h>

struct vertex_PN {
    enum {
        element_count = 2
    };

    v8::math::vector3F  vt_pos;
    v8::math::vector3F  vt_norm;

    typedef const D3D11_INPUT_ELEMENT_DESC (&vertex_format_descriptor_t)[element_count];

    static vertex_format_descriptor_t get_vertex_format_description() {
        static const D3D11_INPUT_ELEMENT_DESC descriptor[element_count] = {
            {
                "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,
                D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
            },
            {
                "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,
                D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
            }
        };
    }

    vertex_PN() {}

    vertex_PN(float px, float py, float pz, float nx, float ny, float nz, float, float)
        : vt_pos(px, py, pz), vt_norm(nx, ny, nz) {}
};

struct vertex_PNDS {
    enum {
        element_count = 4
    };

    v8::math::vector3F  vt_pos;
    v8::math::vector3F  vt_norm;
    v8::math::color     vt_diffuse;
    v8::math::color     vt_specular;

    typedef const D3D11_INPUT_ELEMENT_DESC (&vertex_format_descriptor_t)[element_count];

    static vertex_format_descriptor_t get_vertex_format_description() {
        static const D3D11_INPUT_ELEMENT_DESC descriptor[element_count] = {
            {
                "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 
                D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
            },
            {
                "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,
                D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
            },
            {
                "DIFFUSE", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,
                D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
            },
            {
                "SPECULAR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,
                D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
            }
        };
        return descriptor;
    }
};

struct vertex_PNT {
    enum {
        element_count = 3
    };

    v8::math::vector3F  vt_pos;
    v8::math::vector3F  vt_norm;
    v8::math::vector2F  vt_texc;

    typedef const D3D11_INPUT_ELEMENT_DESC (&vertex_format_descriptor_t)[element_count];

    static vertex_format_descriptor_t get_vertex_format_description() {
        static const D3D11_INPUT_ELEMENT_DESC descriptor[element_count] = {
            {
                "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 
                D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
            },
            {
                "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,
                D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
            },
            {
                "TEXCOORD", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,
                D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
            }
        };

        return descriptor;
    }
};
