#pragma once

#include <cstring>
#include <cstdint>
#include <limits>
#include <v8/math/color.h>

struct sampler_descriptor_t {
    enum Filter_Type {
        Filter_Type_Min_Mag_Mip_Point,
        Filter_Type_Min_Mag_Point_Mip_Linear,
        Filter_Type_Min_Point_Mag_Linear_Mip_Point,
        Filter_Type_Min_Point_Mag_Mip_Linear,
        Filter_Type_Min_Linear_Mag_Mip_Point,
        Filter_Type_Min_Linear_Mag_Point_Mip_Linear,
        Filter_Type_Min_Mag_Linear_Mip_Point,
        Filter_Type_Min_Mag_Mip_Linear,
        Filter_Type_Anisotropic
    };

    enum Texture_Address_Mode {
        Texture_Address_Wrap,
        Texture_Address_Mirror,
        Texture_Address_Clamp,
        Texture_Address_Border,
        Texture_Address_Mirror_Once
    };

    enum Comparison_Func {
        Cmp_Func_Never,
        Cmp_Func_Less,
        Cmp_Func_Equal,
        Cmp_Func_Less_Equal,
        Cmp_Func_Greater,
        Cmp_Func_Not_Equal,
        Cmp_Func_Greater_Equal,
        Cmp_Func_Pass_Always
    };

    Filter_Type             sdt_filter_type;
    Texture_Address_Mode    sdt_texture_address_u;
    Texture_Address_Mode    sdt_texture_address_v;
    Texture_Address_Mode    sdt_texture_address_w;
    float                   sdt_mip_lod_bias;
    uint32_t                sdt_max_anisotropy_level;
    Comparison_Func         sdt_compare_func;
    v8::math::color         sdt_border_color;
    float                   sdt_max_lod;
    float                   sdt_min_lod;

    sampler_descriptor_t(
        Filter_Type flt = Filter_Type_Min_Mag_Mip_Linear,
        Texture_Address_Mode address_u = Texture_Address_Clamp,
        Texture_Address_Mode address_v = Texture_Address_Clamp,
        Texture_Address_Mode address_w = Texture_Address_Clamp,
        float mip_lod_bias = 0.0f,
        uint32_t max_aniso_level = 16,
        Comparison_Func cmp_func = Cmp_Func_Never,
        const v8::math::color& border_color = v8::math::color::Black,
        float max_lod = std::numeric_limits<float>::max(),
        float min_lod = std::numeric_limits<float>::min()
        ) {
        sdt_filter_type = flt;
        sdt_texture_address_u = address_u;
        sdt_texture_address_v = address_v;
        sdt_texture_address_w = address_w;
        sdt_mip_lod_bias = mip_lod_bias;
        sdt_max_anisotropy_level = max_aniso_level;
        sdt_compare_func = cmp_func;
        sdt_border_color = border_color;
        sdt_max_lod = max_lod;
        sdt_min_lod = min_lod;
    }
};

inline bool operator<(
    const sampler_descriptor_t& left, 
    const sampler_descriptor_t& right
    ) {
    return left.sdt_filter_type < right.sdt_filter_type;
}

inline bool operator==(
    const sampler_descriptor_t& s_left, 
    const sampler_descriptor_t& s_right
    ) {
    return !memcmp(&s_left, &s_right, sizeof(sampler_descriptor_t));
}

inline bool operator!=(
    const sampler_descriptor_t& s_left, 
    const sampler_descriptor_t& s_right
    ) {
    return !(s_left == s_right);
}
