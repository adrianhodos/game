#pragma once

#include <cstdint>
#include <tuple>

struct shader_constant;

class shader_data_provider {
public :
    virtual ~shader_data_provider() {}

    virtual std::tuple<shader_constant*, uint32_t> get_data() = 0;
};