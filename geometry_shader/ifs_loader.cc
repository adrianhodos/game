#include "pch_hdr.h"
#include <v8/base/scoped_handle.h>
#include <v8/base/win32_handle_policies.h>

#include "ifs_loader.h"


bool 
tools::mesh_loaders::ifs_loader::readStringData(
    HANDLE fptr, 
    std::vector<unsigned char>* str
    ) {
    unsigned int strLen;
    DWORD bytesOut;
    if (!::ReadFile(fptr, &strLen, sizeof(strLen), &bytesOut, 0))
        return false;

    str->resize(strLen);
    return ::ReadFile(fptr, &(*str)[0], strLen, &bytesOut, 0) == TRUE;
}

bool 
tools::mesh_loaders::ifs_loader::readFloat(
    HANDLE fptr, 
    float* outVal
    ) {
    DWORD bytesRead;
    return ::ReadFile(fptr, outVal, sizeof(float), &bytesRead, 0) == TRUE;
}

bool 
tools::mesh_loaders::ifs_loader::readUint(
    HANDLE fptr, 
    unsigned int* outVal
    ) {
    DWORD bytesRead;
    return ::ReadFile(fptr, outVal, sizeof(unsigned int), &bytesRead, 0) == TRUE;
}

bool 
tools::mesh_loaders::ifs_loader::readElementCount(
    HANDLE fptr, 
    unsigned int* vertexCnt, 
    const char* hdrString
    ) {
    std::vector<unsigned char> headerStr;
    if (!readStringData(fptr, &headerStr))
        return false;

    std::string vertexHDRStr((const char*)&headerStr[0]);
    if (vertexHDRStr.compare(hdrString))
        return false;

    return readUint(fptr, vertexCnt);
}

bool 
tools::mesh_loaders::ifs_loader::readVertices(
    HANDLE fptr, 
    unsigned int howMany
    ) {
    vertexData_.reserve(howMany);
    bool failed = true;
    for (unsigned int i = 0; (i < howMany) && failed; ++i) {
        vertex_PN currentVertex;
        failed = readFloat(fptr, &currentVertex.vt_pos.x_);
        failed = readFloat(fptr, &currentVertex.vt_pos.y_);
        failed = readFloat(fptr, &currentVertex.vt_pos.z_);
        currentVertex.vt_pos.z_ *= 1.0f;
        currentVertex.vt_norm = v8::math::vector3F::zero;
        vertexData_.push_back(currentVertex);
    }
    return failed;
}

bool 
tools::mesh_loaders::ifs_loader::readFaces(
    HANDLE fptr, 
    unsigned int howMany
    ) {
    indexData_.reserve(howMany);
    bool succeeded = true;
    unsigned int currentIndex;
    for (unsigned int i = 0; (i < howMany) && succeeded; ++i) {
        succeeded = readUint(fptr, &currentIndex);
        indexData_.push_back(currentIndex);
        succeeded = readUint(fptr, &currentIndex);
        indexData_.push_back(currentIndex);
        succeeded = readUint(fptr, &currentIndex);
        indexData_.push_back(currentIndex);
    }

    return succeeded;
}

bool 
tools::mesh_loaders::ifs_loader::readIFSHeader(
    HANDLE fptr
    ) {
    std::vector<unsigned char> strBuff;
    if (!readStringData(fptr, &strBuff))
        return false;

    std::string readStr((const char*) &strBuff[0]);
    if (readStr.compare("IFS"))
        return false;

    if (!readFloat(fptr, &ifsVersion_))
        return false;

    if (!readStringData(fptr, &strBuff))
        return false;

    modelName_.assign((const char*) &strBuff[0]);
    return true;
}

void tools::mesh_loaders::ifs_loader::compute_mesh_normals() {
    for (uint32_t i = 0; i < numFaces_; ++i) {
        vertex_PN& v0 = vertexData_[indexData_[i * 3 + 0]];
        vertex_PN& v1 = vertexData_[indexData_[i * 3 + 1]];
        vertex_PN& v2 = vertexData_[indexData_[i * 3 + 2]];

        const auto e0 = v1.vt_pos - v0.vt_pos;
        const auto e1 = v2.vt_pos - v0.vt_pos;

        const auto normal = v8::math::cross_product(e0, e1);

        v0.vt_norm += normal;
        v1.vt_norm += normal;
        v2.vt_norm += normal;
    }

    for (uint32_t i = 0; i < vertexData_.size(); ++i) {
        vertexData_[i].vt_norm.normalize();
    }
}

namespace {
    struct win32_file_deleter_t {
        void operator()(HANDLE fp) const {
            if (fp != INVALID_HANDLE_VALUE)
                ::CloseHandle(fp);
        }
    };
}

bool
tools::mesh_loaders::ifs_loader::loadModel(
    const char* modelFile
    ) {
    isValid_ = false;

    std::unique_ptr<void, win32_file_deleter_t> fileFD(
        ::CreateFileA(modelFile, GENERIC_READ, 0, 0, OPEN_EXISTING, 
                      FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, 0));

    if (fileFD.get() == INVALID_HANDLE_VALUE)
        return false;

    if (!readIFSHeader(fileFD.get()))
        return false;

    //
    // read vertex count
    unsigned int elements;
    if (!readElementCount(fileFD.get(), &elements, "VERTICES"))
        return false;

    if (!readVertices(fileFD.get(), elements))
        return false;

    //
    // read face count
    if (!readElementCount(fileFD.get(), &numFaces_, "TRIANGLES"))
        return false;

    if (!readFaces(fileFD.get(), numFaces_))
        return false;


    compute_mesh_normals();
    isValid_ = true;
    return true;
}