#pragma once

#include <string>

#include "effect_pass.h"
#include "index_buffer.h"
#include "scene_graph_node.h"
#include "vertex_buffer.h"
#include <v8/base/scoped_pointer.h>

class input_layout;

class simple_mesh : public scene_graph_node {
private :
    typedef scene_graph_node    super_t;

    vertex_buffer*          vbuff_;
    index_buffer32_t*       ibuff_;
    input_layout*           input_layout_;
    effect_pass             efpass_;
    std::string             model_file_;
    std::string             layout_name_;
    v8::base::scoped_ptr<ID3D11RasterizerState, v8::base::com_storage> rstate_;

    void load_mesh_params();

protected :
    void own_predraw(scene_graph*) {}

    void own_draw(scene_graph* s);

    void own_post_draw(scene_graph*) {};

public :
    simple_mesh(
        const char* model_file, 
        const char* input_layout,
        scene_graph_node* parent = nullptr
        );

    void update(scene_graph* s, float delta_ms);

    bool initialize(scene_graph* sg);

    void set_input_layout(const std::string& name);

    input_layout* get_input_layout() const {
        return input_layout_;
    }

    const std::string get_input_layout_id() const {
        return layout_name_;
    }
};