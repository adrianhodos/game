#pragma once

#include <cstdint>
#include <string>
#include <vector>
#include <Windows.h>
#include <v8/math/vector3.h>
#include "vertex_formats.h"

namespace tools {

namespace mesh_loaders {

class ifs_loader {
public :
    float                       ifsVersion_;
    std::string                 modelName_;
    std::vector<vertex_PN>      vertexData_;
    std::vector<uint32_t>       indexData_;
    unsigned int                numFaces_;
    bool                        isValid_;

private :

    bool readStringData(HANDLE fptr, std::vector<unsigned char>* str);

    bool readFloat(HANDLE fptr, float* outVal);

    bool readUint(HANDLE fptr, unsigned int* outVal);

    bool readElementCount(HANDLE fptr, unsigned int* vertexCnt, const char* hdrString);

    bool readVertices(HANDLE fptr, unsigned int howMany);

    bool readFaces(HANDLE fptr, unsigned int howMany);

    bool readIFSHeader(HANDLE fptr);

    void compute_mesh_normals();

public :
    ifs_loader() : isValid_(false) {}

    bool loadModel(const char* modelFile);

    size_t getFaceCount() const {
        assert(isValid_);
        return numFaces_;
    }
};

} // namespace model_format_handlers

} // namespace base