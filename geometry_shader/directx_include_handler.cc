#include "pch_hdr.h"

#include <v8/base/crt_handle_policies.h>
#include <v8/base/debug_helpers.h>
#include <v8/base/scoped_handle.h>

#include "app_specific.h"
#include "directx_utils.h"
#include "utility.h"

#include "directx_include_handler.h"

HRESULT dxcompile_include_handler::Open(
    D3D10_INCLUDE_TYPE IncludeType, 
    LPCSTR pFileName, 
    LPCVOID pParentData, 
    LPCVOID *ppData, 
    UINT *pBytes
    ) {

    UNREFERENCED_PARAMETER(pParentData);
    UNREFERENCED_PARAMETER(IncludeType);

    *ppData = nullptr;
    *pBytes = 0;

    std::string include_file_path;
    string_vprintf(&include_file_path, "%s\\%s\\%s", app_dirs::kAppAssetsDir, 
                   app_dirs::kShaderSubdir, pFileName);

    using namespace v8::base;

    struct stat file_data;
    if (stat(include_file_path.c_str(), &file_data)) {
        OUTPUT_DBG_MSGA("Failed to obtain file stats for %s, compiler will fail",
                        include_file_path.c_str());
        return E_INVALIDARG;
    }

    scoped_handle<crt_file_handle> fp(fopen(include_file_path.c_str(), "r"));
    if (!fp) {
        return E_FAIL;
    }

    file_contents_ = new char[file_data.st_size + 1];
    char tmp_buff[1024];
    uint32_t offset = 0;
    while (fgets(tmp_buff, _countof(tmp_buff), scoped_handle_get(fp))) {
        uint32_t byte_count = strlen(tmp_buff);
        memcpy(scoped_pointer_get(file_contents_) + offset, tmp_buff, byte_count);
        offset += byte_count;
    }

    *ppData = scoped_pointer_get(file_contents_);
    *pBytes = offset;
    return S_OK;
}

HRESULT dxcompile_include_handler::Close(LPCVOID) {
    file_contents_ = nullptr;
    return S_OK;
}