#include "pch_hdr.h"

#include <v8/base/debug_helpers.h>

#include "directx_utils.h"
#include "index_buffer.h"
#include "renderer.h"
#include "vertex_buffer.h"

#include "renderer.h"

renderer::renderer() 
    : target_window_(nullptr), 
      target_width_(800.0f), 
      target_height_(600.0f),
      fullscreen_(false) {}
      /*rsmgr_(this) {}*/

renderer::~renderer() {
    //
    // If the swap chain runs in full screen state it must be set to windowed
    // state before destruction.
    if (swapChain_ && is_fullscreen())
        swapChain_->SetFullscreenState(false, nullptr);
}

bool renderer::initialize( 
    HWND window, 
    float width, 
    float height, 
    surface_format backbuffer_format, 
    surface_format depth_stencil_buffer_format, 
    bool full_screen /* = false */, 
    bool handle_full_screen_transition /* = false */
    ) {
    assert(!deviceRef_);
    assert(!deviceCtxRef_);
    assert(!target_window_);

    assert(window);

    target_window_ = window;
    target_width_ = width;
    target_height_ = height;
    backbuffer_format_ = backbuffer_format;
    depth_stencil_buffer_format_ = depth_stencil_buffer_format;
    
    if (!initialize_swap_chain(full_screen))
        return false;

    if (!handle_full_screen_transition)
        disable_auto_alt_enter();

    if (!initialize_font_engine())
        return false;

    return resize_render_target(width, height);
}

bool renderer::initialize_font_engine() {
    assert(deviceRef_);
    assert(deviceCtxRef_);

    using namespace v8::base;
    HRESULT ret_code = ::FW1CreateFactory(
        FW1_VERSION, scoped_pointer_get_impl(font_factory_));
    if (FAILED(ret_code)) {
        OUTPUT_DBG_MSGA("Failed to create font factory, error %#08x", ret_code);
        return false;
    }

    ret_code = font_factory_->CreateFontWrapper(
        scoped_pointer_get(deviceRef_), L"Arial", 
        scoped_pointer_get_impl(font_wrapper_));

    if (FAILED(ret_code)) {
        OUTPUT_DBG_MSGA("Failed to create font, error %#08x", ret_code);
        return false;
    }

    return true;
}

void renderer::draw_string(
    const wchar_t* text, 
    float font_size, 
    float xpos, 
    float ypos, 
    const v8::math::color& color
    ) {
    if (!font_wrapper_)
        return;

    font_wrapper_->DrawString(v8::base::scoped_pointer_get(deviceCtxRef_), 
                              text, font_size, xpos, ypos, 
                              color.to_uint32_abgr(), FW1_RESTORESTATE);
}

bool renderer::disable_auto_alt_enter() const {
    assert(swapChain_);
    using namespace v8::base;

    scoped_ptr<IDXGIFactory, com_storage> factoryPtr;
    HRESULT ret_code = swapChain_->GetParent(
        __uuidof(IDXGIFactory), 
        reinterpret_cast<void**>(scoped_pointer_get_impl(factoryPtr)));
    if (FAILED(ret_code)) {
        OUTPUT_DBG_MSGW(L"Failed to query for IDXGIFactory.");
        return false;
    }

    ret_code = factoryPtr->MakeWindowAssociation(
        target_window_, DXGI_MWA_NO_WINDOW_CHANGES);
    if (FAILED(ret_code))
        OUTPUT_DBG_MSGW(L"Failed to make window association");

    return ret_code == S_OK;
}

bool renderer::initialize_swap_chain(bool fullscreen) {
    using namespace v8::base;

    scoped_ptr<IDXGIFactory, com_storage> dxgifactory;
    HRESULT ret_code = ::CreateDXGIFactory(
        __uuidof(IDXGIFactory), 
        reinterpret_cast<void**>(scoped_pointer_get_impl(dxgifactory)));
    if (FAILED(ret_code)) {
        OUTPUT_DBG_MSGW(L"Failed to create IDXGIFactory, %#08x", ret_code);
        return false;
    }

    scoped_ptr<IDXGIAdapter, com_storage> firstAdapter;
    ret_code = dxgifactory->EnumAdapters(0, scoped_pointer_get_impl(firstAdapter));
    if (FAILED(ret_code)) {
        OUTPUT_DBG_MSGW(L"Failed to enum adapters, %#08x", ret_code);
        return false;
    }

    ret_code = S_OK;
    DXGI_MODE_DESC closestMode;
    memset(&closestMode, 0, sizeof(closestMode));
    for (unsigned int outputIdx = 0; ; ++outputIdx) {
        scoped_ptr<IDXGIOutput, com_storage> adapterOutput;
        ret_code = firstAdapter->EnumOutputs(outputIdx, 
                                             scoped_pointer_get_impl(adapterOutput));
        if (FAILED(ret_code))
            break;

        DXGI_MODE_DESC requestedMode;
        requestedMode.Format = static_cast<DXGI_FORMAT>(backbuffer_format_);
        requestedMode.Width = static_cast<UINT>(target_width_);
        requestedMode.Height = static_cast<UINT>(target_height_);
        requestedMode.RefreshRate.Numerator = 60;
        requestedMode.RefreshRate.Denominator = 1;
        requestedMode.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
        requestedMode.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;

        ret_code = adapterOutput->FindClosestMatchingMode(
            &requestedMode, &closestMode, nullptr);
        if (ret_code == S_OK)
            break;
    }

    if (FAILED(ret_code)) {
        OUTPUT_DBG_MSGW(L"No suitable mode found for %3f %3f "
                        L"DXGI_FORMAT_R8G8B8U8A8_UNORM", 
                        target_width_, target_height_);
        return false;
    }

    DXGI_SWAP_CHAIN_DESC swapChainDesc;
    swapChainDesc.BufferCount = 1;
    swapChainDesc.BufferDesc = closestMode;
    swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    swapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
    swapChainDesc.OutputWindow = target_window_;
    swapChainDesc.SampleDesc.Count = 1;
    swapChainDesc.SampleDesc.Quality = 0;
    swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
    swapChainDesc.Windowed = !fullscreen;

    const UINT kFlagsCreate = D3D11_CREATE_DEVICE_DEBUG;
    D3D_FEATURE_LEVEL outFeatLevels = D3D_FEATURE_LEVEL_11_0;

    CHECK_D3D(
        &ret_code,
        ::D3D11CreateDeviceAndSwapChain(nullptr,
                                        D3D_DRIVER_TYPE_HARDWARE,
                                        nullptr, kFlagsCreate, nullptr, 0,
                                        D3D11_SDK_VERSION, &swapChainDesc,
                                        scoped_pointer_get_impl(swapChain_),
                                        scoped_pointer_get_impl(deviceRef_),
                                        &outFeatLevels,
                                        scoped_pointer_get_impl(deviceCtxRef_)));

    return ret_code == S_OK;
}

bool renderer::resize_render_target( float new_width, float new_height ) {
    assert(swapChain_);
    assert(deviceRef_);
    assert(deviceCtxRef_);

    target_width_ = new_width;
    target_height_ = new_height;

    using namespace v8::base;
    deviceCtxRef_->OMSetRenderTargets(0, nullptr, nullptr);

    scoped_pointer_reset(renderTargetView_);
    scoped_pointer_reset(depthStencilView_);
    scoped_pointer_reset(depthStencil_);

    HRESULT ret_code;
    CHECK_D3D(
        &ret_code,
        swapChain_->ResizeBuffers(1, static_cast<UINT>(target_width_), 
                                  static_cast<UINT>(target_height_), 
                                  static_cast<DXGI_FORMAT>(backbuffer_format_), 
                                  DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH));
    if (FAILED(ret_code))
        return false;
    
    scoped_ptr<ID3D11Texture2D, com_storage> backBufferTexRef;
    ret_code = swapChain_->GetBuffer(
        0, __uuidof(ID3D11Texture2D), 
        reinterpret_cast<void**>(scoped_pointer_get_impl(backBufferTexRef)));
    if (FAILED(ret_code)) {
        OUTPUT_DBG_MSGW(L"Failed to get backbuffer reference.");
        return false;
    }

    CHECK_D3D(
        &ret_code,
        deviceRef_->CreateRenderTargetView(
            scoped_pointer_get(backBufferTexRef),
            nullptr, scoped_pointer_get_impl(renderTargetView_)));
    if (FAILED(ret_code))
        return false;

    D3D11_TEXTURE2D_DESC texDesc = {
        static_cast<UINT>(target_width_),
        static_cast<UINT>(target_height_),
        1,
        1,
        static_cast<DXGI_FORMAT>(depth_stencil_buffer_format_),
        { 1, 0 },
        D3D11_USAGE_DEFAULT,
        D3D11_BIND_DEPTH_STENCIL,
        0,
        0
    };

    CHECK_D3D(
        &ret_code,
        deviceRef_->CreateTexture2D(&texDesc, nullptr,
                                    scoped_pointer_get_impl(depthStencil_)));
    if (FAILED(ret_code))
        return false;

    D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
    depthStencilViewDesc.Format = 
        static_cast<DXGI_FORMAT>(depth_stencil_buffer_format_);
    depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    depthStencilViewDesc.Texture2D.MipSlice = 0;
    depthStencilViewDesc.Flags = 0;

    CHECK_D3D(
        &ret_code,
        deviceRef_->CreateDepthStencilView(
            scoped_pointer_get(depthStencil_), &depthStencilViewDesc, 
            scoped_pointer_get_impl(depthStencilView_)));

    if (FAILED(ret_code))
        return false;
    
    ID3D11RenderTargetView* renderTargetViews[] = { 
        scoped_pointer_get(renderTargetView_) 
    };

    deviceCtxRef_->OMSetRenderTargets(
        _countof(renderTargetViews), renderTargetViews, 
        scoped_pointer_get(depthStencilView_));

    D3D11_VIEWPORT viewPort = {
        0.0f,
        0.0f,
        target_width_,
        target_height_,
        0.0f,
        1.0f
    };

    deviceCtxRef_->RSSetViewports(1, &viewPort);
    return true;
}

void renderer::IA_Stage_set_vertex_buffers(
    const vertex_buffer* vx_buffers, 
    unsigned buffer_count
    ) const {
    assert(deviceRef_);
    assert(deviceCtxRef_);
    assert(vx_buffers);

    std::vector<ID3D11Buffer*> buffer_list;
    std::vector<uint32_t> strides;
    std::vector<uint32_t> offsets;

    buffer_list.reserve(buffer_count);
    strides.reserve(buffer_count);
    offsets.reserve(buffer_count);

    for (uint32_t i = 0; i < buffer_count; ++i) {
        buffer_list.push_back(vx_buffers[i].get_handle());
        strides.push_back(vx_buffers[i].get_element_size());
        offsets.push_back(0);
    }

    deviceCtxRef_->IASetVertexBuffers(0, buffer_list.size(), 
                                      &buffer_list[0], 
                                      &strides[0], 
                                      &offsets[0]);
}