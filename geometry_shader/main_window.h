#pragma once

#include <cstdint>

#include <v8/base/cpu_counter.h>
#include <v8/base/timers.h>

#include "cam_controller_spechical_coordinates.h"
#include "direct3dwindow.h"
#include "keyboard_event_receiver.h"
#include "mouse_event_receiver.h"
#include "renderer.h"

class scene_graph;

class fps_counter {
private :
    float       frames_per_second_;
    uint32_t    current_framecount_;
    float       elapsed_time_;

public :
    fps_counter() 
        : frames_per_second_(0.0f), current_framecount_(0), elapsed_time_(0.0f) {}

    void frame_tick(float delta_elapsed) {
        ++current_framecount_;
        elapsed_time_ += delta_elapsed;

        if (elapsed_time_ > 1000.0f) {
            frames_per_second_ = (static_cast<float>(current_framecount_) / elapsed_time_) * 1000;
            current_framecount_ = 0;
            elapsed_time_ = 0.0f;
        }
    }

    float get_fps_count() const {
        return frames_per_second_;
    }
};

struct window_stats_t {
    bool        ws_initialized;
    bool        ws_fullscreen;
    bool        ws_minimized;
    bool        ws_active;
    bool        ws_resizing;
    bool        ws_mousecaptured;
    bool        ws_quitflag;
    bool        ws_occluded;

    window_stats_t() {
        memset(this, 0, sizeof(*this));
    }
};

struct running_stats_t {
    v8::base::high_resolution_timer<float>                      rvt_timer;
    v8::base::cpu_counter                                       rvt_cpucount_;
    fps_counter                                                 rvt_fpscount_;
};

class MainWindow : public app::Direct3DWindow {
private :
    bool                                            resizing_;
    bool                                            minimized_;
    bool                                            occluded_;
    bool                                            is_fullscreen_;
    keyboard_event_receiver*                        keycatcher_;
    mouse_event_receiver*                           mousecatcher_;
    bool                                            mouse_captured_;
    window_stats_t                                  wndstats_;
    running_stats_t                                 runstats_;

    void HandleWMClose();

    void HandleWMDestroy() { ::PostQuitMessage(0); }

    void HandleWMEnterSizeMove() { wndstats_.ws_resizing = true; }

    void HandleWMExitSizeMove() { wndstats_.ws_resizing = false; }

    void HandleWMSize(WPARAM sizing_request, float newWidth, float newHeight);

    bool HandleWMKeyDown(int key_code, LPARAM l_param);

    bool HandleWMSysCommand(WPARAM w_param, LPARAM l_param);

    bool HandleWMMouseWheel(WPARAM w_param, LPARAM l_param);

    bool HandleWMLeftButtonDown(WPARAM w_param, LPARAM l_param);

    bool HandleWMLeftButtonUp(WPARAM w_param, LPARAM l_param);

    bool HandleWMMouseMoved(WPARAM w_param, LPARAM l_param);

    void HandleGetMinMaxInfo(MINMAXINFO* mmInfo) {
        mmInfo->ptMinTrackSize.x = 320;
        mmInfo->ptMinTrackSize.y = 240;
    }

    void handle_wm_activate(WPARAM w_param);

    void game_frame();

    void on_window_occluded();

    void on_app_idle();

    void game_main_loop();

    void initialize_scene();

    void draw_statistics();

protected :
    LRESULT WindowProcedure(UINT msg, WPARAM w_param, LPARAM l_param);

    void frame_present();

    void frame_draw();

    void frame_tick();

public :
    MainWindow(HINSTANCE inst, float width, float height, bool fullScreen = false);

    ~MainWindow();

    bool Initialize();

    void message_loop();

    void set_keyboard_event_receiver(keyboard_event_receiver* kev_recv) {
        keycatcher_ = kev_recv;
    }

    void set_mouse_event_receiver(mouse_event_receiver* me_recv) {
        mousecatcher_ = me_recv;
    }
};
