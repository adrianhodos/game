#pragma once

#include <string>
#include <D3D11.h>

#include <v8/config/config.h>
#include <v8/base/compiler_quirks.h>
#include <v8/base/scoped_pointer.h>

class renderer;

class input_layout {
private :
    NO_CC_ASSIGN(input_layout);
    std::string layout_name_;
    v8::base::scoped_ptr<ID3D11InputLayout, v8::base::com_storage> layout_;
public :
    typedef ID3D11InputLayout*   layout_handle_t;

    input_layout(const char* name) : layout_name_(name) {}

    bool initialize(const renderer* r);

    layout_handle_t get_handle() const {
        return v8::base::scoped_pointer_get(layout_);
    }

    const std::string& get_name() const {
        return layout_name_;
    }
};
