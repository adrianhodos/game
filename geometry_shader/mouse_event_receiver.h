#pragma once

class mouse_event_receiver {
public :
    static const int kWheelDelta = 120;

    virtual ~mouse_event_receiver() {}

    virtual bool mouse_wheel_event(
        int rotations, 
        int key_flags, 
        int xpos, 
        int ypos
        ) = 0;

    virtual bool left_button_press_event(int key_flags, int xpos, int ypos) = 0;

    virtual bool left_button_depress_event(int key_flags, int xpos, int ypos) = 0;

    virtual bool mouse_moved_event(int key_flags, int xpos, int ypos) = 0;
};
