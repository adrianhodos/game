#pragma once

#include <D3D11.h>
#include <cstdint>
#include <vector>

#include "effect_instance.h"
#include "scene_graph_node.h"
#include "shader_constant.h"
#include "shader_data_provider.h"
#include "mesh.h"

class effect_pass;
class input_layout;

class geometry_node : public scene_graph_node, public shader_data_provider {
protected :
    std::string                     node_name_;
    effect_instance                 effect_inst_;
    mesh_info_t                     mesh_;
    input_layout*                   layout_;
    std::vector<shader_constant>    constants_list_;
    int32_t                         topology_;

    void own_draw(scene_graph* s);

public :
    geometry_node(
        const std::string& name, 
        int32_t topology, 
        scene_graph_node* parent = nullptr
        );

    std::tuple<shader_constant*, uint32_t> get_data();
};