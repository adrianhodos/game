#pragma once

class effect_pass;
class shader_data_provider;
struct ID3D11RasterizerState;
class renderer;

class effect_instance {
private :
    effect_pass*            efp_;
    shader_data_provider*   data_provider_;
    ID3D11RasterizerState*  raster_state_;

public :
    effect_instance() 
        : efp_(nullptr), data_provider_(nullptr), raster_state_(nullptr) {}

    void initialize(
        effect_pass* efp, 
        shader_data_provider* dtp, 
        ID3D11RasterizerState* rstate
        ) {
        efp_ = efp;
        raster_state_ = rstate;
        data_provider_ = dtp;
    }

    void bind_to_pipeline(
        renderer* R
        );
};