#pragma once

#include "controller.h"

namespace v8 { namespace math {
    class light;
}
}

class scene_light_controller : public scene_graph_node_controller {
protected :
    v8::math::light*     controlled_light_;

    scene_light_controller(v8::math::light* controlled_light);

public :
    virtual ~scene_light_controller();

    v8::math::light* get_controlled_light() const {
        return controlled_light_;
    }

    void set_controlled_light(v8::math::light* cl) {
        controlled_light_ = cl;
    }
};
