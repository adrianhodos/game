#include "pch_hdr.h"

#include <sstream>

#include <v8/base/count_of.h>
#include <v8/base/crt_handle_policies.h>
#include <v8/base/debug_helpers.h>
#include <v8/base/scoped_handle.h>
#include <v8/base/string_util.h>
#include <v8/base/timers.h>
#include <v8/math/color.h>
#include <v8/math/camera.h>
#include <v8/math/light.h>
#include <v8/math/matrix4X4.h>
#include <v8/math/vector3.h>
#include <v8/math/vector4.h>

#include "app_globals_handler.h"
#include "direct3dwindow.h"
#include "renderer.h"
#include "resource_manager.h"
#include "scene_graph.h"
#include "simple_mesh.h"

#include "main_window.h"

namespace {
    float kZoomFactor = 0.8f;

enum ShaderType {
    ShaderType_Vertex,
    ShaderType_Geometry,
    ShaderType_Fragment
};

void D_dump_shader_constant_buffer_variable_description(
    unsigned int var_index,
    ID3D11ShaderReflectionConstantBuffer* cbuff_reflector,
    FILE* fp
    ) {
    auto variable = cbuff_reflector->GetVariableByIndex(var_index);
    if (!variable) {
        fprintf(fp, "\nFailed to get infor for variable %ud", var_index);
        return;
    }

    D3D11_SHADER_VARIABLE_DESC svd;
    HRESULT ret_code;
    CHECK_D3D(&ret_code, variable->GetDesc(&svd));
    if (FAILED(ret_code)) {
        fputs("\nFailed to query variable description", fp);
        return;
    }

    using namespace std;
    ostringstream str_desc("\n ### Constant buffer variable description ###\n");
    str_desc << "Name " << svd.Name << '\n';
    str_desc << "StartOffset " << svd.StartOffset << '\n';
    str_desc << "Size " << svd.Size << '\n';
    str_desc << "Flags " << svd.uFlags << '\n';

    fputs(str_desc.str().c_str(), fp);
}

void D_dump_constant_buffer_description(
    unsigned int index, 
    ID3D11ShaderReflection* reflector, 
    FILE* fp
    ) {
    using namespace std;

    auto cbuffer_reflection = reflector->GetConstantBufferByIndex(index);
    if (!cbuffer_reflection) {
        fprintf(fp, "\nNo constant buffer with index %ud", index);
        return;
    }

    D3D11_SHADER_BUFFER_DESC sbd;
    HRESULT ret_code;
    CHECK_D3D(&ret_code, cbuffer_reflection->GetDesc(&sbd));
    if (FAILED(ret_code)) {
        fputs("\nFailed to query constant buffer description.", fp);
        return;
    }

    ostringstream fmt_str("\n ### Constant buffer description ###\n");
    fmt_str << "Name " << sbd.Name << "\n";
    fmt_str << "Type " << sbd.Type << "\n";
    fmt_str << "Variable count " << sbd.Variables << "\n";
    fmt_str << "Size " << sbd.Size << "\n";
    fmt_str << "Flags " << sbd.uFlags << "\n";

    fprintf(fp, "%s", fmt_str.str().c_str());

    for (unsigned int i = 0; i < sbd.Variables; ++i) {
        D_dump_shader_constant_buffer_variable_description(i, cbuffer_reflection, fp);
    }
}

void D_dump_shader_bound_resource_description(
    unsigned resource_index,
    ID3D11ShaderReflection* reflector,
    FILE* fp
    )
{
    HRESULT ret_code;
    D3D11_SHADER_INPUT_BIND_DESC sibd;
    CHECK_D3D(&ret_code, reflector->GetResourceBindingDesc(resource_index, &sibd));
    if (FAILED(ret_code)) {
        fprintf(fp, "\nFailed to get bound resource description for resource index %ud",
                resource_index);
        return;
    }

    using namespace std;
    ostringstream desc_str("\n ### Bound resource description ###\n");

    const char* const kResTypeDesc[] = {
        "Constant buffer",
        "Texture buffer",
        "Texture",
        "Sampler",
        "RW Buffer",
        "Structured buffer",
        "RW structured buffer",
        "Byte address buffer",
        "RW byte address buffer",
        "Append structured buffer",
        "Consume structured buffer",
        "RW structured buffer with counter"
    };


    /*const char* const kSRVDimensionDesc[] = {
        "Unknown",
        "Buffer",
        "Texture1D",
        "Texture1DArray",
        "Texture2D",
        "Texture2DArray",
        "Texture2MS",
        "Texture2MSArray",
        "Texture3D",
        "TextureCube"
    };*/

    desc_str << "Name " << sibd.Name << '\n';
    desc_str << "Type " << kResTypeDesc[sibd.Type] << '\n';
    desc_str << "BindPoint " << sibd.BindPoint << '\n';
    desc_str << "BindCount " << sibd.BindCount << '\n';
    desc_str << "Flags " << sibd.uFlags << '\n';
    desc_str << "Return type " << sibd.ReturnType << "\n";
    desc_str << "Dimension " << sibd.Dimension << "\n";
    desc_str << "Sample count " << sibd.NumSamples << "\n";

    fputs(desc_str.str().c_str(), fp);
}

void D_dump_shader_description(
    ID3D11ShaderReflection* reflector, 
    FILE* fp
    ) {

    HRESULT ret_code;
    D3D11_SHADER_DESC shd;
    CHECK_D3D(&ret_code, reflector->GetDesc(&shd));
    
    if (FAILED(ret_code)) {
        fputs("\nFailed to query shader description.", fp);
        return;
    }

    fprintf(fp, "\n ###### Shader description ######\n");

    using namespace std;
    ostringstream fmt_str;

    fmt_str << "Version " << shd.Version << std::endl;
    fmt_str << "Creator " << shd.Creator << std::endl;
    fmt_str << "Flags " << shd.Flags << std::endl;
    fmt_str << "Constant buffers " << shd.ConstantBuffers << std::endl;
    fmt_str << "Bound resources " << shd.BoundResources << std::endl;
    fmt_str << "Input parameters " << shd.InputParameters << "\n";
    fmt_str << "Output parameters " << shd.OutputParameters << "\n";

    fprintf(fp, "%s", fmt_str.str().c_str());

    for (unsigned int i = 0; i < shd.ConstantBuffers; ++i) {
        D_dump_constant_buffer_description(i, reflector, fp);
    }

    for (unsigned int i = 0; i < shd.BoundResources; ++i) {
        D_dump_shader_bound_resource_description(i, reflector, fp);
    }
}

void D_dump_shader_info(const char* shader_file, const char* output_file) {
    using namespace v8::base;

    scoped_handle<crt_file_handle> file_ptr(fopen(shader_file, "r"));
    if (!file_ptr)
        return;

    std::string shader_code;
    char line_buff[1024];
    while (fgets(line_buff, _countof(line_buff), scoped_handle_get(file_ptr)))
        shader_code.append(line_buff);

    scoped_ptr<ID3D10Blob, com_storage> bytecode;
    scoped_ptr<ID3D10Blob, com_storage> err_msg;

    const unsigned compile_flags = D3DCOMPILE_ENABLE_STRICTNESS 
        | D3DCOMPILE_PACK_MATRIX_ROW_MAJOR | D3DCOMPILE_WARNINGS_ARE_ERRORS;

    HRESULT ret_code;
    CHECK_D3D(
        &ret_code,
        ::D3DCompile(shader_code.c_str(), shader_code.size(), nullptr, nullptr,
                     nullptr, "vs_main", "vs_4_0", compile_flags, 0,
                     scoped_pointer_get_impl(bytecode),
                     scoped_pointer_get_impl(err_msg)));
    if (FAILED(ret_code))
        return;

    scoped_ptr<ID3D11ShaderReflection, com_storage> shader_reflection;
    CHECK_D3D(
        &ret_code,
        ::D3DReflect(bytecode->GetBufferPointer(), bytecode->GetBufferSize(),
                     IID_ID3D11ShaderReflection, 
                     reinterpret_cast<void**>(scoped_pointer_get_impl(shader_reflection))));

    if (FAILED(ret_code))
        return;

    scoped_handle_reset(file_ptr, fopen(output_file, "w"));
    if (!file_ptr)
        return;

    D_dump_shader_description(scoped_pointer_get(shader_reflection), 
                              scoped_handle_get(file_ptr));
}

}

MainWindow::MainWindow(HINSTANCE inst, float width, float height, bool fs)
    : app::Direct3DWindow(inst, width, height), 
      keycatcher_(nullptr), mousecatcher_(nullptr) {
    wndstats_.ws_fullscreen = fs;
}

MainWindow::~MainWindow() {
    if (wndstats_.ws_mousecaptured)
        ::ReleaseCapture();
}

bool MainWindow::Initialize() {
    if (!Direct3DWindow::Initialize())
        return false;

    runstats_.rvt_timer.start();

    if (!G_app->get_rnd()->initialize(windowHandle_, windowWidth_, windowHeight_, 
                                      renderer::surface_fmt_R8G8A8B8UNORM, 
                                      renderer::surface_fmt_D24UNORM_S8UINT, 
                                      wndstats_.ws_fullscreen, true))
        return false;

    
    G_app->get_rnd()->set_clear_color(v8::math::color::Black);
    if (!G_app->get_rsm()->initialize(G_app->get_rnd()))
        return false;

    G_app->get_sgraph()->initialize(G_app->get_rnd(), G_app->get_rsm(), this);   
    G_app->get_sgraph()->get_scene_cam()->get_camera()->set_projection_matrix(
        v8::math::matrix_4X4F().make_perspective_projection_lh(
            windowWidth_ / windowHeight_, 45.0f, 1.0f, 1000.0f, 0.0f, 1.0f), 
        v8::math::camera::proj_type_perspective);
    
    wndstats_.ws_active = true;
    wndstats_.ws_initialized = true;

    ::ShowWindow(windowHandle_, SW_SHOW);
    ::UpdateWindow(windowHandle_);
    return true;
}

void MainWindow::draw_statistics() {
    wchar_t frame_stats[2048];
    v8::base::snwprintf(frame_stats, v8::base::count_of_array(frame_stats), 
                        L"FPS : %3.3f\nCPU usage : %3.3f",
                        runstats_.rvt_fpscount_.get_fps_count(),
                        runstats_.rvt_cpucount_.get_cpu_usage());
    G_app->get_rnd()->draw_string(frame_stats, 22.0f, 5.0f, 5.0f, 
                                  v8::math::color::LimeGreen);
}

void MainWindow::frame_draw() {
    using namespace v8::base;
    using namespace v8::math;

    if (wndstats_.ws_occluded) {
        auto present_result = G_app->get_rnd()->present_frame(
            renderer::fp_flag_present_test_frame);
        if (present_result == renderer::fp_result_window_occluded) {
            on_window_occluded();
            return;
       }
        wndstats_.ws_occluded = false;
    }

    //renderer_.restore_default_settings(); 
    //renderer_.clear_blending_state();
    G_app->get_rnd()->clear_depth_stencil();
    //renderer_.clear_raster_state();
    G_app->get_rnd()->clear_render_target();

    /*scene_graph_->draw();*/
    G_app->get_sgraph()->draw();
    draw_statistics();

    frame_present();
}

bool MainWindow::HandleWMMouseMoved(WPARAM w_param, LPARAM l_param) {
    if (!wndstats_.ws_mousecaptured)
        return false;

    return mousecatcher_ ? mousecatcher_->mouse_moved_event(
        w_param, GET_X_LPARAM(l_param), GET_Y_LPARAM(l_param))
        : false;
}

bool MainWindow::HandleWMLeftButtonDown(WPARAM w_param, LPARAM l_param) {
    ::SetCapture(windowHandle_);
    wndstats_.ws_mousecaptured = true;

    return mousecatcher_ ? 
        mousecatcher_->left_button_press_event(
            w_param, GET_X_LPARAM(l_param), GET_Y_LPARAM(l_param))
            : true;
}

bool MainWindow::HandleWMLeftButtonUp(WPARAM w_param, LPARAM l_param) {
    ::ReleaseCapture();
    wndstats_.ws_mousecaptured = false;

    return mousecatcher_ ?
        mousecatcher_->left_button_depress_event(
            w_param, GET_X_LPARAM(l_param), GET_Y_LPARAM(l_param))
            : true;
}

void MainWindow::HandleWMClose() {
    ::DestroyWindow(windowHandle_);
}

void MainWindow::frame_tick() {
    const float delta_millis = runstats_.rvt_timer.tick();
    runstats_.rvt_fpscount_.frame_tick(delta_millis);
    G_app->get_sgraph()->update(delta_millis);
}

void MainWindow::on_window_occluded() {
    on_app_idle();
}

void MainWindow::on_app_idle() {
    ::Sleep(50);
}

void MainWindow::frame_present() {
    assert(!wndstats_.ws_occluded);
    auto present_result = G_app->get_rnd()->present_frame(renderer::fp_flag_present_all);
    wndstats_.ws_occluded = (present_result == renderer::fp_result_window_occluded);
}

void MainWindow::game_frame() {
    frame_tick();
    frame_draw();
}

void MainWindow::game_main_loop() {
    if (!wndstats_.ws_initialized)
        return;
    
    if (wndstats_.ws_minimized) {
        on_app_idle();
        return;
    }

    if (wndstats_.ws_active) {
        game_frame();
        ::Sleep(2);
        return;
    } else {
        on_app_idle();
    }

    if (wndstats_.ws_quitflag) {
        ::PostMessage(windowHandle_, WM_CLOSE, 0, 0);
    }
}

void MainWindow::message_loop() {
    MSG msg = { 0 };
    
    while (msg.message != WM_QUIT && !wndstats_.ws_quitflag) {

        while (!::PeekMessage(&msg, nullptr, 0, 0, PM_NOREMOVE)) {
            game_main_loop();
        }

        do {
            int32_t ret_code = ::GetMessage(&msg, nullptr, 0, 0);
            if (ret_code == -1) {
                OUTPUT_DBG_MSGA("GetMessage() error, %d", GetLastError());
                wndstats_.ws_quitflag = true;
                break;
            }

            if (ret_code == 0)
                break;

            ::TranslateMessage(&msg);
            ::DispatchMessage(&msg);
        } while (::PeekMessage(&msg, nullptr, 0, 0, PM_NOREMOVE));
    }
}

void MainWindow::HandleWMSize(
    WPARAM sizing_request, float newWidth, float newHeight
    ) {
    windowWidth_ = newWidth;
    windowHeight_ = newHeight;

    G_app->get_sgraph()->get_scene_cam()->get_camera()->set_projection_matrix(
        v8::math::matrix_4X4F().make_perspective_projection_lh(
            windowWidth_ / windowHeight_, 45.0f, 1.0f, 1000.0f, 0.0f, 1.0f), 
        v8::math::camera::proj_type_perspective);

    switch (sizing_request) {
    case SIZE_MINIMIZED :
        wndstats_.ws_minimized = true;
        return;
        break;

    case SIZE_MAXIMIZED : case SIZE_RESTORED :
        wndstats_.ws_minimized = false;
        break;

    default :
        break;
    }

    if (!wndstats_.ws_resizing && wndstats_.ws_initialized)
        G_app->get_rnd()->resize_render_target(windowWidth_, windowHeight_);
}

void MainWindow::handle_wm_activate(WPARAM w_param) {
    const int32_t status = LOWORD(w_param);
    wndstats_.ws_active = (status == WA_ACTIVE) || (status == WA_CLICKACTIVE);
}

LRESULT MainWindow::WindowProcedure(
    UINT msg, 
    WPARAM w_param, 
    LPARAM l_param
    ) {
    using namespace std;
    tuple<bool, LRESULT> message_status(make_tuple(false, 0L));
    
    switch (msg) {
    case WM_CLOSE :
        HandleWMClose();
        return 0L;
        break;

    case WM_DESTROY :
        HandleWMDestroy();
        return 0L;
        break;

    case WM_ENTERSIZEMOVE :
        HandleWMEnterSizeMove();
        return 0L;
        break;

    case WM_EXITSIZEMOVE :
        HandleWMExitSizeMove();
        return 0L;
        break;

    case WM_SIZE :
        HandleWMSize(w_param, static_cast<float>(LOWORD(l_param)), 
                     static_cast<float>(HIWORD(l_param)));
        return 0L;
        break;

    case WM_KEYDOWN :
        message_status = make_tuple(HandleWMKeyDown(w_param, l_param), 0L);
        break;

    case WM_GETMINMAXINFO :
        HandleGetMinMaxInfo(reinterpret_cast<MINMAXINFO*>(l_param));
        return 0L;
        break;

    case WM_SYSCOMMAND :
        message_status = make_tuple(HandleWMSysCommand(w_param, l_param), 0L);
        break;

    case WM_MOUSEWHEEL :
        message_status = make_tuple(HandleWMMouseWheel(w_param, l_param), 0);
        break;

    case WM_MOUSEMOVE :
        message_status = make_tuple(HandleWMMouseMoved(w_param, l_param), 0);
        break;

    case WM_LBUTTONDOWN :
        message_status = make_tuple(HandleWMLeftButtonDown(w_param, l_param), 0);
        break;

    case WM_LBUTTONUP :
        message_status = make_tuple(HandleWMLeftButtonUp(w_param, l_param), 0);
        break;

    case WM_ACTIVATE :
        message_status = make_tuple(true, 0L);
        handle_wm_activate(w_param);
        break;

    default :
        break;
    }

    if (get<0>(message_status)) {
        return get<1>(message_status);
    }
    
    return ::DefWindowProcW(windowHandle_, msg, w_param, l_param);
}

bool MainWindow::HandleWMMouseWheel(WPARAM w_param, LPARAM l_param) {
    if (mousecatcher_)
        return mousecatcher_->mouse_wheel_event(GET_WHEEL_DELTA_WPARAM(w_param),
                                                GET_KEYSTATE_WPARAM(w_param),
                                                GET_X_LPARAM(l_param),
                                                GET_Y_LPARAM(l_param));
    return false;
}

bool MainWindow::HandleWMKeyDown(int key_code, LPARAM) {
    if (!keycatcher_)
        return false;

    return keycatcher_->key_press_event(key_code);
}

bool MainWindow::HandleWMSysCommand(WPARAM w_param, LPARAM l_param) {
    const int kReqCommand = w_param & 0xFFF0;
    if ((kReqCommand == SC_KEYMENU) && (l_param == VK_RETURN)) {
        auto ret_code = G_app->get_rnd()->set_fullscreen(!wndstats_.ws_fullscreen);
        if (!ret_code) {
            OUTPUT_DBG_MSGW(L"Failed to set swapchain to full screen, #%08x", ret_code);
        } else {
            wndstats_.ws_fullscreen = !wndstats_.ws_fullscreen;
        }
        return true;
    }

    return false;
}

void MainWindow::initialize_scene() {    
}