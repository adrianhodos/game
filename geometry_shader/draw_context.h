#pragma once

#include <d3d11.h>
#include <v8/math/light.h>
#include <v8/math/matrix4X4.h>
#include <v8/math/vector3.h>

struct draw_context_t {
    ID3D11Device*               deviceRef;
    ID3D11DeviceContext*        deviceContextRef;
    v8::math::matrix_4X4F       projViewMatrix;
    float                       dist_to_eye;
    v8::math::light             light;
    v8::math::vector3F          eye_pos;
    float                       screenWidth;
    float                       screenHeight;
};
