#pragma once

#include <d3d10shader.h>

#include <v8/base/scoped_pointer.h>

class dxcompile_include_handler : public ID3D10Include {
private :
    v8::base::scoped_ptr<char, v8::base::default_array_storage> file_contents_;

public :
    HRESULT __stdcall Open(
        D3D10_INCLUDE_TYPE IncludeType,
        LPCSTR pFileName,
        LPCVOID pParentData,
        LPCVOID *ppData,
        UINT *pBytes
    );

    HRESULT __stdcall Close(LPCVOID data_ptr);
};