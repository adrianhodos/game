#include "pch_hdr.h"
#include "effect_pass.h"
#include "ifs_mesh.h"
#include "ifs_model_loader.h"
#include "renderer.h"

namespace {

void compute_surface_normals(
    vertex_PN* model_data,
    unsigned int vertex_count,
    const unsigned* indices,
    unsigned int face_count
    ) {
    for (unsigned int i = 0; i < face_count; ++i) {
        vertex_PN& v0 = model_data[indices[i * 3]];
        vertex_PN& v1 = model_data[indices[i * 3 + 1]];
        vertex_PN& v2 = model_data[indices[i * 3 + 2]];

        const v8::math::vector3F e0 = v1.vt_pos - v0.vt_pos;
        const v8::math::vector3F e1 = v2.vt_pos - v0.vt_pos;
        const v8::math::vector3F poly_normal = v8::math::cross_product(e0, e1);

        v0.vt_norm += poly_normal;
        v1.vt_norm += poly_normal;
        v2.vt_norm += poly_normal;
    }

    for (unsigned int i = 0; i < vertex_count; ++i) {
        model_data[i].vt_norm.normalize();
    }
}

}

IFS_Mesh::IFS_Mesh() : geometric_mesh<vertex_PN>() {}

IFS_Mesh::~IFS_Mesh() {}

bool IFS_Mesh::load_from_file(
    const graphics_render_t* R, 
    const char* file_name,
    effect_pass_t* effpass
    ) {
   IFSLoader model_loader;
   if (!model_loader.loadModel(file_name))
       return false;

   using namespace std;
   vector<vertex_PN> vertices;
   vertices.reserve(model_loader.getVertexCount());
   transform(
       model_loader.getVertexListPointer(), 
       model_loader.getVertexListPointer() + model_loader.getVertexCount(),
       back_inserter(vertices),
       [](const v8::math::vector3F& vs_in) -> vertex_PN {
           vertex_PN vs_out;
           vs_out.vt_pos = vs_in;
           vs_out.vt_norm = v8::math::vector3F::zero;
           return vs_out;
   });

   compute_surface_normals(
       &vertices[0], 
       vertices.size(), 
       model_loader.getIndexListPointer(), 
       model_loader.getFaceCount()
       );

   return base_t::initialize_mesh(
       R, &vertices[0], vertices.size(),
       model_loader.getIndexListPointer(),
       model_loader.getIndexCount(),
       effpass
       );
}