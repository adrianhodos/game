#include "pch_hdr.h"

#include <v8/base/crt_handle_policies.h>
#include <v8/base/debug_helpers.h>
#include <v8/base/scoped_handle.h>

#include "app_specific.h"
#include "directx_renderer.h"
#include "directx_utils.h"
#include "utility.h"

#include "directx_input_layout.h"

bool input_layout::initialize(const renderer* r) {
    if (layout_)
        return true;

    const char* const kDescriptorFileName = "layout_format_descriptors.json";
    std::string config_file_path;
    string_vprintf(&config_file_path, "%s\\%s\\%s", app_dirs::kAppAssetsDir, 
                   app_dirs::kVertexLayoutDescSubdir, kDescriptorFileName);

    using namespace v8::base;
    scoped_handle<crt_file_handle> fp(fopen(config_file_path.c_str(), "r"));
    if (!fp) {
        OUTPUT_DBG_MSGA("Failed to open layout description file %s", 
                        config_file_path.c_str());
        return false;
    }

    using namespace rapidjson;
    char rdbuff[65535];
    rapidjson::FileReadStream fsr(scoped_handle_get(fp), rdbuff, sizeof(rdbuff));
    rapidjson::Document desc_file;
    if (desc_file.ParseStream<0, Document::EncodingType>(fsr).HasParseError()) {
        OUTPUT_DBG_MSGA("Layout descriptor file %s, parse error %s", 
                        config_file_path.c_str(), 
                        desc_file.GetParseError());
        return false;
    }

    if (!desc_file.HasMember(layout_name_.c_str())) {
        OUTPUT_DBG_MSGA("No description for layout %s", layout_name_.c_str());
        return false;
    }

    using namespace std;
    Value& layout_info = (desc_file[layout_name_.c_str()])["layout_data"];

    std::vector<D3D11_INPUT_ELEMENT_DESC> ed;
    for (uint32_t i = 0; i < layout_info.Size(); ++i) {
        const Value& descriptor = layout_info[i];

        D3D11_INPUT_ELEMENT_DESC new_element;
        new_element.AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
        new_element.Format = static_cast<DXGI_FORMAT>(
            descriptor["dxgi_format"].GetInt());
        new_element.InputSlot = descriptor["input_slot"].GetInt();
        new_element.InputSlotClass = static_cast<D3D11_INPUT_CLASSIFICATION>(
            descriptor["input_class"].GetInt());
        new_element.InstanceDataStepRate = descriptor["step_rate"].GetInt();
        new_element.SemanticIndex = descriptor["semantic_index"].GetInt();
        new_element.SemanticName = descriptor["semantic_name"].GetString();

        ed.push_back(new_element);
    }

    std::string validation_file_path;
    string_vprintf(&validation_file_path, "%s\\%s\\%s", app_dirs::kAppAssetsDir, 
                    app_dirs::kShaderSubdir,
                    desc_file[layout_name_.c_str()]["validation_shader_file"]);
    auto bytecode = compile_shader_into_bytecode(
        validation_file_path.c_str(), "vs_main", "vs_4_1", 0);
    if (!bytecode)
        return false;

    HRESULT ret_code;
    CHECK_D3D(
        &ret_code,
        r->get_device()->CreateInputLayout(
            &ed[0], ed.size(), bytecode->GetBufferPointer(),
            bytecode->GetBufferSize(), 
            scoped_pointer_get_impl(layout_)));

    return ret_code == S_OK;
}