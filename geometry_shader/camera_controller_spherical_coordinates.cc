#include "pch_hdr.h"

#include <v8/base/count_of.h>
#include <v8/base/crt_handle_policies.h>
#include <v8/base/debug_helpers.h>
#include <v8/base/scoped_handle.h>

#include "app_specific.h"
#include "cam_controller_spechical_coordinates.h"
#include "utility.h"

const float camera_controller_spherical_coords::kPHIMin = 10.0f;
const float camera_controller_spherical_coords::kPHIMax = 170.0f;
const float camera_controller_spherical_coords::kZoomMin = 5.0f;
const float camera_controller_spherical_coords::kZoomMax = 250.0f;

bool camera_controller_spherical_coords::key_press_event(int key_code) {
    bool key_handled = true;

    switch (key_code) {
    case VK_LEFT :
        rotate_z(true);
        break;

    case VK_RIGHT :
        rotate_z(false);
        break;

    case VK_UP :
        rotate_y(false);
        break;

    case VK_DOWN :
        rotate_y(true);
        break;

    case VK_ADD :
        zoom(true);
        break;

    case VK_SUBTRACT :
        zoom(false);
        break;

    case VK_F9 :
        initialize();
        break;

    default :
        key_handled = false;
        break;
    }

    return key_handled;
}

bool camera_controller_spherical_coords::mouse_wheel_event(
    int rotations, 
    int key_flags, 
    int xpos, 
    int ypos
    ) {
    UNREFERENCED_PARAMETER(key_flags);
    UNREFERENCED_PARAMETER(xpos);
    UNREFERENCED_PARAMETER(ypos);

    zoom(rotations > 0);
    return true;
}

bool camera_controller_spherical_coords::left_button_press_event(
    int /* key_flags */, 
    int /* xpos */, 
    int /* ypos */
    ) {
    return false;
}

bool camera_controller_spherical_coords::left_button_depress_event(
    int /*key_flags*/, 
    int /*xpos*/, 
    int /*ypos*/
    ) {
    return false;
}

bool camera_controller_spherical_coords::mouse_moved_event(
    int /*key_flags*/, 
    int xpos, 
    int ypos
    ) {
    int delta_x_axis = xpos - last_mouse_pos_.x_;
    int delta_y_axis = ypos - last_mouse_pos_.y_;

    if (delta_y_axis) {
        rotate_y(delta_y_axis > 0);
        last_mouse_pos_.y_ = ypos;
    }

    if (delta_x_axis) {
        rotate_z(delta_x_axis > 0);
        last_mouse_pos_.x_ = xpos;
    }

    return true;
}

bool camera_controller_spherical_coords::initialize() {
    const char* const kCFGFileName = "cam_controller_spherical_coords.cfg.json";
    std::string config_file_path;
    string_vprintf(&config_file_path, "%s\\%s\\%s", app_dirs::kAppAssetsDir, 
                   app_dirs::kConfigSubDir, kCFGFileName);

    using namespace v8::base;
    scoped_handle<crt_file_handle> fp(fopen(config_file_path.c_str(), "r"));
    if (!fp) {
        OUTPUT_DBG_MSGA("Cannot open config file %s", config_file_path.c_str());
        return false;
    }

    char read_buffer[65535];
    rapidjson::FileReadStream frs(scoped_handle_get(fp), read_buffer, sizeof(read_buffer));

    using namespace rapidjson;
    Document config_file;
    if (config_file.ParseStream<0, Document::EncodingType>(
            frs).HasParseError()) {
        OUTPUT_DBG_MSGA("Config file %s -> parse error %s",
                        config_file_path.c_str(), config_file.GetParseError());
        return false;
    }

    updated_ = false;

    angle_theta_ = static_cast<float>(config_file["theta"].GetDouble());
    const float read_val = static_cast<float>(config_file["phi"].GetDouble());
    angle_phi_ = v8::math::clamp(read_val, kPHIMin, kPHIMax);
    radius_ = static_cast<float>(config_file["radius"].GetDouble());
    radius_ = v8::math::clamp(radius_, kZoomMin, kZoomMax);
    rotate_speed_ = static_cast<float>(config_file["rotate_speed"].GetDouble());
    zoom_speed_ = static_cast<float>(config_file["zoom_speed"].GetDouble());

    auto load_float_data_from_cfg_file = [](
        rapidjson::Document* json_doc, const char* member_name, 
        float* out_data, uint32_t max_size
        ) {
        assert(json_doc->HasMember(member_name));
        const rapidjson::Value& data = (*json_doc)[member_name];
        assert(data.IsArray());

        for (rapidjson::SizeType i = 0; i < max_size && i < data.Size(); ++i) {
            out_data[i] = static_cast<float>(data[i].GetDouble());
        }
    };

    v8::math::vector3F cam_vec_data;
    load_float_data_from_cfg_file(
        &config_file, "cam_origin", cam_vec_data.elements_, 
        v8::base::count_of_array(cam_vec_data.elements_)
        );

    cam_ptr_->set_origin(cam_vec_data);
    return true;
}