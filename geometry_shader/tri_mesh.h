#pragma once

#include <cstdint>
#include <d3d11.h>

#include <v8/base/scoped_pointer.h>
#include <v8/math/matrix4x4.h>

#include "directx_shader.h"
#include "index_buffer.h"
#include "vertex_buffer.h"

class ifs_mesh {
private :
    vertex_buffer       vtx_buffer_;
    index_buffer32_t    idx_buffer_;
    vertex_shader_t     vtx_shader_;
    pixel_shader_t      frag_shader_;
    v8::base::scoped_ptr<ID3D11InputLayout, v8::base::com_storage> input_layout_;
    v8::base::scoped_ptr<ID3D11RasterizerState, v8::base::com_storage> raster_wireframe_;

    bool setup_shaders_and_input_layout(const directx_renderer* rnd);

public :
    ifs_mesh();

    virtual ~ifs_mesh();

    virtual bool initialize_mesh(
        const directx_renderer* rnd, 
        const char* model_file
        );

    virtual bool initialize_mesh(
        const directx_renderer* rnd,
        const std::vector<vertex_PN>& vertices, 
        const std::vector<unsigned>& indices
        );

    virtual void update(
        const draw_context_t*
        );

    virtual void draw(
        const draw_context_t*, 
        const directx_renderer* R
        );
};