#pragma once

#include <cstdint>
#include <cassert>
#include <hash_map>

#include <d3d11.h>

#include <v8/base/scoped_pointer.h>
#include <v8/math/color.h>
#include <FW1FontWrapper.h>

#include "directx_index_buffer.h"
#include "directx_input_layout.h"
#include "directx_vertex_buffer.h"
#include "directx_rasterizer_state.h"

class renderer {
public :
    typedef ID3D11Device            render_device_t;
    typedef ID3D11DeviceContext     render_device_context_t;

    enum surface_format {
        surface_fmt_R8G8A8B8UNORM = DXGI_FORMAT_R8G8B8A8_UNORM,
        surface_fmt_D24UNORM_S8UINT = DXGI_FORMAT_D24_UNORM_S8_UINT
    };

    enum frame_present_result {
        fp_result_ok,
        fp_result_error,
        fp_result_window_occluded
    };

    enum frame_present_flags {
        fp_flag_present_all,
        fp_flag_present_test_frame
    };

    enum primitive_topology_type {
        Topology_Type_Undefined,
        Topology_Type_PointList,
        Topology_Type_LineList,
        Topology_Type_LineStrip,
        Topology_Type_TriangleList,
        Topology_Type_TriangleStrip,
        Topology_Type_LineList_Adjacency,
        Topology_Type_LineStrip_Adjacency,
        Topology_Type_TriangleList_Adjacency,
        Topology_Type_TriangleStrip_Adjacency
    };

private :
    v8::base::scoped_ptr<ID3D11Device, v8::base::com_storage>           deviceRef_;
    v8::base::scoped_ptr<ID3D11DeviceContext, v8::base::com_storage>    deviceCtxRef_;
    v8::base::scoped_ptr<IDXGISwapChain, v8::base::com_storage>         swapChain_;
    v8::base::scoped_ptr<ID3D11Texture2D, v8::base::com_storage>        depthStencil_;
    v8::base::scoped_ptr<ID3D11RenderTargetView, v8::base::com_storage> renderTargetView_;
    v8::base::scoped_ptr<ID3D11DepthStencilView, v8::base::com_storage> depthStencilView_;
    v8::base::scoped_ptr<IFW1Factory, v8::base::com_storage>            font_factory_;
    v8::base::scoped_ptr<IFW1FontWrapper, v8::base::com_storage>        font_wrapper_;
    HWND                                                                target_window_;
    v8::math::color                                                     clear_color_;
    float                                                               target_width_;
    float                                                               target_height_;
    bool                                                                fullscreen_;
    surface_format                                                      backbuffer_format_;
    surface_format                                                      depth_stencil_buffer_format_;

    bool disable_auto_alt_enter() const;

    bool initialize_swap_chain(bool fullscreen);

    bool initialize_font_engine();
public :
    renderer(); 

    ~renderer();

    bool initialized() const {
        return swapChain_ && deviceRef_ && deviceCtxRef_;
    }

    bool is_fullscreen() const {
        return fullscreen_;
    }

    render_device_t* get_device() const {
        return v8::base::scoped_pointer_get(deviceRef_);
    }

    render_device_context_t* get_device_context() const {
        return v8::base::scoped_pointer_get(deviceCtxRef_);
    }

    HWND get_target_window() const {
        return target_window_;
    }

    float get_target_width() const {
        return target_width_;
    }

    float get_target_height() const {
        return target_height_;
    }

    void set_clear_color(const v8::math::color& clear_color) {
        clear_color_ = clear_color;
    }

    inline bool set_fullscreen(bool fullscreen);

    bool initialize(
        HWND window, 
        float width, 
        float height, 
        surface_format backbuffer_format, 
        surface_format depth_stencil_buffer_format,
        bool full_screen = false,
        bool handle_full_screen_transition = false
        );

    bool resize_render_target(
        float new_width,
        float new_height
        );

    void IA_Stage_set_vertex_buffers(
        const vertex_buffer* vxbuffers, 
        uint32_t count
        ) const;

    template<typename index_type>
    void IA_Stage_set_index_buffer(
        const index_buffer<index_type>* ibuff, 
        unsigned offset = 0
        ) const;

    inline void IA_Stage_set_primitive_topology_type(
        primitive_topology_type topology_type
        ) const;

    inline void IA_Stage_set_input_layout(
        input_layout* il
        );

    void VS_Stage_clear_shader() const {
        assert(deviceCtxRef_);
        deviceCtxRef_->VSSetShader(nullptr, nullptr, 0);
    }

    inline void GS_Stage_clear_shader() const {
        assert(deviceCtxRef_);
        deviceCtxRef_->GSSetShader(nullptr, nullptr, 0);
    }

    inline void PS_Stage_clear_shader() const {
        assert(deviceCtxRef_);
        deviceCtxRef_->PSSetShader(nullptr, nullptr, 0);
    }

    inline void draw(
        uint32_t vertex_count,
        uint32_t first_vertex_location = 0
        ) const;

    inline void draw_indexed(
        uint32_t index_count, 
        uint32_t first_index_location = 0,
        int index_offset = 0
        ) const;

    inline void RS_stage_set_state(
        ID3D11RasterizerState* rs = nullptr
        );

    inline void reset_render_state();

    inline void clear_render_target();

    inline void clear_depth_stencil();

    inline void clear_blending_state();

    inline void clear_raster_state();

    void restore_default_settings() {
        deviceCtxRef_->ClearState();
    }

    inline frame_present_result present_frame(frame_present_flags flags);

    void draw_string(
        const wchar_t* text, 
        float font_size, 
        float xpos, 
        float ypos, 
        const v8::math::color& color
        );

    /*inline void draw_string(
        const std::string& str,
        float font_size, 
        float xpos, 
        float ypos, 
        const v8::math::color& color
        );*/
};

//inline void renderer::draw_string( 
//    const std::string& str, 
//    float font_size, 
//    float xpos, 
//    float ypos, 
//    const v8::math::color& color
//    ) {
//    draw_string(str.c_str(), font_size, xpos, ypos, color);
//}

inline bool renderer::set_fullscreen(bool fullscreen) {
    assert(swapChain_);

    HRESULT ret_code = swapChain_->SetFullscreenState(fullscreen, nullptr);
    if (SUCCEEDED(ret_code)) {
        fullscreen_ = fullscreen;
        return true;
    }

    return false;
}

inline void renderer::IA_Stage_set_input_layout(
    input_layout* il
    ) {
    deviceCtxRef_->IASetInputLayout(il->get_handle());
}

inline void renderer::draw(
    uint32_t vertex_count, 
    uint32_t first_vertex_location /* = 0 */
    ) const {
    assert(deviceCtxRef_);

    deviceCtxRef_->Draw(vertex_count, first_vertex_location);
}

inline void renderer::draw_indexed( 
    uint32_t index_count, 
    uint32_t first_index_location /* = 0 */, 
    int index_offset /* = 0 */ 
    ) const {
    assert(deviceCtxRef_);

    deviceCtxRef_->DrawIndexed(index_count, first_index_location, index_offset);
}

inline void renderer::clear_raster_state() {
    deviceCtxRef_->RSSetState(nullptr);
}

inline void renderer::reset_render_state() {
    assert(deviceCtxRef_);
    deviceCtxRef_->ClearState();
}

inline void renderer::clear_render_target() {
    assert(deviceCtxRef_);

    deviceCtxRef_->ClearRenderTargetView(
        v8::base::scoped_pointer_get(renderTargetView_), 
        clear_color_.components_
        );
}

inline void renderer::clear_blending_state() {
    const float kBlendFactors[] = { 0.0f, 0.0f, 0.0f, 0.0f };
    deviceCtxRef_->OMSetBlendState(nullptr, kBlendFactors, 0xffffffff);
}

inline void renderer::clear_depth_stencil() {
    assert(deviceCtxRef_);

    deviceCtxRef_->OMSetDepthStencilState(nullptr, 0);
    deviceCtxRef_->ClearDepthStencilView(
        v8::base::scoped_pointer_get(depthStencilView_),
        D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL,
        1.0f, 0);
}

inline renderer::frame_present_result renderer::present_frame(
    renderer::frame_present_flags flags
    ) {
    assert(swapChain_);

    const UINT kFlagsMappings[] = {
        0,
        DXGI_PRESENT_TEST
    };

    HRESULT ret_code = swapChain_->Present(0, kFlagsMappings[flags]);
    if (ret_code == S_OK)
        return fp_result_ok;

    if (ret_code == DXGI_STATUS_OCCLUDED)
        return fp_result_window_occluded;

    return fp_result_error;
}

inline void renderer::IA_Stage_set_primitive_topology_type(
    primitive_topology_type topology_type
    ) const {
    assert(deviceCtxRef_);

    static const D3D11_PRIMITIVE_TOPOLOGY kTopologyMappings[] = {
        D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED,
        D3D11_PRIMITIVE_TOPOLOGY_POINTLIST,
        D3D11_PRIMITIVE_TOPOLOGY_LINELIST,
        D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP,
        D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
        D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP,
        D3D11_PRIMITIVE_TOPOLOGY_LINELIST_ADJ,
        D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP_ADJ,
        D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST_ADJ,
        D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP_ADJ,
    };

    deviceCtxRef_->IASetPrimitiveTopology(kTopologyMappings[topology_type]);
}

template<typename index_type>
void renderer::IA_Stage_set_index_buffer(
    const index_buffer<index_type>* ibuff, 
    unsigned offset
    ) const {
    assert(deviceCtxRef_);

    deviceCtxRef_->IASetIndexBuffer(
        ibuff->get_handle(), 
        index_buffer<index_type>::index_traits_t::get_dxgi_format(), 
        offset
        );
}

inline void renderer::RS_stage_set_state(ID3D11RasterizerState* rs) {
    deviceCtxRef_->RSSetState(rs);
}