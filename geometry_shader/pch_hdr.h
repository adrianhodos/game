#pragma once

#ifndef NOMINMAX
#define NOMINMAX
#endif

#ifndef _CRT_SECURE_NO_DEPRECATE
#define _CRT_SECURE_NO_DEPRECATE
#endif

#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstddef>
#include <cstdint>
#include <sys/types.h>
#include <sys/stat.h>

#include <algorithm>
#include <fstream>
#include <functional>
#include <iterator>
#include <list>
#include <limits>
#include <memory>
#include <tuple>
#include <vector>

#pragma warning(push)
#pragma warning(disable : 4100 4611 4512)
#include <rapidjson/document.h>
#include <rapidjson/rapidjson.h>
#include <rapidjson/filewritestream.h>
#include <rapidjson/filereadstream.h>
#include <rapidjson/reader.h>
#include <rapidjson/writer.h>
#pragma warning(pop)

#include <d3d11.h>
#include <D3DX11.h>
#include <D3Dcompiler.h>
#include <D3D10.h>
#include <D3DX10.h>

#if defined(NTDDI_VERSION)
#undef NTDDI_VERSION
#endif

#define NTDDI_VERSION NTDDI_WIN7

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <Windows.h>
#include <WindowsX.h>
