#include "pch_hdr.h"
#include "directx_shader.h"

const char* const shader_traits<ID3D11VertexShader>::Profile_Strings[] = {
    "vs_3_0",
    "vs_4_0",
    "vs_4_1",
    "vs_5_0"
};

const char* const shader_traits<ID3D11GeometryShader>::Profile_Strings[] = {
    "none",
    "gs_4_0",
    "gs_4_1",
    "gs_5_0"
};

const char* const shader_traits<ID3D11PixelShader>::Profile_Strings[] = {
    "ps_3_0",
    "ps_4_0",
    "ps_4_1",
    "ps_5_0"
};

bool sampler_state::initialize( 
    const renderer* R, 
    const D3D11_SAMPLER_DESC& sam_desc
    ) {
    v8::base::scoped_pointer_reset(handle_);

    HRESULT ret_code;
    CHECK_D3D(
        &ret_code, 
        R->get_device()->CreateSamplerState(
            &sam_desc, v8::base::scoped_pointer_get_impl(handle_)));

    return ret_code == S_OK;
}

bool texture2D::initialize(
    const renderer* R, 
    const D3D11_TEXTURE2D_DESC& tex_desc, 
    const void* initial_data /* = nullptr */
    ) {
    v8::base::scoped_pointer_reset(handle_);

    D3D11_SUBRESOURCE_DATA init_data = {
        initial_data, 0, 0
    };

    HRESULT ret_code;
    CHECK_D3D(
        &ret_code,
        R->get_device()->CreateTexture2D(
            &tex_desc, &init_data, v8::base::scoped_pointer_get_impl(handle_)));

    return ret_code == S_OK;
}

void texture2D::update_data(
    const renderer* R,
    const void* data, 
    uint32_t /*data_size*/
    ) {
    assert(handle_);

    R->get_device_context()->UpdateSubresource(
        v8::base::scoped_pointer_get(handle_),
        0,
        nullptr,
        data,
        0,
        0);
}

bool shader_uniform_block_t::initialize(
    const renderer* R, 
    const D3D11_SHADER_BUFFER_DESC& buff_desc,
    uint32_t bind_point
    ) {
    assert(!handle_);
    assert(!data_storage_);

    name_ = buff_desc.Name;
    varcount_ = buff_desc.Variables;
    size_ = buff_desc.Size;
    data_dirty_ = false;
    bind_point_ = bind_point;

    D3D11_BUFFER_DESC buffer_description = {
        buff_desc.Size,
        D3D11_USAGE_DEFAULT,
        D3D11_BIND_CONSTANT_BUFFER,
        0,
        0,
        buff_desc.Size
    };

    HRESULT ret_code;
    CHECK_D3D(
        &ret_code,
        R->get_device()->CreateBuffer(&buffer_description, nullptr,
                                      v8::base::scoped_pointer_get_impl(handle_)));
    if (FAILED(ret_code))
        return false;

    v8::base::scoped_pointer_reset(
        data_storage_, 
        static_cast<char*>(malloc(buff_desc.Size * sizeof(char))));
    return true;
}

void shader_uniform_block_t::set_block_component_data(
    const void* component_data, 
    uint32_t component_offset, 
    uint32_t component_size
    ) {
    assert(component_size <= (size_ - component_offset));

    char* mem_ptr = static_cast<char*>(
        v8::base::scoped_pointer_get(data_storage_)) + component_offset;
    memcpy(mem_ptr, component_data, component_size);
    data_dirty_ = true;
}
