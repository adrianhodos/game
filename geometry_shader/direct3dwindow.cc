#include "pch_hdr.h"
#include <v8/base/debug_helpers.h>
#include "direct3dwindow.h"

namespace {
    const wchar_t* const kWindowClassName = L"Direct3DWindowClass";
}

app::Direct3DWindow::Direct3DWindow(
    HINSTANCE instance, float clientWidth, float clientHeight
    ) 
    : windowWidth_(clientWidth), windowHeight_(clientHeight),
      instanceRef_(instance), windowHandle_(nullptr) {}

app::Direct3DWindow::~Direct3DWindow() {}

LRESULT WINAPI app::Direct3DWindow::WindowProcStub(
    HWND window, UINT msg, WPARAM w_param, LPARAM l_param
    )
{
    if (msg == WM_CREATE) {
        Direct3DWindow* objPtr = static_cast<Direct3DWindow*>(
            reinterpret_cast<CREATESTRUCT*>(l_param)->lpCreateParams);
        ::SetWindowLongPtr(window, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(objPtr));
        return true;
    }

    Direct3DWindow* objPtr = reinterpret_cast<Direct3DWindow*>(
        ::GetWindowLongPtr(window, GWLP_USERDATA));

    return objPtr ? objPtr->WindowProcedure(msg, w_param, l_param) :
        ::DefWindowProc(window, msg, w_param, l_param);
}

bool app::Direct3DWindow::RegisterWindowClass() {
    WNDCLASS wc;
    wc.style         = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc   = &Direct3DWindow::WindowProcStub; 
    wc.cbClsExtra    = 0;
    wc.cbWndExtra    = 0;
    wc.hInstance     = instanceRef_;
    wc.hIcon         = LoadIcon(0, IDI_APPLICATION);
    wc.hCursor       = LoadCursor(0, IDC_ARROW);
    wc.hbrBackground = reinterpret_cast<HBRUSH>(GetStockObject(NULL_BRUSH));
    wc.lpszMenuName  = 0;
    wc.lpszClassName = kWindowClassName;

    return ::RegisterClass(&wc) != 0;
}

bool app::Direct3DWindow::Initialize() {
    assert(!windowHandle_);
    if (!RegisterWindowClass())
        return false;

    RECT windowRect = { 
        0, 0, static_cast<LONG>(windowWidth_), static_cast<LONG>(windowHeight_) 
    };

    const DWORD kWindowStyle = WS_OVERLAPPEDWINDOW;
    /*const DWORD kWindowStyle = WS_BORDER | WS_CAPTION | WS_MAXIMIZEBOX 
        | WS_MINIMIZEBOX | WS_THICKFRAME | WS_SYSMENU | WS_CLIPCHILDREN 
        | WS_CLIPSIBLINGS | WS_VISIBLE;*/
    const BOOL ret_code = ::AdjustWindowRect(&windowRect, kWindowStyle, false);
    assert(ret_code && "Failed to adjust window rectangle");

    windowHandle_ = ::CreateWindow(
        kWindowClassName, 
        L"Direct3DWindow", 
        kWindowStyle, 
        CW_USEDEFAULT, 
        CW_USEDEFAULT, 
        windowRect.right - windowRect.left, 
        windowRect.bottom - windowRect.top,
        nullptr, nullptr, instanceRef_, this
        );

    if (!windowHandle_)
        return false;
    return true;
}

