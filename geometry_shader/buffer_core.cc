#include "pch_hdr.h"
#include "renderer.h"

#include "buffer_core.h"
#include "directx_utils.h"

bool buffer_core::initialize( 
    const renderer* rnd, 
    unsigned int element_size, 
    unsigned int element_count, 
    Buffer_Type type, 
    Buffer_Usage_Flags use_flags, 
    Resource_CPU_Access cpu_access,
    const void* initial_data
    ) {
    assert(!buffer_handle_);

    const D3D11_BIND_FLAG kBindFlags[Buffer_Type_Max] = {
        D3D11_BIND_VERTEX_BUFFER,
        D3D11_BIND_INDEX_BUFFER,
        D3D11_BIND_CONSTANT_BUFFER,
        D3D11_BIND_UNORDERED_ACCESS,
        D3D11_BIND_STREAM_OUTPUT
    };

    elements_ = element_count;
    element_size_ = element_size;

    D3D11_BUFFER_DESC buff_desc = {
        element_size * element_count,
        static_cast<D3D11_USAGE>(use_flags),
        kBindFlags[type],
        cpu_access,
        0,
        element_size
    };

    D3D11_SUBRESOURCE_DATA buff_initdata = {
        initial_data, 0, 0
    };

    HRESULT ret_code;
    CHECK_D3D(
        &ret_code,
        rnd->get_device()->CreateBuffer(
            &buff_desc, &buff_initdata,
            v8::base::scoped_pointer_get_impl(buffer_handle_)));

    return ret_code == S_OK;
}

void buffer_core::update_data(
    const renderer* rnd, 
    const void* data
    ) {
    assert(buffer_handle_);
    rnd->get_device_context()->UpdateSubresource(
        v8::base::scoped_pointer_get(buffer_handle_), 0, nullptr, 
        data, 0, 0);
}