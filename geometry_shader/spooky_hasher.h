#ifndef __spooky_hasher_h__
#define __spooky_hasher_h__

#include "spooky.h"

template<typename Key_Type, typename Key_Cmp>
struct spooky_hasher_t {
    Key_Cmp     key_comparator_;

    enum {
        bucket_size = 4,
        min_buckets = 8
    };

    spooky_hasher_t() : key_comparator_() {}

    spooky_hasher_t(Key_Cmp cmp) : key_comparator_(cmp) {}

    bool operator()(const Key_Type& left, const Key_Type& right) const {
        return key_comparator_(left, right);
    }

    size_t operator()(const Key_Type& key_obj) const {
        return SpookyHash::Hash32(&key_obj, sizeof(key_obj), 0);
    }
};

#endif // !__spooky_hasher_h__