#pragma once

#include <cstdint>
#include <cstdlib>
#include <iterator>
#include <v8/base/string_util.h>

//namespace rapidjson {
//    class Document;
//}

template<typename Container_Type, typename Comparator_Type>
typename Container_Type::iterator 
binary_search_container(
    Container_Type& cont,
    Comparator_Type cmp
    ) {
    uint32_t low = 0;
    uint32_t high = cont.size();

    while (low < high) {
        const uint32_t middle = (low + high) / 2;
        const int cmp_result = cmp(cont[middle]);

        if (cmp_result < 0) {
            low = middle + 1;
        } else if (cmp_result > 0) {
            high = middle;
        } else {
            return std::begin(cont) + middle;
        }
    }

    return std::end(cont);
}

template<typename T>
T rand_number_in_interval(T min, T max) {
    float pos = (static_cast<float>(rand()) / RAND_MAX) * (max - min);
    return static_cast<T>(pos) + min;
}

void string_vprintf(
    std::string* dst,
    const char* fmt,
    ...
    );

struct file_info_t {
    uint64_t        fi_file_size;
    std::string     fi_file_name;
};

bool find_file(const char* pattern, file_info_t* file_info);

inline bool find_file(const std::string& pattern, file_info_t* file_info) {
    return find_file(pattern.c_str(), file_info);
}

bool load_and_parse_config_file(
    const std::string& name, 
    rapidjson::Document* cfg_file
    );