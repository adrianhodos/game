#include "pch_hdr.h"
#include "app_specific.h"
#include "effect_pass.h"
#include "geometry_node.h"
#include "input_layout.h"
#include "mesh.h"
#include "renderer.h"
#include "resource_manager.h"
#include "scene_graph.h"
#include "utility.h"

geometry_node::geometry_node(
    const std::string& name, 
    int32_t topology, 
    scene_graph_node* parent
    ) : scene_graph_node(parent), node_name_(name), topology_(topology) {}

void geometry_node::own_draw(scene_graph* sg) {
    renderer* R_ptr = sg->get_renderer();

    R_ptr->IA_Stage_set_vertex_buffers(mesh_.vbuff, 1);
    R_ptr->IA_Stage_set_index_buffer(mesh_.ibuff);
    R_ptr->IA_Stage_set_input_layout(layout_);
    R_ptr->IA_Stage_set_primitive_topology_type(
        (renderer::primitive_topology_type) topology_);

    effect_inst_.bind_to_pipeline(R_ptr);
    R_ptr->draw_indexed(mesh_.ibuff->get_element_count());
}

std::tuple<shader_constant*, uint32_t> geometry_node::get_data() {
    return std::make_tuple(&constants_list_[0], constants_list_.size());
}