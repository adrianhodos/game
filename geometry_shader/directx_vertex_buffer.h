#pragma once

#include "buffer_core.h"

class renderer;

class vertex_buffer : private buffer_core {
public :

    vertex_buffer() : buffer_core() {}

    vertex_buffer(vertex_buffer&& rval) {
        buffer_core::buffer_core(std::move(rval));
    }

    vertex_buffer& operator=(vertex_buffer&& rval) {
        buffer_core::operator=(std::move(rval));
        return *this;
    }

    vertex_buffer(
        const renderer* rnd, 
        unsigned int element_size, 
        unsigned int elements, 
        const void* initial_data = nullptr, 
        Buffer_Usage_Flags use_flags = Buffer_Use_GPU_RDONLY, 
        Resource_CPU_Access cpu_access = Resource_CPU_NoAccess
        ) : buffer_core(rnd, element_size, elements, Buffer_Type_Vertex, 
                        use_flags, cpu_access, initial_data) {
    }

    bool initialize(
        const renderer* rnd, 
        unsigned int element_size, 
        unsigned int elements, 
        const void* initial_data = nullptr, 
        Buffer_Usage_Flags use_flags = Buffer_Use_GPU_RDONLY, 
        Resource_CPU_Access cpu_access = Resource_CPU_NoAccess
        ) {
        return buffer_core::initialize(rnd, element_size, elements, 
                                       Buffer_Type_Vertex, use_flags, 
                                       cpu_access, initial_data);
    }

    using buffer_core::get_element_count;
    using buffer_core::get_element_size;
    using buffer_core::get_buffer_size;
    using buffer_core::get_handle;
    using buffer_core::update_data;
    using buffer_core::operator int buffer_core::helper_t::*;
    using buffer_core::operator!;
};
