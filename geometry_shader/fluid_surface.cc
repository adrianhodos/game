#include "pch_hdr.h"

#include <v8/math/light.h>
#include <v8/math/matrix3X3.h>
#include <v8/math/matrix4X4.h>
#include <v8/math/vector3.h>

#include "directx_renderer.h"
#include "directx_utils.h"
#include "draw_context.h"
#include "fluid_surface.h"
#include "sampler_descriptor.h"

//struct vxshader_globals_t {
//    v8::math::matrix_4X4F   world_view_projection;
//    v8::math::matrix_4X4F   world_transform;
//    v8::math::matrix_4X4F   normal_transform;
//    v8::math::matrix_3X3F   texture_transform;
//};
//
//struct fragshader_globals_t {
//    v8::math::light         light_source;
//    v8::math::vector3F      eye_pos;
//};
//
//static_assert((sizeof(fragshader_globals_t) % 16) == 0, "must be multiple of 16");
//
//fluid_surface::fluid_surface( 
//    float time_factor, 
//    float wave_speed, 
//    float damping_factor, 
//    float cell_size, 
//    int quads_x_axis, 
//    int quads_z_axis 
//    )
//    : timeDelta_(0.0f), timeFactor_(time_factor), waveSpeed_(wave_speed),
//      dampingFactor_(damping_factor), cellDist_(cell_size),
//      trisXAxis_(quads_x_axis + 1), trisZAxis_(quads_z_axis + 1),
//      currentBuffer_(0), vtx_shader_("basic_transform"), 
//      frag_shader_("lighting"), freezeTime_(false),
//      sampler_("Min_Mag_Mip_Linear"),
//      tex_offset_(v8::math::vector2F::zero) {}
//
//void fluid_surface::precompute_coefficients() {
//    float miuMultTime = dampingFactor_ * timeFactor_ + 2.0f;
//    float ctSquatedOverD = (waveSpeed_ * waveSpeed_ * timeFactor_ * timeFactor_)
//        / (cellDist_ * cellDist_);
//    coefficientOne_ = (4.0f - 8.0f * ctSquatedOverD) / miuMultTime;
//    coefficientTwo_ = (dampingFactor_ * timeFactor_ - 2.0f) / miuMultTime;
//    coefficientThree_ = 2.0f * ctSquatedOverD / miuMultTime;
//}
//
//void fluid_surface::evaluate_surface(const graphics_render_t* R) {
//    //
//    // do nothing if surface is frozen
//    if (freezeTime_)
//        return;
//
//    for (int j = 1; j < (trisZAxis_ - 1); ++j) {
//        const vertex_PNT* currData = currentSolution_ + j * trisXAxis_;
//        vertex_PNT* prevData = previousSolution_ + j * trisXAxis_;
//        for (int i = 1; i < (trisXAxis_ - 1); ++i) {
//            prevData[i].vt_pos.y_ = 
//                coefficientOne_ * currData[i].vt_pos.y_
//                + coefficientTwo_ * prevData[i].vt_pos.y_
//                + coefficientThree_ * (currData[i + 1].vt_pos.y_ + currData[i - 1].vt_pos.y_
//                + currData[i + trisXAxis_].vt_pos.y_
//                + currData[i - trisXAxis_].vt_pos.y_);
//        }
//    }
//    //
//    // swap buffers
//    std::swap(previousSolution_, currentSolution_);
//
//    //
//    // compute normals
//    for (int j = 1; j < (trisZAxis_ - 1); ++j) {
//        for (int i = 1; i < (trisXAxis_ - 1); ++i) {
//            float l = currentSolution_[j * trisXAxis_ + i - 1].vt_pos.y_;
//            float r = currentSolution_[j * trisXAxis_ + i + 1].vt_pos.y_;
//            float t = currentSolution_[(j - 1) * trisXAxis_ + i].vt_pos.y_;
//            float b = currentSolution_[(j + 1) * trisXAxis_ + i].vt_pos.y_;
//
//            v8::math::vector3F& surfaceNormal = currentSolution_[j * trisXAxis_ + i].vt_norm;
//            surfaceNormal.x_ = -r + l;
//            surfaceNormal.z_ = b - t;
//            surfaceNormal.y_ = 2 * cellDist_;
//            surfaceNormal.normalize();
//        }
//    }
//
//    //
//    // update GPU data
//    vertexBuffer_.update_data(R, currentSolution_);
//}
//
//void fluid_surface::generate_surface_grid() {
//    v8::math::matrix_4X4F mtxTranslateToOrigin(v8::math::matrix_4X4F::identity);
//
//    mtxTranslateToOrigin.set_column(4, -((trisXAxis_ - 1) * cellDist_) / 2, 
//                                    0.0f, -((trisZAxis_ - 1) * cellDist_) / 2, 
//                                    1.0f);
//
//    v8::base::scoped_pointer_reset(
//        buffVertices_, new vertex_PNT[2 * trisXAxis_ * trisZAxis_]);
//    previousSolution_ = v8::base::scoped_pointer_get(buffVertices_);
//    currentSolution_ = v8::base::scoped_pointer_get(buffVertices_) 
//        + trisXAxis_ * trisZAxis_;
//
//    int currentIndex = 0;
//    const float delta_u = 1.0f / (trisXAxis_ - 1);
//    const float delta_v = 1.0f / (trisZAxis_ - 1);
//
//    for (int j = 0; j < trisZAxis_; ++j) {
//        for (int i = 0; i < trisXAxis_; ++i) {
//            vertex_PNT currentVertex;
//            currentVertex.vt_pos = v8::math::vector3F(i * cellDist_, 0.0f, j * cellDist_);
//            //currentVertex.vt_pos = mtxTranslateToOrigin * currentVertex.vt_pos;
//            mtxTranslateToOrigin.transform_affine_vector(&currentVertex.vt_pos);
//            //currentVertex.diffuseMaterial_ = gfx::color(0.0f, 0.0f, 1.0f);
//            //currentVertex.specularMaterial_ = gfx::color(1.0f, 1.0f, 1.0f, 128.0f);
//            currentVertex.vt_norm = v8::math::vector3F::unit_y;
//            currentVertex.vt_texc.x_ = i * delta_u;
//            currentVertex.vt_texc.y_ = j * delta_v;
//
//            previousSolution_[currentIndex] = currentVertex;
//            currentSolution_[currentIndex] = currentVertex;
//
//            ++currentIndex;
//        }
//    }
//}
//
//bool fluid_surface::setup_vertex_index_buffers(
//    const graphics_render_t* R
//    )
//{
//    if(!vertexBuffer_.initialize(R, sizeof(vertex_PNT) * trisXAxis_ * trisZAxis_,
//                                 trisXAxis_ * trisZAxis_, nullptr, 
//                                 Buffer_Use_GPU_RDONLY_CPU_RDONLY,
//                                 Resource_CPU_WriteAccess))
//        return false;
//
//    std::vector<uint32_t> indicesInfo;
//    indicesInfo.reserve(trisXAxis_ * trisZAxis_);
//    for (int j = 0; j < (trisZAxis_ - 1); ++j) {
//        for (int i = 0; i < (trisXAxis_ - 1); ++i) {
//            //
//            // First triangle
//            indicesInfo.push_back(j * trisXAxis_ + i);
//            indicesInfo.push_back((j + 1) * trisXAxis_ + i);
//            indicesInfo.push_back((j + 1) * trisXAxis_ + i + 1);
//            //
//            // Second triangle
//            indicesInfo.push_back(j * trisXAxis_ + i);
//            indicesInfo.push_back((j + 1) * trisXAxis_ + i + 1);
//            indicesInfo.push_back(j * trisXAxis_ + i + 1);
//        }
//    }
//
//    if (!indexBuffer_.initialize(R, indicesInfo.size(), &indicesInfo[0]))
//        return false;
//
//    sampler_descriptor_t sampler_desc;
//    sampler_desc.sdt_filter_type = sampler_descriptor_t::Filter_Type_Min_Mag_Mip_Linear;
//    sampler_desc.sdt_texture_address_u = sampler_desc.sdt_texture_address_v =
//        sampler_descriptor_t::Texture_Address_Wrap;
//
//    if (!sampler_.initialize(R, sampler_desc))
//        return false;
//
//    return true;
//}
//
//bool fluid_surface::load_materials(const graphics_render_t* R) {
//    if (rsview_fluid_texture_)
//        return true;
//
//    const char* const fluid_texture_file = "D:\\projects\\texturecache\\water1.dds";
//
//    HRESULT ret_code;
//    CHECK_D3D(
//        &ret_code,
//        ::D3DX11CreateShaderResourceViewFromFileA(
//            R->get_device(), fluid_texture_file, nullptr, nullptr,
//            v8::base::scoped_pointer_get_impl(rsview_fluid_texture_),
//            nullptr));
//
//    return ret_code == S_OK;
//}
//
//bool fluid_surface::setup_grid_layout(const graphics_render_t* R) {
//    const uint32_t compile_flags = 
//        vertex_shader_t::Shader_Compile_Debug 
//        | vertex_shader_t::Shader_Compile_Force_IEEE_Strictness
//        | vertex_shader_t::Shader_Compile_O0
//        | vertex_shader_t::Shader_Compile_Pack_Matrix_Row_Major
//        | vertex_shader_t::Shader_Compile_Skip_Optimization
//        | vertex_shader_t::Shader_Compile_Warnings_Errors;
//
//    const char* kVertexShaderFile = 
//        "D:\\projects\\shadercache\\geometry_shader\\vertex_shader.hlsl";
//
//    if (!vtx_shader_.create_shader_from_file(R, kVertexShaderFile, "vs_main", 
//            vertex_shader_t::Shader_Model_5_0, compile_flags))
//            return false;
//
//    HRESULT ret_code;
//    CHECK_D3D(
//        &ret_code,
//        R->get_device()->CreateInputLayout(
//            vertex_PNT::get_vertex_format_description(), 
//            vertex_PNT::element_count,
//            vtx_shader_.get_compiled_bytecode()->GetBufferPointer(),
//            vtx_shader_.get_compiled_bytecode()->GetBufferSize(),
//            v8::base::scoped_pointer_get_impl(fluidInputLayout_)
//            ));
//
//    if (FAILED(ret_code))
//        return false;
//
//    const char* const kFragmentShaderFile = 
//        "D:\\projects\\shadercache\\geometry_shader\\frag_lighting.hlsl";
//    return frag_shader_.create_shader_from_file(
//        R, kFragmentShaderFile, "ps_main",
//        pixel_shader_t::Shader_Model_5_0, compile_flags
//        );
//}
//
//bool fluid_surface::initialize(const graphics_render_t* R) {
//    generate_surface_grid();
//    precompute_coefficients();
//
//    typedef bool (fluid_surface::*init_func_t)(const graphics_render_t*);
//
//    const init_func_t init_sequence[] = {
//        &fluid_surface::setup_vertex_index_buffers,
//        &fluid_surface::setup_grid_layout,
//        &fluid_surface::load_materials
//    };
//
//    auto itr_seq = std::find_if_not(
//        std::begin(init_sequence), std::end(init_sequence),
//        [this, R](init_func_t init_fnptr) {
//            return (this->*init_fnptr)(R);
//    });
//
//    return itr_seq == std::end(init_sequence);
//}
//
//void fluid_surface::update(float delta_time, const graphics_render_t* R) {
//    timeDelta_ += delta_time;
//    if (timeDelta_ < timeFactor_)
//        return;
//
//    timeDelta_ = 0.0f;
//    evaluate_surface(R);
//    tex_offset_.y_ += 0.1f * delta_time;
//    tex_offset_.x_ = 0.25f * sin(4.0f * tex_offset_.y_);
//}
//
//void fluid_surface::disturb(int row, int column, float wave_magnitude) {
//    if (freezeTime_)
//        return;
//
//    assert(row > 1 && (row < (trisXAxis_ - 1)));
//    assert(column > 1 && (column < (trisZAxis_ - 1)));
//
//    const int kVertsXAxis = trisXAxis_;
//    const int kMainVertex = column * kVertsXAxis + row;
//    const int kVertexNeighbour1 = column * kVertsXAxis + row - 1;
//    const int kVertexNeighbour2 = (column + 1) * kVertsXAxis + row - 1;
//    const int kVertexNeighbour3 = (column + 1) * kVertsXAxis + row;
//    const int kVertexNeighbour4 = (column + 1) * kVertsXAxis + row + 1;
//    const int kVertexNeighbour5 = (column) * kVertsXAxis + row + 1;
//    const int kVertexNeighbour6 = (column - 1) * kVertsXAxis + row + 1;
//    const int kVertexNeighbour7 = (column - 1) * kVertsXAxis + row;
//    const int kVertexNeighbour8 = (column - 1) * kVertsXAxis + row - 1;
//
//    struct VertexPerturbData {
//        int     vertexIndex;
//        float   perturbationMagnitude;
//    } perturbedVertices[] = {
//        { kMainVertex, wave_magnitude},
//        { kVertexNeighbour1, 0.5f * wave_magnitude },
//        { kVertexNeighbour2, 0.5f * wave_magnitude },
//        { kVertexNeighbour3, 0.5f * wave_magnitude },
//        { kVertexNeighbour4, 0.5f * wave_magnitude },
//        { kVertexNeighbour5, 0.5f * wave_magnitude },
//        { kVertexNeighbour6, 0.5f * wave_magnitude },
//        { kVertexNeighbour7, 0.5f * wave_magnitude },
//        { kVertexNeighbour8, 0.5f * wave_magnitude }
//    };
//
//    std::for_each(
//        std::begin(perturbedVertices),
//        std::end(perturbedVertices),
//        [this](const VertexPerturbData& perturbInfo) {
//            currentSolution_[perturbInfo.vertexIndex].vt_pos.y_ += 
//                perturbInfo.perturbationMagnitude;
//    });
//}
//
//void fluid_surface::draw(
//    const draw_context_t* draw_ctx, 
//    const graphics_render_t* R
//    )
//{
//    const unsigned int strides = sizeof(vertex_PNT);
//    const unsigned int offsets = 0;
//
//    R->IA_Stage_set_vertex_buffers(&vertexBuffer_, 1, &offsets, &strides);
//    R->IA_Stage_set_index_buffer(&indexBuffer_);
//    R->IA_Stage_set_primitive_topology_type(
//        graphics_render_t::Topology_Type_TriangleList
//        );
//    R->get_device_context()->IASetInputLayout(
//        v8::base::scoped_pointer_get(fluidInputLayout_)
//        );
//
//    v8::math::matrix_3X3F texture_coord_scale;
//    texture_coord_scale.make_scale(5.0f, 5.0f);
//    v8::math::matrix_3X3F texture_coord_translate;
//    texture_coord_translate.make_translation(tex_offset_);
//
//    vxshader_globals_t globals_vxshader = {
//        draw_ctx->projViewMatrix, // use identity as model to world transform
//        v8::math::matrix_4X4F::identity,
//        v8::math::matrix_4X4F::identity,
//        texture_coord_translate * texture_coord_scale
//    };
//
//    vtx_shader_.set_uniform_block_by_name("globals", globals_vxshader);
//
//    fragshader_globals_t globals_fragment_shader = {
//        draw_ctx->light,
//        draw_ctx->eye_pos
//    };
//
//    const char* const uniform_materials[] = {
//        "g_material_diffuse",
//        "g_material_specular",
//        "g_material_ambient"
//    };
//
//    using namespace std;
//    for_each(
//        begin(uniform_materials), end(uniform_materials), 
//        [this](const char* uniform_name) {
//            frag_shader_.set_uniform_by_name(
//                uniform_name, 
//                v8::base::scoped_pointer_get(rsview_fluid_texture_));
//    });
//
//    frag_shader_.set_uniform_block_by_name("globals", globals_fragment_shader);
//    frag_shader_.set_sampler("g_sampler", sampler_);
//
//    if (draw_ctx->light.get_type() == v8::math::light::light_type_spot) {
//        const float Phi_Over_Two = cos(draw_ctx->light.get_spot_cone_angle() / 2);
//        const float Theta_Over_Two = cos(draw_ctx->light.get_spot_cone_angle() / 4);
//
//        frag_shader_.set_uniform_by_name("Phi_Over_Two", Phi_Over_Two);
//        frag_shader_.set_uniform_by_name("Theta_Over_Two", Theta_Over_Two);
//    }
//
//    vtx_shader_.bind_to_pipeline(R);
//    frag_shader_.bind_to_pipeline(R);
//
//    R->draw_indexed(indexBuffer_.get_element_count());
//}
