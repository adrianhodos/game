#include "pch_hdr.h"

#include <v8/base/crt_handle_policies.h>
#include <v8/base/count_of.h>
#include <v8/base/debug_helpers.h>
#include <v8/base/scoped_handle.h>

#include "app_specific.h"
#include "cam_controller_spechical_coordinates.h"
#include "main_window.h"
#include "renderer.h"
#include "scene_graph_node.h"
#include "scene_graph_group.h"
#include "scene_graph.h"
#include "simple_mesh.h"
#include "utility.h"

inline void json_read_float_array(const rapidjson::Value& arr_obj, float* dst_arr, uint32_t max_count) {
    uint32_t i = 0;
    while (i < max_count && i < arr_obj.Size()) {
        dst_arr[i] = static_cast<float>(arr_obj[i].GetDouble());
        ++i;
    }
}

scene_graph::scene_graph() 
    : renderer_(nullptr), light_count_(0), active_light_count_(0), cam_() {}

scene_graph::~scene_graph() {}

void scene_graph::read_lighs_section(const rapidjson::Value& lights_section) {
    assert(lights_section.IsArray());

    using namespace v8::base;
    using namespace v8::math;

    uint32_t i = 0;
    while (i < kMaxLights && i < lights_section.Size()) {
        const rapidjson::Value& current_entry = lights_section[i];

        color color_info;

        struct field_and_ptr_t {
            const char* fp_name;
            color       fp_color;

            field_and_ptr_t() : fp_name(nullptr) {}

            field_and_ptr_t(const char* name)
                : fp_name(name) {}
        };

        field_and_ptr_t kCommonFields[] = {
            field_and_ptr_t("ambient"),
            field_and_ptr_t("diffuse"),
            field_and_ptr_t("specular")
        };

        for (uint32_t j = 0; j < count_of_array(kCommonFields); ++j) {
            assert(current_entry.HasMember(kCommonFields[j].fp_name));
            json_read_float_array(
                current_entry[kCommonFields[j].fp_name], 
                kCommonFields[j].fp_color.components_, 
                count_of_array(kCommonFields[j].fp_color.components_));
        }

        int32_t ltype = current_entry["type"].GetInt();
        if (ltype == light::light_type_directional) {
            vector3F dir_vec;
            json_read_float_array(current_entry["direction"], dir_vec.elements_, 
                                  count_of_array(dir_vec.elements_));
            add_scene_light(light(kCommonFields[0].fp_color, 
                            kCommonFields[1].fp_color, 
                            kCommonFields[2].fp_color, dir_vec));
        } else if (ltype == light::light_type_point) {
            vector3F pos_point;
            json_read_float_array(current_entry["position"], pos_point.elements_, 
                                  count_of_array(pos_point.elements_));
            vector3F atten_factors;
            json_read_float_array(current_entry["attenuation"],
                atten_factors.elements_,
                count_of_array(atten_factors.elements_));
            float max_range = static_cast<float>(
                current_entry["max_range"].GetDouble());

            add_scene_light(light(
                kCommonFields[0].fp_color, kCommonFields[1].fp_color, 
                kCommonFields[2].fp_color, pos_point, atten_factors, max_range
                ));
        } else if (ltype == light::light_type_spot) {

        } else {
            OUTPUT_DBG_MSGA("Incorrect value in type field (%d)", ltype);
        }

        ++i;
    }
}

bool scene_graph::read_scene_file(const char* file_name) {
    std::string cfg_file_path;
    string_vprintf(&cfg_file_path, "%s\\%s\\%s", app_dirs::kAppAssetsDir, 
                   app_dirs::kConfigSubDir, file_name);

    using namespace v8::base;
    scoped_handle<crt_file_handle> fp(fopen(cfg_file_path.c_str(), "r"));
    if (!fp) {
        OUTPUT_DBG_MSGA("Failed to open config file %s", cfg_file_path.c_str());
        return false;
    }

    struct stat file_stats;
    uint32_t rdbuff_size = 65535;
    if (!stat(cfg_file_path.c_str(), &file_stats)) {
        rdbuff_size = file_stats.st_size;
    }

    std::vector<char> read_buff;
    read_buff.resize(rdbuff_size);
    using namespace rapidjson;
    FileReadStream frs(scoped_handle_get(fp), &read_buff[0], read_buff.size());
    Document conf_file;
    if (conf_file.ParseStream<0, Document::EncodingType>(frs).HasParseError()) {
        OUTPUT_DBG_MSGA("Failed to parse config file %s", cfg_file_path.c_str());
        return false;
    }

    assert(conf_file.HasMember("scene_section"));
    assert(conf_file["scene_section"].HasMember("lights"));
    read_lighs_section((conf_file["scene_section"])["lights"]);

    assert(conf_file["scene_section"].HasMember("objects"));
    std::vector<object_data_t> object_list;
    read_objects_section((conf_file["scene_section"])["objects"], &object_list);
    if (!object_list.empty())
        create_objects(object_list);
    return true;
}

void scene_graph::create_objects(
    const std::vector<object_data_t>& objects
    ) {
    using namespace v8::base;
    scoped_ptr<scene_graph_group> group(new scene_graph_group("root_group"));
    for (uint32_t i = 0; i < objects.size(); ++i) {
        scoped_ptr<simple_mesh> new_node(
            new simple_mesh(objects[i].mesh.c_str(), "vertex_PN", 
            scoped_pointer_get(group)));

        v8::math::transformF object_transform(objects[i].position);
        if (objects[i].has_scale) {
            object_transform.set_scale_component(objects[i].scale_factor);
        }

        new_node->set_local_transform(object_transform);

        if (new_node->initialize(this)) {
            group->add_child(scoped_pointer_release(new_node));
        }
    }
    scene_root_ = std::move(group);
}

void scene_graph::read_objects_section(
    const rapidjson::Value& object_section, 
    std::vector<object_data_t>* objects
    ) {
    assert(object_section.IsArray());
    for (uint32_t i = 0; i < object_section.Size(); ++i) {
        const rapidjson::Value& current_val = object_section[i];
        object_data_t current_obj;
        current_obj.mesh = current_val["mesh"].GetString();
        json_read_float_array(
            current_val["position"], current_obj.position.elements_, 
            v8::base::count_of_array(current_obj.position.elements_)
            );

        if (current_val.HasMember("scale_factor")) {
            current_obj.has_scale = true;
            current_obj.scale_factor = static_cast<float>(
                current_val["scale_factor"].GetDouble());
        }
        objects->push_back(current_obj);
    }
}

void scene_graph::initialize(renderer* r, resource_manager* rsm, MainWindow* mw) {
    renderer_ = r;
    rsman_ = rsm;

    v8::base::scoped_ptr<camera_controller_spherical_coords> controller_ptr(
        new camera_controller_spherical_coords());
    mw->set_keyboard_event_receiver(v8::base::scoped_pointer_get(controller_ptr));
    mw->set_mouse_event_receiver(v8::base::scoped_pointer_get(controller_ptr));

    cam_.set_controller(v8::base::scoped_pointer_release(controller_ptr));
    cam_.initialize();

    const char* const kCFGFileName = "scene_config.json";
    read_scene_file(kCFGFileName);
}

void scene_graph::build_lights_list() {
    active_light_count_ = 0;
    uint32_t i = 0;
    while (i < light_count_) {
        if (scene_lights_[i].is_on()) {
            active_scene_lights_[i] = *scene_lights_[i].get_light();
            ++active_light_count_;
        }
        ++i;
    }
}

void scene_graph::update(float delta_ms) {
    using namespace std;

    for_each(begin(scene_lights_), end(scene_lights_),
             [this, delta_ms](scene_light& sl) {
        sl.update(this, delta_ms);
    });

    cam_.update(this, delta_ms);

    if (scene_root_)
        scene_root_->update(this, delta_ms);
}

void scene_graph::draw() {
    if (scene_root_) {
        build_lights_list();
        scene_root_->pre_draw(this);
        scene_root_->draw(this);
        scene_root_->post_draw(this);
    }
}