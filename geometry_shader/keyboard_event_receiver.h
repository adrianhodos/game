#pragma once

class keyboard_event_receiver {
public :
    virtual ~keyboard_event_receiver() {}

    virtual bool key_press_event(int key_code) = 0;

    virtual bool key_depress_event(int key_code) = 0;
};
