#pragma once

namespace v8 { namespace math {
    class camera;
} }

class scene_graph;

class camera_controller {
protected :
    v8::math::camera*   cam_ptr_;

public :
    virtual ~camera_controller() {}

    camera_controller(v8::math::camera* cam = nullptr)
        : cam_ptr_(cam) {}

    v8::math::camera* get_camera() {
        return cam_ptr_;
    }

    void set_camera(v8::math::camera* cam) {
        cam_ptr_ = cam;
    }

    virtual bool initialize() { return true; }

    virtual void update(scene_graph* s, float delta_ms) = 0;
};
