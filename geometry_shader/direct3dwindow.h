#pragma once

#include <dxgi.h>
#include <d3d11.h>
#include <v8/base/scoped_pointer.h>

namespace app {

class Direct3DWindow {
protected :
    float                                                               windowWidth_;
    float                                                               windowHeight_;
    HINSTANCE                                                           instanceRef_;
    HWND                                                                windowHandle_;

    virtual LRESULT WindowProcedure(UINT msg, WPARAM w_param, LPARAM l_param) = 0;

    virtual bool RegisterWindowClass();

private :
    static LRESULT WINAPI WindowProcStub(
        HWND window, UINT msg, WPARAM w_param, LPARAM l_param
        );

public :
    Direct3DWindow(HINSTANCE instance, float clientWidth, float clientHeight);

    virtual ~Direct3DWindow();

    virtual bool Initialize();

    virtual void message_loop() = 0;

    float GetWindowHeight() const { return windowHeight_; }

    float GetWindowWidth() const { return windowWidth_; }
};

}
