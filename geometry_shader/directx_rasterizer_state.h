#pragma once

#include <cstdint>
#include <D3D11.h>

enum Raster_Fill_Mode {
    RS_Fill_Solid,
    RS_Fill_Wireframe
};

enum Raster_Culling_Mode {
    RS_Cull_None,
    RS_Cull_Front,
    RS_Cull_Back
};

struct raster_state_info_t {
    Raster_Fill_Mode        rsi_fill_mode;
    Raster_Culling_Mode     rsi_cull_mode;
    bool                    rsi_front_face_is_ccw;
    int32_t                 rsi_depth_bias;
    float                   rsi_depth_bias_clamp;
    float                   rsi_slope_bias;
    bool                    rsi_depth_clip;
    bool                    rsi_scissor;
    bool                    rsi_multisample;
    bool                    rsi_antialias;

    static raster_state_info_t default_state() {
        raster_state_info_t rst = {
            RS_Fill_Solid,
            RS_Cull_Back,
            false,
            0,
            0.0f,
            0.0f,
            true,
            false,
            false,
            false
        };

        return rst;
    }
};

inline bool operator<(
    const raster_state_info_t& left, 
    const raster_state_info_t& right
    ) {
    if (left.rsi_fill_mode < right.rsi_fill_mode)
        return true;

    if (left.rsi_cull_mode < right.rsi_cull_mode)
        return true;

    return false;
}

//class renderer;
//
//class rasterizer_state {
//private :
//    ID3D11RasterizerState*  rstate_;
//
//public :
//    typedef ID3D11RasterizerState* rstate_handle_t;
//
//    rasterizer_state() : rstate_(nullptr) {}
//
//    bool initialize(renderer* R, const raster_state_info_t& init_params);
//
//    bool operator!() const {
//        return rstate_ == nullptr;
//    }
//
//    rstate_handle_t get_handle() const {
//        return rstate_;
//    }
//};