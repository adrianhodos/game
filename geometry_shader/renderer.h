#pragma once

#include <v8/config/config.h>

#if defined(RENDER_SYSTEM_D3D11)
#include "directx_renderer.h"
#else
#error no renderer for this build system
#endif