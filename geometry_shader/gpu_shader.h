#pragma once

#include <D3Dcompiler.h>
#include <cstdint>
#include <string>

class gpu_shader {
public :
    enum Shader_Type {
        Shader_Type_Vertex,
        Shader_Type_Geometry,
        Shader_Type_Fragment
    };

    enum Shader_Profile {
        Shader_Profile_3_0,
        Shader_Profile_4_0,
        Shader_Profile_4_1,
        Shader_Profile_5_0
    };

    enum Shader_Compile_Flags {
        Shader_Compile_Avoid_Flow_Control = D3DCOMPILE_AVOID_FLOW_CONTROL,
        Shader_Compile_Debug = D3DCOMPILE_DEBUG,
        Shader_Compile_With_Backward_Compat = D3DCOMPILE_ENABLE_BACKWARDS_COMPATIBILITY,
        Shader_Compile_Strict_Syntax = D3DCOMPILE_ENABLE_STRICTNESS,
        Shader_Compile_Pixel_Soft_No_Opt = D3DCOMPILE_FORCE_PS_SOFTWARE_NO_OPT,
        Shader_Compile_Vertex_Soft_No_Opt = D3DCOMPILE_FORCE_VS_SOFTWARE_NO_OPT,
        Shader_Compile_Force_IEEE_Strictness = D3DCOMPILE_IEEE_STRICTNESS,
        Shader_Compile_Disable_Preshaders = D3DCOMPILE_NO_PRESHADER,
        Shader_Compile_O0 = D3DCOMPILE_OPTIMIZATION_LEVEL0,
        Shader_Compile_O1 = D3DCOMPILE_OPTIMIZATION_LEVEL1,
        Shader_Compile_O2 = D3DCOMPILE_OPTIMIZATION_LEVEL2,
        Shader_Compile_O3 = D3DCOMPILE_OPTIMIZATION_LEVEL3,
        Shader_Compile_Pack_Matrix_Row_Major = D3DCOMPILE_PACK_MATRIX_ROW_MAJOR,
        Shader_Compile_Pack_Matrix_Col_Major = D3DCOMPILE_PACK_MATRIX_COLUMN_MAJOR,
        Shader_Compile_Use_Partial_Precision = D3DCOMPILE_PARTIAL_PRECISION,
        Shader_Compile_Prefer_Flow_Control = D3DCOMPILE_PREFER_FLOW_CONTROL,
        Shader_Compile_Skip_Optimization = D3DCOMPILE_SKIP_OPTIMIZATION,
        Shader_Compile_Skip_Validation = D3DCOMPILE_SKIP_VALIDATION,
        Shader_Compile_Warnings_Errors = D3DCOMPILE_WARNINGS_ARE_ERRORS
    };

private :
    Shader_Type         type_;
    Shader_Profile      profile_;
    const uint32_t      compile_flags_;
    const std::string   name_;

public :
    static const uint32_t kDefaultCompileFlags = 
        Shader_Compile_Debug | Shader_Compile_Force_IEEE_Strictness 
        | Shader_Compile_O0 | Shader_Compile_Skip_Optimization 
        | Shader_Compile_Pack_Matrix_Row_Major;

    gpu_shader(
        Shader_Type type, 
        Shader_Profile profile, 
        const char* name, 
        uint32_t compile_flags = kDefaultCompileFlags
        );
};