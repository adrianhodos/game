#pragma once

#include <cstdint>
#include <utility>
#include <d3d11.h>
#include <v8/base/scoped_pointer.h>

class renderer;

enum Buffer_Type {
    Buffer_Type_Vertex,
    Buffer_Type_Index,
    Buffer_Type_Constant,
    Buffer_Type_Unordered_Access,
    Buffer_Type_Stream_Output,
    Buffer_Type_Max
};

enum Buffer_Usage_Flags {
    Buffer_Use_GPU_RDWR = D3D11_USAGE_DEFAULT,
    Buffer_Use_GPU_RDONLY = D3D11_USAGE_IMMUTABLE,
    Buffer_Use_GPU_RDONLY_CPU_RDONLY = D3D11_USAGE_DYNAMIC,
    Buffer_Use_GPU_COPY_TO_CPU = D3D11_USAGE_STAGING
};

enum Resource_CPU_Access {
    Resource_CPU_NoAccess = 0,
    Resource_CPU_ReadAccess = D3D11_CPU_ACCESS_READ,
    Resource_CPU_WriteAccess = D3D11_CPU_ACCESS_WRITE
};

class buffer_core {
private :
    v8::base::scoped_ptr<ID3D11Buffer, v8::base::com_storage> buffer_handle_;
    uint32_t    elements_;
    uint32_t    element_size_;

    struct helper_t {
        int dummy;
    };

public :
    typedef ID3D11Buffer*   handle_type;

    buffer_core() : elements_(0), element_size_(0) {}

    buffer_core(buffer_core&& rval) {
        *this = std::move(rval);
    }

    buffer_core& operator=(buffer_core&& rval) {
        buffer_handle_ = std::move(rval.buffer_handle_);
        elements_ = rval.elements_;
        element_size_ = rval.element_size_;
        rval.elements_ = 0;
        rval.element_size_ = 0;        
        return *this;
    }

    buffer_core(
        const renderer* rnd,
        unsigned int element_size,
        unsigned int element_count,
        Buffer_Type type,
        Buffer_Usage_Flags use_flags,
        Resource_CPU_Access cpu_access,
        const void* initial_data = nullptr
        ) {
        initialize(rnd, element_size, element_count, type, use_flags, 
                   cpu_access, initial_data);
    }

    bool initialize(
        const renderer* rnd,
        unsigned int element_size,
        unsigned int element_count,
        Buffer_Type type,
        Buffer_Usage_Flags use_flags,
        Resource_CPU_Access cpu_access,
        const void* initial_data = nullptr
        );

    void update_data(
        const renderer* rnd,
        const void* data
        );

    handle_type get_handle() const {
        return v8::base::scoped_pointer_get(buffer_handle_);
    }

    uint32_t get_element_count() const {
        return elements_;
    }

    uint32_t get_element_size() const {
        return element_size_;
    }

    uint32_t get_buffer_size() const {
        return elements_ * element_size_;
    }

    bool operator!() const {
        return !buffer_handle_;
    }

    operator int helper_t::*() const {
        return !buffer_handle_ ? nullptr : &helper_t::dummy;
    }
};
