#pragma once

#include <v8/base/scoped_pointer.h>
#include <v8/math/camera.h>
#include "camera_controller.h"

class scene_graph;

class scene_camera {
private :
    v8::math::camera                            cam_;
    v8::base::scoped_ptr<camera_controller>     cam_controller_;

public :
    scene_camera(camera_controller* cam_controller = nullptr)
        : cam_(), cam_controller_(cam_controller) {
            if (cam_controller_)
                cam_controller_->set_camera(&cam_);
    }

    scene_camera(const v8::math::camera& cam, 
                 camera_controller* controller = nullptr)
        : cam_(cam), cam_controller_(controller) {
        if (cam_controller_)
            cam_controller_->set_camera(&cam_);
    }

    v8::math::camera* get_camera() {
        return &cam_;
    }

    void update(scene_graph* sg, float delta_ms) {
        assert(cam_controller_);
        cam_controller_->update(sg, delta_ms);
    }

    camera_controller* get_controller() const {
        return v8::base::scoped_pointer_get(cam_controller_);
    }

    void set_controller(camera_controller* cam_c) {
        v8::base::scoped_pointer_reset(cam_controller_, cam_c);
        cam_controller_->set_camera(&cam_);
    }

    bool initialize() { 
        assert(cam_controller_);
        return cam_controller_->initialize();
    }
};
