#pragma once

#include <v8/base/compiler_quirks.h>
#include <v8/base/scoped_pointer.h>

class renderer;
class resource_manager;
class scene_graph;

class app_globals {
private :
    NO_CC_ASSIGN(app_globals);
    struct private_impl_t;
    v8::base::scoped_ptr<private_impl_t>    pimpl_;

public :
    app_globals();

    ~app_globals();

    renderer*           get_rnd() const;
    resource_manager*   get_rsm() const;
    scene_graph*        get_sgraph() const;
};

extern app_globals* G_app;