#include "pch_hdr.h"

#include <v8/base/crt_handle_policies.h>
#include <v8/base/count_of.h>
#include <v8/base/scoped_handle.h>

#include "directx_include_handler.h"
#include "directx_utils.h"

v8::base::scoped_ptr<ID3D10Blob, v8::base::com_storage>
    compile_shader_into_bytecode(
    const char* fileName, 
    const char* entryPoint,
    const char* targetProfile, 
    unsigned int compileFlags
    ) {
    using namespace v8::base;

    scoped_ptr<ID3D10Blob, com_storage> compiled_bytecode;

    scoped_handle<crt_file_handle> fp(fopen(fileName, "r"));
    if (fp) {
        std::string source_code;
        char rdbuff[1024];
        while (fgets(rdbuff, count_of_array(rdbuff), scoped_handle_get(fp))) {
            source_code.append(rdbuff);
        }    

        scoped_ptr<ID3D10Blob, com_storage> error_msg;
        HRESULT ret_code;
        dxcompile_include_handler include_handler;

        CHECK_D3D(
            &ret_code,
            ::D3DCompile(source_code.c_str(), source_code.size(), nullptr,
                         nullptr, &include_handler, entryPoint, targetProfile,
                         compileFlags, 0,
                         scoped_pointer_get_impl(compiled_bytecode),
                         scoped_pointer_get_impl(error_msg)));

        if (FAILED(ret_code) && error_msg && error_msg->GetBufferPointer()) {
            OUTPUT_DBG_MSGA("Shader compilation error %s", 
                            error_msg->GetBufferPointer());
        }
    }
    return compiled_bytecode;
}
