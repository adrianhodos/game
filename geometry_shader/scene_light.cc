#include "pch_hdr.h"
#include "scene_light_controller.h"
#include "scene_light.h"

scene_light::scene_light() : active_(false), controller_(nullptr) {}

scene_light::scene_light( 
    const v8::math::light& light, 
    bool active /* = true */, 
    scene_light_controller* controller /* = nullptr */
    )
    : light_(light), active_(active), controller_(controller) {}

scene_light::~scene_light() {}

void scene_light::update(scene_graph* sg, float delta_ms) {
    if (controller_ && active_)
        controller_->update(sg, delta_ms);
}
