#pragma once

#include <cstdint>
#include <algorithm>
#include <vector>

#include <v8/math/light.h>
#include <v8/math/transform.h>
#include <v8/base/scoped_pointer.h>

class   renderer;
class   scene_graph;

class scene_graph_node {
protected :
    typedef v8::base::scoped_ptr<scene_graph_node>        child_ptr_t;

    v8::math::transformF       world_transform_;
    v8::math::transformF       local_transform_;
    bool                       transform_dirty_flag_;
    scene_graph_node*          parent_;
    std::vector<child_ptr_t>   children_;

    scene_graph_node(scene_graph_node* parent = nullptr);

    virtual void own_predraw(scene_graph* s) = 0;

    virtual void own_draw(scene_graph* s) = 0;

    virtual void own_post_draw(scene_graph* s) = 0;

public :
    virtual ~scene_graph_node();

    virtual bool initialize(scene_graph*) { return true; }

    virtual void update(scene_graph* s, float delta_ms);

    void pre_draw(scene_graph* s);

    void draw(scene_graph* s);

    void post_draw(scene_graph* s);

    void add_child(scene_graph_node* child) {
        children_.push_back(v8::base::scoped_ptr<scene_graph_node>(child));
    }

    uint32_t get_child_count() const {
        return children_.size();
    }

    void remove_child(scene_graph_node* child);

    bool transform_cache_dirty() const {
        return transform_dirty_flag_;
    }

    const v8::math::transformF& get_world_transform() const {
        return world_transform_;
    }

    const v8::math::transformF& get_local_transform() const {
        return local_transform_;
    }

    void set_world_transform(const v8::math::transformF& wt) {
        world_transform_ = wt;
        transform_dirty_flag_ = true;
    }

    void set_local_transform(const v8::math::transformF& wt) {
        local_transform_ = wt;
        transform_dirty_flag_ = true;
    }

    scene_graph_node* get_child_at(uint32_t i);

    scene_graph_node* get_parent() const {
        return parent_;
    }

    void set_parent(scene_graph_node* parent) {
        parent_ = parent;
    }
};
