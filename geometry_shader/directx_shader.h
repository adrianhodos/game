#pragma once

#include <d3d11.h>
#include <D3D11Shader.h>
#include <D3Dcompiler.h>

#include <algorithm>
#include <cstdint>
#include <iterator>
#include <string>
#include <tuple>
#include <vector>

#include <v8/base/scoped_pointer.h>
#include <v8/base/fundamental_types.h>

#include "directx_renderer.h"
#include "directx_sampler.h"
#include "directx_utils.h"
#include "utility.h"

class sampler_state {
private :
    v8::base::scoped_ptr<ID3D11SamplerState, v8::base::com_storage> handle_;
public :
    typedef ID3D11SamplerState  sampler_state_handle_t;

    sampler_state(ID3D11SamplerState* sam_state)
        : handle_(sam_state) {}

    sampler_state_handle_t* get_handle() const {
        return v8::base::scoped_pointer_get(handle_);
    }

    bool operator!() const {
        return !handle_;
    }

    bool initialize(
        const renderer* R, 
        const D3D11_SAMPLER_DESC& sam_desc
        );
};

class texture2D {
private :
    struct resview_t {
        v8::base::scoped_ptr<
            ID3D11ShaderResourceView, 
            v8::base::com_storage
        > rv_handle;
        DXGI_FORMAT rv_format;

        resview_t(ID3D11ShaderResourceView* rview, DXGI_FORMAT fmt)
            : rv_handle(rview), rv_format(fmt) {}

        resview_t(resview_t&& rval) {
            *this = std::move(rval);
        }

        resview_t& operator=(resview_t&& rval) {
            rv_handle = std::move(rval.rv_handle);
            rv_format = rval.rv_format;
            return *this;
        }
    };

    v8::base::scoped_ptr<ID3D11Texture2D>   handle_;
public :
    typedef ID3D11Texture2D                 texture2D_handle_t;

    texture2D(ID3D11Texture2D* texture = nullptr)
        : handle_(texture) {}

    texture2D_handle_t* get_handle() const {
        return v8::base::scoped_pointer_get(handle_);
    }

    texture2D(texture2D&& rvalue) {
        *this = std::move(rvalue);
    }

    texture2D& operator=(texture2D&& rvalue) {
        handle_ = std::move(rvalue.handle_);
        return *this;
    }

    bool operator!() const {
        return !handle_;
    }

    texture2D clone() const;

    bool initialize(
        const renderer* R,
        const D3D11_TEXTURE2D_DESC& tex_desc,
        const void* initial_data = nullptr
        );

    void update_data(
        const renderer* R,
        const void* data,
        uint32_t data_size
        );

    //bool create_resource_view()
};

struct shader_uniform_block_t {
public :
    
    v8::base::scoped_ptr<ID3D11Buffer, v8::base::com_storage>   handle_;
    v8::base::scoped_ptr<char, v8::base::malloc_storage>        data_storage_;
    std::string                                                 name_;
    uint32_t                                                    varcount_;
    uint32_t                                                    size_;
    uint32_t                                                    bind_point_;
    mutable bool                                                data_dirty_;
    
    shader_uniform_block_t() 
        : name_(), varcount_(0), size_(0), data_dirty_(false) {}

    shader_uniform_block_t(shader_uniform_block_t&& rvalue) {
        *this = std::move(rvalue);
    }

    void set_block_component_data(
        const void* component_data, 
        uint32_t component_offset, 
        uint32_t component_size
        );

    shader_uniform_block_t& operator=(shader_uniform_block_t&& rvalue) {
        handle_ = std::move(rvalue.handle_);
        data_storage_ = std::move(rvalue.data_storage_);
        name_ = std::move(rvalue.name_);
        varcount_ = rvalue.varcount_;
        size_ = rvalue.size_;
        data_dirty_ = rvalue.data_dirty_;
        return *this;
    }

    bool operator!() const {
        return !handle_;
    }

    const std::string& get_name() const {
        return name_;
    }
    
    uint32_t get_bind_point() const {
        return bind_point_;
    }

    ID3D11Buffer* get_handle() const {
        return v8::base::scoped_pointer_get(handle_);
    }

    bool initialize(
        const renderer* R, 
        const D3D11_SHADER_BUFFER_DESC& buff_desc,
        uint32_t bind_point
        );

    template<typename Component_Type>
    inline void set_component_data(
        const Component_Type& component_value,
        uint32_t component_offset
        );

    template<typename ValueType>
    inline void set_block_data(
        const ValueType& value
        );

    inline void sync_data_with_gpu(
        const renderer* R
        ) const;
};

template<typename Component_Type>
inline void shader_uniform_block_t::set_component_data( 
    const Component_Type& component_value, 
    uint32_t component_offset
    ) {
    set_block_component_data(&component_value, component_offset, 
                             sizeof(component_value));
}

template<typename ValueType>
inline void shader_uniform_block_t::set_block_data(const ValueType& val) {
    set_block_component_data(&val, 0, sizeof(val));
}

inline void shader_uniform_block_t::sync_data_with_gpu(
    const renderer* R
    ) const {
    if (!data_dirty_)
        return;

    R->get_device_context()->UpdateSubresource(
        v8::base::scoped_pointer_get(handle_),
        0, nullptr, v8::base::scoped_pointer_get(data_storage_), 0, 0
        );

    data_dirty_ = false;
}

struct shader_uniform_t {
    shader_uniform_block_t*     parent_block_;
    std::string                 name_;
    uint32_t                    start_offset_;
    uint32_t                    size_;

    shader_uniform_t(
        shader_uniform_block_t* parent, 
        const D3D11_SHADER_VARIABLE_DESC var_desc
        )
        : parent_block_(parent),
          name_(var_desc.Name),
          start_offset_(var_desc.StartOffset),
          size_(var_desc.Size) {}

    template<typename DataType>
    inline void set_value(
        const DataType& value
        );

    inline void set_raw_value(
        const void* data, 
        uint32_t byte_count
        );
};

template<typename DataType>
inline void shader_uniform_t::set_value(const DataType& value) {
    set_raw_value(&value, sizeof(value));
}

void shader_uniform_t::set_raw_value(const void* data, uint32_t byte_count) {
    assert(parent_block_);
    assert(byte_count <= size_);
    parent_block_->set_block_component_data(data, start_offset_, byte_count);
}

struct sampler_state_t {
    std::string             name_;
    uint32_t                bindpoint_;

    sampler_state_t(
        const char* name, 
        uint32_t bind_point
        )
        : name_(name),
          bindpoint_(bind_point) {}

};

struct resource_view_t {
    std::string             rvt_name;
    uint32_t                rvt_bindpoint;
    D3D10_SRV_DIMENSION     rvt_dimension;
    uint32_t                rvt_count;

    resource_view_t(
        const char* name, uint32_t bind_point, D3D10_SRV_DIMENSION dim, 
        uint32_t count
        ) : rvt_name(name), rvt_bindpoint(bind_point), 
            rvt_dimension(dim), rvt_count(count) {}
};

template<typename T>
struct shader_traits;

template<>
struct shader_traits<ID3D11VertexShader> {
    typedef ID3D11VertexShader shader_handle_t;

    static const char* const Profile_Strings[];

    static v8::base::scoped_ptr<ID3D11VertexShader, v8::base::com_storage> 
    create_shader(const renderer* rnd, ID3D10Blob* compiledBytecode) {
        v8::base::scoped_ptr<ID3D11VertexShader, v8::base::com_storage> sh_ptr;
        HRESULT ret_code;
        CHECK_D3D(
            &ret_code,
            rnd->get_device()->CreateVertexShader(
            compiledBytecode->GetBufferPointer(), 
            compiledBytecode->GetBufferSize(), 
            nullptr,
            v8::base::scoped_pointer_get_impl(sh_ptr)));
        return sh_ptr;
    }

    static void bind_shader_to_pipeline_stage(
        const renderer* R,
        ID3D11VertexShader* vxshader
        ) {
        R->get_device_context()->VSSetShader(vxshader, nullptr, 0);
    }

    static void bind_uniform_blocks_to_pipeline_stage(
        const renderer* R,
        uint32_t start_slot,
        const std::vector<ID3D11Buffer*>& uniforms
        ) {
        R->get_device_context()->VSSetConstantBuffers(
            start_slot,
            uniforms.size(),
            &uniforms[0]
        );
    }

    static void bind_sampler_states_to_pipeline_stage(
        const renderer* R,
        uint32_t start_slot,
        const std::vector<ID3D11SamplerState*>& sam_states
        ) {
            R->get_device_context()->VSSetSamplers(
                start_slot, sam_states.size(), &sam_states[0]
            );
    }

    static void bind_resource_views_to_pipeline_stage(
        const renderer* R,
        uint32_t start_slot,
        const std::vector<ID3D11ShaderResourceView*>& resource_views
        ) {
            R->get_device_context()->VSSetShaderResources(
                start_slot, resource_views.size(), &resource_views[0]
            );
    }
};

template<>
struct shader_traits<ID3D11GeometryShader> {
    typedef ID3D11GeometryShader    shader_handle_t;

    static const char* const Profile_Strings[];

    static v8::base::scoped_ptr<ID3D11GeometryShader, v8::base::com_storage>
    create_shader(
        const renderer* rnd, 
        ID3D10Blob* compiled_bytecode
        ) {
        v8::base::scoped_ptr<ID3D11GeometryShader, v8::base::com_storage> sh_ptr;
        HRESULT ret_code;
        CHECK_D3D(
            &ret_code,
            rnd->get_device()->CreateGeometryShader(
                compiled_bytecode->GetBufferPointer(),
                compiled_bytecode->GetBufferSize(),
                nullptr,
                v8::base::scoped_pointer_get_impl(sh_ptr)));

        return sh_ptr;
    }

    static void bind_shader_to_pipeline_stage(
        const renderer* R, 
        ID3D11GeometryShader* gsshader
        ) {
        R->get_device_context()->GSSetShader(gsshader, nullptr, 0);
    }

    static void bind_uniform_blocks_to_pipeline_stage(
        const renderer* R,
        uint32_t start_slot,
        const std::vector<ID3D11Buffer*>& uniforms
    ) {
        R->get_device_context()->GSSetConstantBuffers(
            start_slot, 
            uniforms.size(), 
            &uniforms[0]
        );
    }

    static void bind_sampler_states_to_pipeline_stage(
        const renderer* R,
        uint32_t start_slot,
        const std::vector<ID3D11SamplerState*>& sam_states
        ) {
            R->get_device_context()->GSSetSamplers(
                start_slot, sam_states.size(), &sam_states[0]
            );
    }
    static void bind_resource_views_to_pipeline_stage(
        const renderer* R,
        uint32_t start_slot,
        const std::vector<ID3D11ShaderResourceView*>& resource_views
        ) {
            R->get_device_context()->GSSetShaderResources(
                start_slot, resource_views.size(), &resource_views[0]
            );
    }
};

template<>
struct shader_traits<ID3D11PixelShader> {
    typedef ID3D11PixelShader   shader_handle_t;

    static const char* const Profile_Strings[];

    static v8::base::scoped_ptr<ID3D11PixelShader, v8::base::com_storage>
    create_shader(
        const renderer* R, 
        ID3D10Blob* compiled_bytecode
        ) {
        v8::base::scoped_ptr<ID3D11PixelShader, v8::base::com_storage> sh_ptr;
        HRESULT ret_code;
        CHECK_D3D(
            &ret_code,
            R->get_device()->CreatePixelShader(
                compiled_bytecode->GetBufferPointer(),
                compiled_bytecode->GetBufferSize(),
                nullptr,
                v8::base::scoped_pointer_get_impl(sh_ptr)));
        return sh_ptr;
    }

    static void bind_shader_to_pipeline_stage(
        const renderer* R, 
        ID3D11PixelShader* pxsh
        ) {
        R->get_device_context()->PSSetShader(pxsh, nullptr, 0);
    }

    static void bind_uniform_blocks_to_pipeline_stage(
        const renderer* R,
        uint32_t start_slot,
        const std::vector<ID3D11Buffer*>& uniforms
        ) {
        R->get_device_context()->PSSetConstantBuffers(
            start_slot,
            uniforms.size(),
            &uniforms[0]
        );
    }
    
    static void bind_sampler_states_to_pipeline_stage(
        const renderer* R,
        uint32_t start_slot,
        const std::vector<ID3D11SamplerState*>& sam_states
        ) {
            R->get_device_context()->PSSetSamplers(
                start_slot, sam_states.size(), &sam_states[0]
            );
    }

    static void bind_resource_views_to_pipeline_stage(
        const renderer* R,
        uint32_t start_slot,
        const std::vector<ID3D11ShaderResourceView*>& resource_views
        ) {
            R->get_device_context()->PSSetShaderResources(
                start_slot, resource_views.size(), &resource_views[0]
            );
    }
};

template<typename Shader_Traits>
class directx_shader_t {
public :
    typedef Shader_Traits                               shader_traits_t;
    typedef typename shader_traits_t::shader_handle_t   shader_handle_t;

private :
    struct cmp_helper_t {
        int dummy;
    };

    typedef std::vector<shader_uniform_block_t>::iterator 
        uniform_block_list_iterator_t;

    typedef std::vector<shader_uniform_t>::iterator 
        uniform_list_iterator_t;

    v8::base::scoped_ptr<ID3D10Blob, v8::base::com_storage>         bytecode_;
    v8::base::scoped_ptr<shader_handle_t, v8::base::com_storage>    handle_;
    std::vector<shader_uniform_block_t>     uniform_blocks_;
    std::vector<ID3D11Buffer*>              uniform_block_binding_list_;
    std::vector<shader_uniform_t>           uniforms_;
    std::vector<sampler_state_t>            sampler_states_;
    std::vector<ID3D11SamplerState*>        sampler_state_binding_list_;
    std::vector<resource_view_t>            resource_views_;
    std::vector<ID3D11ShaderResourceView*>  resource_view_binding_list_;
    std::string                             name_;

    inline uniform_block_list_iterator_t find_uniform_block_by_name(
        const char* block_name
        );

    inline uniform_list_iterator_t find_uniform_by_name(
        const char* uniform_name
        );

    bool reflect_shader(
        const renderer* R
        );

    void reflect_constant_buffer(
        const renderer* R,
        ID3D11ShaderReflection* S_reflector, 
        uint32_t index
        );

    void reflect_bound_resource(
        const renderer* R,
        ID3D11ShaderReflection* S_reflector,
        uint32_t index
        );

public :

    enum Shader_Model {
        Shader_Model_3_0,
        Shader_Model_4_0,
        Shader_Model_4_1,
        Shader_Model_5_0,
        Shader_Model_Max
    };

    enum {
        Invalid_Uniform_Location = 0xffffffff
    };

    enum Shader_Compile_Flags {
        Shader_Compile_Avoid_Flow_Control = D3DCOMPILE_AVOID_FLOW_CONTROL,
        Shader_Compile_Debug = D3DCOMPILE_DEBUG,
        Shader_Compile_With_Backward_Compat = D3DCOMPILE_ENABLE_BACKWARDS_COMPATIBILITY,
        Shader_Compile_Strict_Syntax = D3DCOMPILE_ENABLE_STRICTNESS,
        Shader_Compile_Pixel_Soft_No_Opt = D3DCOMPILE_FORCE_PS_SOFTWARE_NO_OPT,
        Shader_Compile_Vertex_Soft_No_Opt = D3DCOMPILE_FORCE_VS_SOFTWARE_NO_OPT,
        Shader_Compile_Force_IEEE_Strictness = D3DCOMPILE_IEEE_STRICTNESS,
        Shader_Compile_Disable_Preshaders = D3DCOMPILE_NO_PRESHADER,
        Shader_Compile_O0 = D3DCOMPILE_OPTIMIZATION_LEVEL0,
        Shader_Compile_O1 = D3DCOMPILE_OPTIMIZATION_LEVEL1,
        Shader_Compile_O2 = D3DCOMPILE_OPTIMIZATION_LEVEL2,
        Shader_Compile_O3 = D3DCOMPILE_OPTIMIZATION_LEVEL3,
        Shader_Compile_Pack_Matrix_Row_Major = D3DCOMPILE_PACK_MATRIX_ROW_MAJOR,
        Shader_Compile_Pack_Matrix_Col_Major = D3DCOMPILE_PACK_MATRIX_COLUMN_MAJOR,
        Shader_Compile_Use_Partial_Precision = D3DCOMPILE_PARTIAL_PRECISION,
        Shader_Compile_Prefer_Flow_Control = D3DCOMPILE_PREFER_FLOW_CONTROL,
        Shader_Compile_Skip_Optimization = D3DCOMPILE_SKIP_OPTIMIZATION,
        Shader_Compile_Skip_Validation = D3DCOMPILE_SKIP_VALIDATION,
        Shader_Compile_Warnings_Errors = D3DCOMPILE_WARNINGS_ARE_ERRORS
    };

    directx_shader_t(
        const char* shader_name
        );

    directx_shader_t(
        directx_shader_t<Shader_Traits>&& rval
        ) { *this = std::move(rval); }

    directx_shader_t<Shader_Traits>& operator=(
        directx_shader_t<Shader_Traits>&& rval
        );

    shader_handle_t* get_handle() const {
        return v8::base::scoped_pointer_get(handle_);
    }

    operator int cmp_helper_t::*() const {
        return bytecode_ && handle_ ? &cmp_helper_t::dummy : nullptr;
    }

    bool operator!() const {
        return !handle_ || !bytecode_;
    }

    const std::string& get_name() const {
        return name_;
    }

    uint32_t get_uniform_block_count() const {
        assert(handle_);
        return uniform_blocks_.size();
    }

    uint32_t get_uniform_count() const {
        assert(handle_);
        return uniforms_.size();
    }

    uint32_t get_sampler_count() const {
        assert(handle_);
        return sampler_states_.size();
    }

    ID3D10Blob* get_compiled_bytecode() const {
        assert(bytecode_);
        return v8::base::scoped_pointer_get(bytecode_);
    }

    bool create_shader_from_file(
        const renderer* R,
        const char* file_name,
        const char* entry_point,
        Shader_Model shader_model,
        uint32_t compile_flags
        );

    void bind_to_pipeline(
        const renderer* R
        );

    template<typename Uniform_Type>
    void set_uniform_block_by_name(
        const char* block_name, 
        const Uniform_Type& uniform
        );

    uint32_t get_uniform_block_location_by_name(
        const char* block_name
        );

    template<typename Uniform_Type>
    inline void set_uniform_by_name(
        const char* uniform_name,
        const Uniform_Type& uniform_data
        );

    void set_raw_uniform_by_name(
        const char* uniform_name, 
        const void* uniform_data,
        uint32_t byte_count
        );

    uint32_t get_uniform_location_by_name(
        const char* uniform_name
        );

    void set_sampler(
        const char* sampler_name, 
        ID3D11SamplerState* samp_state
        );

    void set_texture(
        const char* texture_object_name,
        ID3D11ShaderResourceView* rsrc_view
        );
};

template<typename Shader_Traits>
directx_shader_t<Shader_Traits>::directx_shader_t(const char* shader_name)
    : name_(shader_name) {}

template<typename Shader_Traits>
directx_shader_t<Shader_Traits>& 
directx_shader_t<Shader_Traits>::operator=(
    directx_shader_t<Shader_Traits>&& rval
    ) {
    bytecode_ = std::move(rval.bytecode_);
    handle_ = std::move(rval.handle_);
    uniform_blocks_ = std::move(rval.uniform_blocks_);
    uniform_block_binding_list_ = std::move(rval.uniform_block_binding_list_);
    uniforms_ = std::move(rval.uniforms_);
    sampler_states_ = std::move(rval.sampler_states_);
    sampler_state_binding_list_ = std::move(rval.sampler_state_binding_list_);
    name_ = std::move(rval.name_);
    return *this;
}

template<typename Shader_Traits>
inline typename directx_shader_t<Shader_Traits>::uniform_block_list_iterator_t
directx_shader_t<Shader_Traits>::find_uniform_block_by_name(
    const char* block_name
    ) {
    assert(block_name);
    assert(handle_);

    return binary_search_container(
        uniform_blocks_, 
        [block_name](const shader_uniform_block_t& ublock){
            return strcmp(ublock.get_name().c_str(), block_name);
    });
}

template<typename Shader_Traits>
inline typename directx_shader_t<Shader_Traits>::uniform_list_iterator_t
directx_shader_t<Shader_Traits>::find_uniform_by_name(
    const char* uniform_name
    ) {
    assert(uniform_name);
    assert(handle_);

    return binary_search_container(
        uniforms_, 
        [uniform_name](const shader_uniform_t& unif) {
            return strcmp(unif.name_.c_str(), uniform_name);
    });
}

template<typename Shader_Traits>
void directx_shader_t<Shader_Traits>::reflect_constant_buffer(
    const renderer* R,
    ID3D11ShaderReflection* S_reflector, 
    uint32_t index
    ) {
    ID3D11ShaderReflectionConstantBuffer* constbuffer_reflector = 
        S_reflector->GetConstantBufferByIndex(index);
    if (!constbuffer_reflector) {
        OUTPUT_DBG_MSGW(L"Warning : failed to reflect constant buffer @ slot %ud",
                        index);
        return;
    }

    D3D11_SHADER_BUFFER_DESC sbd;
    HRESULT ret_code;
    CHECK_D3D(&ret_code, constbuffer_reflector->GetDesc(&sbd));
    if (FAILED(ret_code)) 
        return;

    uniform_blocks_.push_back(shader_uniform_block_t());

    if (!uniform_blocks_.back().initialize(R, sbd, index)) {
        uniform_blocks_.pop_back();
        return;
    }

    uniform_block_binding_list_.push_back(uniform_blocks_.back().get_handle());

    for (uint32_t i = 0; i < sbd.Variables; ++i) {
        auto V_reflector = constbuffer_reflector->GetVariableByIndex(i);
        if (!V_reflector) {
            OUTPUT_DBG_MSGA("Warning : failed to reflect variable @ %ud "
                            "for constant buffer %s",
                            i, sbd.Name);
            return;
        }

        D3D11_SHADER_VARIABLE_DESC var_desc;
        CHECK_D3D(&ret_code, V_reflector->GetDesc(&var_desc));
        if (FAILED(ret_code))
            continue;

        uniforms_.push_back(shader_uniform_t(&uniform_blocks_.back(), var_desc));
    }
}

template<typename Shader_Traits>
void directx_shader_t<Shader_Traits>::reflect_bound_resource(
    const renderer*, 
    ID3D11ShaderReflection* S_reflector, 
    uint32_t index
    ) {
    D3D11_SHADER_INPUT_BIND_DESC bound_res_desc;
    HRESULT ret_code;
    CHECK_D3D(&ret_code, S_reflector->GetResourceBindingDesc(
        index, &bound_res_desc));
    if (FAILED(ret_code))
        return;

    if (bound_res_desc.Type == D3D10_SIT_SAMPLER) {
        sampler_states_.push_back(sampler_state_t(bound_res_desc.Name,
                                                  bound_res_desc.BindPoint));
    } else if (bound_res_desc.Type == D3D10_SIT_TEXTURE) {
        resource_views_.push_back(
            resource_view_t(bound_res_desc.Name, bound_res_desc.BindPoint, 
                            bound_res_desc.Dimension, bound_res_desc.BindCount));
    }
}

template<typename Shader_Traits>
bool directx_shader_t<Shader_Traits>::reflect_shader(
    const renderer* R
    ) {
    assert(bytecode_);
    
    using namespace v8::base;
    scoped_ptr<ID3D11ShaderReflection, com_storage> S_reflector;
    HRESULT ret_code;
    CHECK_D3D(
        &ret_code,
        ::D3DReflect(bytecode_->GetBufferPointer(), bytecode_->GetBufferSize(),
                     IID_ID3D11ShaderReflection,
                     reinterpret_cast<void**>(scoped_pointer_get_impl(S_reflector))));

    if (FAILED(ret_code))
        return false;

    D3D11_SHADER_DESC S_description;
    CHECK_D3D(&ret_code, S_reflector->GetDesc(&S_description));
    if (FAILED(ret_code))
        return false;

    for (uint32_t i = 0; i < S_description.ConstantBuffers; ++i) {
        reflect_constant_buffer(R, scoped_pointer_get(S_reflector), i);
    }

    for (uint32_t i = 0; i < S_description.BoundResources; ++i) {
        reflect_bound_resource(R, v8::base::scoped_pointer_get(S_reflector), i);
    }

    //
    // 
    // Sort all resources by name, so we can binary search them.
    std::sort(
        std::begin(uniform_blocks_),
        std::end(uniform_blocks_),
        [](const shader_uniform_block_t& left, const shader_uniform_block_t& right) {
            return left.name_ < right.name_;
    });

    std::sort(
        std::begin(uniforms_),
        std::end(uniforms_),
        [](const shader_uniform_t& lhs, const shader_uniform_t& rhs) {
            return lhs.name_ < rhs.name_;
    });

    std::sort(
        std::begin(sampler_states_),
        std::end(sampler_states_),
        [](const sampler_state_t& lhs, const sampler_state_t& rhs) {
            return lhs.name_ < rhs.name_;
    });

    std::sort(
        std::begin(resource_views_),
        std::end(resource_views_),
        [](const resource_view_t& lhs, const resource_view_t& rhs) {
            return lhs.rvt_name < rhs.rvt_name;
    });
    
    sampler_state_binding_list_.resize(sampler_states_.size());
    resource_view_binding_list_.resize(resource_views_.size());
    return true;
}

template<typename Shader_Traits>
bool directx_shader_t<Shader_Traits>::create_shader_from_file(
    const renderer* R, 
    const char* file_name, 
    const char* entry_point, 
    Shader_Model shader_model, 
    uint32_t compile_flags
    ) {
    using namespace v8::base;
    scoped_pointer_reset(bytecode_);
    scoped_pointer_reset(handle_);
    uniform_blocks_.clear();
    uniforms_.clear();
    sampler_states_.clear();
    sampler_state_binding_list_.clear();
    resource_views_.clear();
    resource_view_binding_list_.clear();

    bytecode_ = compile_shader_into_bytecode(
        file_name, entry_point, shader_traits_t::Profile_Strings[shader_model],
        compile_flags);
    if (!bytecode_)
        return false;

    handle_ = shader_traits_t::create_shader(R, scoped_pointer_get(bytecode_));
    if (!handle_)
        return false;

    return reflect_shader(R);
}

template<typename Shader_Traits>
void directx_shader_t<Shader_Traits>::bind_to_pipeline(
    const renderer* R
    ) {
    assert(bytecode_);
    assert(handle_);

    //
    // Bind uniforms.
    if (!uniform_block_binding_list_.empty()) {
        //
        // Sync uniform values with values stored in GPU memory
        std::for_each(
            std::begin(uniform_blocks_), std::end(uniform_blocks_), 
            [R](shader_uniform_block_t& ufb) {
                ufb.sync_data_with_gpu(R);
        });

        shader_traits_t::bind_uniform_blocks_to_pipeline_stage(
            R, 0, uniform_block_binding_list_);
    }

    //
    // Bind samplers.
    if (!sampler_state_binding_list_.empty())
        shader_traits_t::bind_sampler_states_to_pipeline_stage(
            R, 0, sampler_state_binding_list_);

    //
    // Bind resource views.
    if (!resource_view_binding_list_.empty())
        shader_traits_t::bind_resource_views_to_pipeline_stage(
            R, 0, resource_view_binding_list_);

    //
    // Finally, bind the shader object.
    shader_traits_t::bind_shader_to_pipeline_stage(
        R, v8::base::scoped_pointer_get(handle_));
}

template<typename Shader_Traits>
template<typename Uniform_Block_Type>
void directx_shader_t<Shader_Traits>::set_uniform_block_by_name(
    const char* block_name, 
    const Uniform_Block_Type& uniform_data
    ) {
    assert(handle_);
    assert(block_name);

    auto itr_uniblock = find_uniform_block_by_name(block_name);
    if (itr_uniblock == std::end(uniform_blocks_)) {
        OUTPUT_DBG_MSGA("Trying to set non existent uniform block [%s]",
                        block_name);
        return;
    }

    itr_uniblock->set_block_data(uniform_data);
}

template<typename Shader_Traits>
uint32_t directx_shader_t<Shader_Traits>::get_uniform_block_location_by_name(
    const char* block_name
    ) {
    assert(handle_);
    assert(block_name);

    auto itr_uniblock = find_uniform_block_by_name(block_name);
    if (itr_uniblock == std::end(uniform_blocks_)) {
        OUTPUT_DBG_MSGA("Trying to query location for non-existent uniform block [%s]",
                        block_name);
        return Invalid_Uniform_Location;
    }

    return itr_uniblock->get_bind_point();
}

template<typename Shader_Traits>
template<typename Uniform_Type>
inline void directx_shader_t<Shader_Traits>::set_uniform_by_name(
    const char* uniform_name, 
    const Uniform_Type& uniform_data
    ) {
    set_raw_uniform_by_name(uniform_name, &uniform_data, sizeof(uniform_data));
}

template<typename Shader_Traits>
void directx_shader_t<Shader_Traits>::set_raw_uniform_by_name(
    const char* uniform_name, 
    const void* uniform_data, 
    uint32_t byte_count
    ) {
    assert(uniform_name);
    assert(handle_);

    auto itr_uniform = find_uniform_by_name(uniform_name);
    if (itr_uniform == std::end(uniforms_)) {
        OUTPUT_DBG_MSGA("Trying to query non-existent uniform [%s]", 
            uniform_name);
        return;
    }

    itr_uniform->set_raw_value(uniform_data, byte_count);
}

template<typename Shader_Traits>
uint32_t directx_shader_t<Shader_Traits>::get_uniform_location_by_name(
    const char* uniform_name
    ) {
    assert(uniform_name);
    assert(handle_);

    auto itr_uniform = find_uniform_by_name(uniform_name);
    if (itr_uniform == std::end(uniforms_)) {
        OUTPUT_DBG_MSGA("Trying to query non-existent uniform [%s]", 
                        uniform_name);
        return;
    }
    return itr_uniform->start_offset_;
}

template<typename Shader_Traits>
void directx_shader_t<Shader_Traits>::set_sampler(
    const char* sampler_name, 
    ID3D11SamplerState* samp_state
    ) {
    auto itr_descriptor = binary_search_container(
        sampler_states_, 
        [sampler_name](const sampler_state_t& sstate) {
            return !strcmp(sstate.name_.c_str(), sampler_name);
    });

    if (itr_descriptor != std::end(sampler_states_)) {
        sampler_state_binding_list_[itr_descriptor->bindpoint_] = samp_state;
        return;
    }

    OUTPUT_DBG_MSGA("Trying to set inexistent sampler state [%s]", 
                    smp.get_name().c_str());
}

template<typename Shader_Traits>
void directx_shader_t<Shader_Traits>::set_texture(
    const char* texture_object_name, 
    ID3D11ShaderResourceView* rsrc_view
    ) {
    assert(handle_);
    assert(texture_object_name);

    auto itr_texdata = binary_search_container(
        resource_views_,
        [texture_object_name](const resource_view_t& rsrc_data) {
            return strcmp(rsrc_data.rvt_name.c_str(), texture_object_name);
    });

    if (itr_texdata == std::end(resource_views_)) {
        OUTPUT_DBG_MSGA("Trying to bind non existent Texture2D, [%s]",
                        texture_object_name);
        return;
    }

    resource_view_binding_list_[itr_texdata->rvt_bindpoint] = rsrc_view;
}

typedef directx_shader_t<shader_traits<ID3D11VertexShader>> vertex_shader_t;

typedef directx_shader_t<shader_traits<ID3D11GeometryShader>> geometry_shader_t;

typedef directx_shader_t<shader_traits<ID3D11PixelShader>> pixel_shader_t;
