#pragma once

#include <vector>
#include <v8/math/vector2.h>
#include <v8/math/vector3.h>

#include "vertex_formats.h"

class terrain_generator {
public :
    typedef vertex_PNT  vertex_type;

private :
    std::vector<vertex_type>            terrain_vertices_;
    std::vector<unsigned int>           index_list_;
    unsigned                            face_count_;

    void compute_normals();

public :
    terrain_generator() : face_count_(0) {}

    template<typename Generator>
    void generate_terrain(
        float dimensionX,
        float dimensionY,
        float cellSizeX,
        float cellSizeY,
        Generator gen_fn
        );

    const std::vector<vertex_type>& get_vertex_list() const {
        return terrain_vertices_;
    }

    const std::vector<unsigned int>& get_index_list() const {
        return index_list_;
    }

    unsigned int get_face_count() const {
        return face_count_;
    }
};

template<typename Generator>
void terrain_generator::generate_terrain(
    float dimensionX, 
    float dimensionY, 
    float cellSizeX, 
    float cellSizeY, 
    Generator gen_fn
    ) {
    terrain_vertices_.clear();

    const float kHalfWidth = (dimensionX - 1) * cellSizeX * 0.5f;
    const float kHalfHeight = (dimensionY - 1) * cellSizeY * 0.5f;

    const float delta_u = 1.0f / (dimensionX - 1);
    const float delta_v = 1.0f / (dimensionY - 1);

    for (unsigned int i = 0; i < dimensionX; ++i) {
        const float x_coord = i * cellSizeX - kHalfWidth;

        for (unsigned int j = 0; j < dimensionY; ++j) {
            const float z_coord = j * cellSizeY - kHalfHeight;

            const float y_coord = gen_fn.gen_y_coord(x_coord, z_coord);

            vertex_type new_vertex = {
                v8::math::vector3F(x_coord, y_coord, z_coord), 
                v8::math::vector3F::zero,
                v8::math::vector2F(i * delta_u, j * delta_v)
            };
            terrain_vertices_.push_back(new_vertex);
        }
    }

    index_list_.clear();

    for (unsigned int i = 0; i < dimensionX - 1; ++i) {
        for (unsigned int j = 0; j < dimensionY - 1; ++j) {
            index_list_.push_back((i + 1) * dimensionY + j);
            index_list_.push_back(i * dimensionY + j);
            index_list_.push_back(i * dimensionY + j + 1);

            index_list_.push_back((i + 1) * dimensionY + j);
            index_list_.push_back(i * dimensionY + j + 1);
            index_list_.push_back((i + 1) * dimensionY + j + 1);
        }
    }

    face_count_ = (dimensionX - 1) * (dimensionY - 1) * 2;

    std::for_each(
        std::begin(terrain_vertices_), std::end(terrain_vertices_),
        [&gen_fn](vertex_type& tv) {
            tv.vt_norm = gen_fn.gen_vertex_normal(tv.vt_pos);
    });
}

struct simple_terrain_generator {
    //
    // f(x, z) = 0.3(z*sin(0.1*x) + x*cos(0.1f*z))
    float gen_y_coord(float x, float z) const {
        return 0.3f * (z * sin(0.1f * x) + x * cos(0.1f * z));
    }

    //
    // df/dx = 0.3f*(0.1f*z*cos(0.1f*x) + cos(0.1f*z))
    // df/dz = 0.3f*(sin(0.1f*x) + 0.1f*x*sin(0.1f*z))
    // vt_x = (1.0f, df/dx, 0.0f)
    // vt_z = (0.0d, df/dz, 1.0f)
    // normal = cross(vt_z, vt_x)
    v8::math::vector3F gen_vertex_normal(const v8::math::vector3F& pos) {
       v8::math::vector3F result;
       result.x_ = -0.03f * pos.z_ * cos(0.1f * pos.x_) - 0.3f * cos(0.1f * pos.z_);
       result.y_ = 1.0f;
       result.z_ = -0.3f * sin(0.1f * pos.x_) + 0.03f * pos.x_ * sin(0.1f * pos.z_);

       return result;
    }
};