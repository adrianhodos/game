#pragma once

#include <v8/base/scoped_pointer.h>
#include <v8/math/light.h>
#include "scene_light_controller.h"

class scene_graph;

class scene_light {
private :
    v8::math::light                                 light_;
    bool                                            active_;
    v8::base::scoped_ptr<scene_light_controller>    controller_;

public :
    scene_light();

    scene_light(
        const v8::math::light& light, 
        bool active = true,
        scene_light_controller* controller = nullptr
        );

    ~scene_light();

    v8::math::light* get_light() {
        return &light_;
    }

    const v8::math::light* get_light() const {
        return &light_;
    }

    void set_light(const v8::math::light& light) {
        light_ = light;
    }

    void set_on() {
        active_ = true;
    }

    void set_off() {
        active_ = false;
    }

    bool is_on() const {
        return active_;
    }

    scene_light_controller* get_controller() {
        return v8::base::scoped_pointer_get(controller_);
    }

    void set_controller(scene_light_controller* controller) {
        v8::base::scoped_pointer_reset(controller_, controller);
    }

    void update(scene_graph* sg, float delta_ms);
};
