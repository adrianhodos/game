#pragma once

#include <cstdarg>
#include <cstdint>
#include <stack>
#include <string>
#include <vector>

namespace tools { namespace mesh_loaders {

struct obj_face_t {
    static const int32_t kCoordSysMappings[2][4];

    uint8_t     group_id;
    int32_t     indices[4 * 3];
    int32_t     elements;
    
    obj_face_t(uint8_t grp_id = 0) : group_id(grp_id), elements(0) {}
    
    void add_element(int32_t val, uint8_t offset) {
        indices[offset * 4 + elements] = val;
    }
    
    int32_t get_element(uint8_t offset, int32_t pos) const {
        return indices[offset * 4 + pos];
    }
    
    void add_vertex(int32_t val) {
        add_element(val, 0);
    }
    
    void add_texcoord(int32_t val) {
        add_element(val, 1);
    }
    
    void add_normal(int32_t val) {
        add_element(val, 2);
    }
    
    int32_t get_vertex(int32_t pos) const {
        return get_element(0, pos);
    }
    
    int32_t get_texcoord(int32_t pos) const {
        return get_element(1, pos);
    }
    
    int32_t get_normal(int32_t pos) const {
        return get_element(2, pos);
    }
    
    int32_t element_count() const {
        return elements;
    }
    
    void increment_element_count() {
        ++elements;
    }

    uint8_t get_group_id() const {
        return group_id;
    }
};

struct obj_group_t {
    std::string group_name;
    uint8_t     group_id;
    int32_t     offset_vertex;
    int32_t     offset_texcoord;
    int32_t     offset_normals;
    int32_t     vertex_count;
    int32_t     texcoord_count;
    int32_t     normal_count;
    int32_t     face_count;

    void init_count() {
        vertex_count = texcoord_count = normal_count = face_count = 0;
    }
    
    obj_group_t() 
        : group_name("default"), group_id(0), offset_vertex(0), offset_texcoord(0), 
          offset_normals(0) {
        init_count();
    }
    
    obj_group_t(const char* name, uint8_t id, int32_t start_vertex, 
                int32_t start_texcoord, int32_t start_normals) 
        : group_name(name), group_id(id), offset_vertex(start_vertex), 
          offset_texcoord(start_texcoord), offset_normals(start_normals) {
        init_count();
    }
};

struct vertex2F {
    union {
        struct {
            float x_;
            float y_;
        };
        float e_[2];
    };
    
    vertex2F() {}
    
    vertex2F(float x, float y) : x_(x), y_(y) {}
};

struct vertex3F {
    union {
        struct {
            float x_;
            float y_;
            float z_;
        };
        float e_[2];
    };
    
    vertex3F() {}
    
    vertex3F(float x, float y, float z) : x_(x), y_(y), z_(z) {}
};

class wavefront_loader {
public :

    enum Mesh_Format {
        Mesh_Fmt_VertexData = 1 << 0,
        Mesh_Fmt_TexcoordData = 1 << 1,
        Mesh_Fmt_NormalData = 1 << 2
    };

    std::vector<vertex3F>   vertices_;
    std::vector<vertex3F>   normals_;
    std::vector<vertex2F>   texcoords_;
    std::vector<obj_face_t> faces_;
    std::stack<obj_group_t> groups_;
    
private :
    struct file_deleter_t {
        void operator()(FILE* fp) const {
            if (fp)
                fclose(fp);
        }
    };
    
    uint32_t                mesh_dataflags_;
    bool                    parsing_error_;
    std::string             err_info_;
    
    void reset_state() {
        mesh_dataflags_ = Mesh_Fmt_TexcoordData;
        vertices_.clear();
        normals_.clear();
        texcoords_.clear();
        faces_.clear();
        while (!groups_.empty())
            groups_.pop();
        parsing_error_ = false;
        err_info_.clear();        
    }
    
    char* skip_whitespaces(char* p) {
        while (*p && isspace(*p))
            ++p;
        return p;
    }
    
    char* trim_whitespaces(char* p) {
        char* pfirst = p;
        char* plast = p;

        while (*pfirst) {
            if (!isspace(*pfirst))
                plast = pfirst + 1;
            ++pfirst;
        }

        *plast = '\0';
        return p;
    }
    
    void set_error(const char* desc, ...) {
        va_list args_ptr;
        va_start(args_ptr, desc);
        
        char err_msg[2048];
        _snprintf(err_msg, _countof(err_msg), desc, args_ptr);
        va_end(args_ptr);
        
        parsing_error_ = true;
        err_info_ = err_msg;
    }
    
    int32_t read_int(char** lptr);
    
    uint32_t read_floats(char* lptr, float* out_data, uint32_t max_count);
    
    void parse_material_decl(char* line);
    
    void parse_group_decl(char* line);
    
    void parse_face_decl(char* line);
    
    void parse_vertex_decl(char* line);
    
    void parse_line(char* line);
    
public :
    wavefront_loader() {
        reset_state();
    }
    
    bool has_texcoords_data() const {
        return !texcoords_.empty();
    }
    
    bool has_normals_data() const {
        return !normals_.empty();
    }
    
    bool has_parse_error() const {
        return parsing_error_;
    }
    
    const std::string& get_error_desc() const {
        return err_info_;
    }
    
    void parse_file(const char* file_name);

    template<typename face_visitor>
    void visit_faces(face_visitor& visitor);
};

template<typename face_visitor>
void wavefront_loader::visit_faces(face_visitor& visitor) {
    uint32_t i = 0;
    while (i < faces_.size()) {
        visitor.visit_face(faces_[i++]);
    }
}

}
}
