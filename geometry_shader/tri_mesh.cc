#include "pch_hdr.h"

#include <v8/base/debug_helpers.h>
#include <v8/math/color.h>
#include <v8/math/light.h>
#include <v8/math/matrix4x4.h>
#include <v8/math/vector3.h>

#include "directx_utils.h"
#include "draw_context.h"
#include "ifs_model_loader.h"
#include "renderer.h"
#include "tri_mesh.h"

namespace {

struct vertex_t {
    v8::math::vector3F  vt_pos;
    v8::math::vector3F  vt_norm;
};

struct cbuff_vxshader_t {
    v8::math::matrix_4X4F   world_view_projection;
    v8::math::matrix_4X4F   world;
};

struct cbuff_fragshader_t {
    v8::math::light         light;
    v8::math::vector3F      eye_pos;
    float                   pad__;
};

static_assert((sizeof(cbuff_fragshader_t) % 16 == 0), "must be multiple of 16");

void compute_surface_normals(
    vertex_PNDS* model_data,
    unsigned int vertex_count,
    const unsigned* indices,
    unsigned int face_count
    )
{
    for (unsigned int i = 0; i < face_count; ++i) {
        vertex_PNDS& v0 = model_data[indices[i * 3]];
        vertex_PNDS& v1 = model_data[indices[i * 3 + 1]];
        vertex_PNDS& v2 = model_data[indices[i * 3 + 2]];

        const v8::math::vector3F e0 = v1.vt_pos - v0.vt_pos;
        const v8::math::vector3F e1 = v2.vt_pos - v0.vt_pos;
        const v8::math::vector3F poly_normal = v8::math::cross_product(e0, e1);

        v0.vt_norm += poly_normal;
        v1.vt_norm += poly_normal;
        v2.vt_norm += poly_normal;
    }

    for (unsigned int i = 0; i < vertex_count; ++i) {
        model_data[i].vt_norm.normalize();
    }
}

}

ifs_mesh::ifs_mesh() : vtx_shader_("basic_vsh"), frag_shader_("light") {}

ifs_mesh::~ifs_mesh() {}

bool ifs_mesh::setup_shaders_and_input_layout(const directx_renderer* rnd) {
    const char* const kVXShaderFile = 
        "C:\\shadercache\\illumination\\vertex_shader.hlsl";
    const unsigned int kFlagsCompile = 
        D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION 
        | D3DCOMPILE_ENABLE_STRICTNESS 
        | D3DCOMPILE_PACK_MATRIX_ROW_MAJOR;

    if (!vtx_shader_.create_shader_from_file(
            rnd, kVXShaderFile, "vs_main", 
            vertex_shader_t::Shader_Model_5_0, 
            kFlagsCompile))
        return false;

    D3D11_INPUT_ELEMENT_DESC vertex_format_desc[] = {
        {
            "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 
            D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
        },
        {
            "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,
            D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
        },
        {
            "DIFFUSE", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,
            D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
        },
        {
            "SPECULAR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,
            D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
        }
    };

    HRESULT ret_code;
    CHECK_D3D(
        &ret_code,
        rnd->get_device()->CreateInputLayout(
            vertex_format_desc, _countof(vertex_format_desc),
            vtx_shader_.get_compiled_bytecode()->GetBufferPointer(),
            vtx_shader_.get_compiled_bytecode()->GetBufferSize(),
            scoped_pointer_get_impl(input_layout_)));

    if (FAILED(ret_code))
        return false;

    const char* const kPXShaderFile = 
        "C:\\shadercache\\illumination\\pixel_shader.hlsl";
    if (!frag_shader_.create_shader_from_file(
            rnd, kPXShaderFile, "ps_main", 
            pixel_shader_t::Shader_Model_5_0,
            kFlagsCompile))
        return false;

    D3D11_RASTERIZER_DESC raster_desc = {
        D3D11_FILL_WIREFRAME,
        D3D11_CULL_BACK,
        false,
        0.0f,
        0.0f,
        0.0f,
        true,
        false,
        false,
        false
    };

    CHECK_D3D(
        &ret_code,
        rnd->get_device()->CreateRasterizerState(
            &raster_desc, scoped_pointer_get_impl(raster_wireframe_)));

    return ret_code == S_OK;
}

bool ifs_mesh::initialize_mesh(
    const directx_renderer* rnd, 
    const char* model_file
    ) {
    IFSLoader model_loader;
    if (!model_loader.loadModel(model_file))
        return false;

    using namespace v8::base;
    using namespace v8::math;
    using namespace std;

    vector<vertex_PNDS> vertex_data;
    vertex_data.reserve(model_loader.getVertexCount());

    transform(
        model_loader.getVertexListPointer(),
        model_loader.getVertexListPointer() + model_loader.getVertexCount(),
        back_inserter(vertex_data),
        [](const vector3F& in_data) -> vertex_PNDS {
            vertex_PNDS out_data = { in_data, v8::math::vector3F::zero };
            out_data.vt_pos *= 10.0f;
            out_data.vt_pos.z_ *= -1.0f;
            out_data.vt_norm = v8::math::vector3F::zero;
            if (out_data.vt_pos.y_ < 0.0f) {
                out_data.vt_diffuse = v8::math::color::AliceBlue;
            } else {
                out_data.vt_diffuse = v8::math::color::DarkKhaki;
            }

            out_data.vt_specular = v8::math::color::White;
            return out_data;
    });

    vector<unsigned> index_data;
    index_data.reserve(model_loader.getIndexCount());

    for (unsigned int i = 0; i < model_loader.getIndexCount() / 3; ++i) {
        index_data.push_back(model_loader.getIndexListPointer()[i * 3 + 2]);
        index_data.push_back(model_loader.getIndexListPointer()[i * 3 + 1]);
        index_data.push_back(model_loader.getIndexListPointer()[i * 3 + 0]);
    }

    compute_surface_normals(&vertex_data[0], vertex_data.size(), &index_data[0], 
                            model_loader.getFaceCount());

    if (!idx_buffer_.initialize(rnd, index_data.size(), &index_data[0]))
        return false;
    if (!vtx_buffer_.initialize(rnd, sizeof(vertex_data[0]) * vertex_data.size(),
                                vertex_data.size(), &vertex_data[0]))
        return false;

    return setup_shaders_and_input_layout(rnd);
}

bool ifs_mesh::initialize_mesh(
    const directx_renderer* rnd,
    const std::vector<vertex_PN>& vertices, 
    const std::vector<unsigned>& indices
    )
{
    using namespace v8::base;
    using namespace v8::math;
    using namespace std;

    vector<vertex_PNDS> model_vertices;
    model_vertices.reserve(vertices.size());
    transform(
        begin(vertices), end(vertices),
        back_inserter(model_vertices),
        [](const vertex_PN& vt_pn) -> vertex_PNDS {
            vertex_PNDS output_vertex;
            output_vertex.vt_pos = vt_pn.vt_pos;
            output_vertex.vt_norm = vt_pn.vt_norm;

            if (output_vertex.vt_pos.y_ < -10.0f)
                output_vertex.vt_diffuse = v8::math::color::SandyBrown;
            else if (output_vertex.vt_pos.y_ < 5.0f)
                output_vertex.vt_diffuse = v8::math::color::YellowGreen;
            else if (output_vertex.vt_pos.y_ < 12.0f)
                output_vertex.vt_diffuse = v8::math::color::DarkSeaGreen;
            else if (output_vertex.vt_pos.y_ < 20.0f)
                output_vertex.vt_diffuse = v8::math::color::Brown;
            else
                output_vertex.vt_diffuse = v8::math::color::White;

            output_vertex.vt_specular = v8::math::color::White;
            output_vertex.vt_specular.a_ = 128;

            return output_vertex;
    });

    if (!vtx_buffer_.initialize(rnd, model_vertices.size() * sizeof(model_vertices[0]), 
                                model_vertices.size(), &model_vertices[0]))
        return false;

    vector<unsigned> model_indices;
    model_vertices.reserve(indices.size());
    transform(
        begin(indices), end(indices),
        back_inserter(model_indices),
        [](unsigned int idx_u32) {
            return (idx_u32);
    });

    if (!idx_buffer_.initialize(rnd, model_indices.size(), &model_indices[0]))
        return false;

    return setup_shaders_and_input_layout(rnd);
}

void ifs_mesh::draw(const draw_context_t* context, const directx_renderer* R) {
    using namespace v8::base;
    using namespace v8::math;

    unsigned offsets = 0;
    unsigned strides = sizeof(vertex_PNDS);

    R->IA_Stage_set_vertex_buffers(&vtx_buffer_, 1, &offsets, &strides);
    R->IA_Stage_set_index_buffer(&idx_buffer_);
    R->get_device_context()->IASetInputLayout(scoped_pointer_get(input_layout_));
    R->get_device_context()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

    cbuff_vxshader_t vertex_shader_constants = {
        context->projViewMatrix,
        matrix_4X4F::identity
    };

    vtx_shader_.set_uniform_block_by_name("globals", vertex_shader_constants);

    cbuff_fragshader_t fragment_shader_constants = {
        context->light,
        context->eye_pos
    };

    frag_shader_.set_uniform_block_by_name("globals", fragment_shader_constants);

    vtx_shader_.bind_to_pipeline(R);
    frag_shader_.bind_to_pipeline(R);
    R->draw_indexed(idx_buffer_.get_element_count());
}

void ifs_mesh::update(const draw_context_t*) {}

