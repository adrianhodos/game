#pragma once

#include "buffer_core.h"

template<unsigned index_type>
struct index_traits;

template<>
struct index_traits<16> {
    typedef unsigned short index_type_t;

    enum {
        index_size = sizeof(index_type_t)
    };

    static DXGI_FORMAT get_dxgi_format() {
        return DXGI_FORMAT_R16_UINT;
    }
};

template<>
struct index_traits<32> {
    typedef unsigned int index_type_t;

    enum {
        index_size = sizeof(index_type_t)
    };

    static DXGI_FORMAT get_dxgi_format() {
        return DXGI_FORMAT_R32_UINT;
    }
};

template<typename idx_traits>
class index_buffer : private buffer_core {
private :
public :
    typedef idx_traits  index_traits_t;
    typedef typename index_traits_t::index_type_t   index_type_t;

    index_buffer() 
        : buffer_core() {}

    index_buffer(index_buffer<idx_traits>&& rval) {
        buffer_core::buffer_core(std::move(rval));
    }

    index_buffer<idx_traits>& operator=(index_buffer<idx_traits>&& rval) {
        buffer_core::operator=(std::move(rval));
        return *this;
    }

    index_buffer(
        const renderer* rnd,
        unsigned int elements,
        const index_type_t* initial_data = nullptr,
        Buffer_Usage_Flags use_flags = Buffer_Use_GPU_RDONLY,
        Resource_CPU_Access cpu_access = Resource_CPU_NoAccess
        ) : buffer_core(rnd, idx_traits::index_size, elements, 
                        Buffer_Type_Index, use_flags, cpu_access, initial_data) {
    }

    bool initialize(
        const renderer* rnd, 
        unsigned int elements,
        const index_type_t* initial_data = nullptr,
        Buffer_Usage_Flags use_flags = Buffer_Use_GPU_RDONLY,
        Resource_CPU_Access cpu_access = Resource_CPU_NoAccess
        )
    {
        return buffer_core::initialize(
            rnd, idx_traits::index_size, elements, Buffer_Type_Index,
            use_flags, cpu_access, initial_data
            );
    }

    using buffer_core::get_element_count;
    using buffer_core::get_buffer_size;
    using buffer_core::get_handle;
    using buffer_core::update_data;
    using buffer_core::operator int buffer_core::helper_t::*;
    using buffer_core::operator!;
};

typedef index_buffer<index_traits<16>> index_buffer16_t;

typedef index_buffer<index_traits<32>> index_buffer32_t;
