#pragma once

#include "index_buffer.h"
#include "vertex_buffer.h"

struct mesh_info_t {
    vertex_buffer*      vbuff;
    index_buffer32_t*   ibuff;

    mesh_info_t() {
        vbuff = nullptr;
        ibuff = nullptr;
    }
};