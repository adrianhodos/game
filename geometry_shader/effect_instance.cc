#include "pch_hdr.h"
#include "effect_pass.h"
#include "shader_constant.h"
#include "shader_data_provider.h"
#include "renderer.h"

#include "effect_instance.h"

namespace {

//void VS_BindData(vertex_shader_t* vsh, const shader_constant& cs) {
//    switch (cs.u_type) {
//    case Type_Maxtrix4X4 :
//        vsh->set_uniform_by_name(cs.u_name, *(const v8::math::matrix_4X4F*) cs.u_mat4x4);
//        break;
//
//    case Type_Float :
//        vsh->set_uniform_by_name(cs.u_name, cs.u_fscalar);
//        break;
//    }
//}

}

void effect_instance::bind_to_pipeline(renderer* R) {
    //
    // Query shader constants from provider if needed
    if (data_provider_) {
        auto data_to_bind = data_provider_->get_data();
        auto data_ptr = std::get<0>(data_to_bind);
        auto data_cnt = std::get<1>(data_to_bind);
        for (uint32_t i = 0; i < data_cnt; ++i) {
            const shader_constant& sc = data_ptr[i];
            if (sc.u_bindtype & Bind_Vertex_Shader) {
                efp_->get_vertex_shader()->set_raw_uniform_by_name(
                    sc.u_name, (const void*) &sc, sc.u_size);
            }

            if ((sc.u_bindtype & Bind_Geometry_Shader) && efp_->get_geometry_shader()) {
                efp_->get_geometry_shader()->set_raw_uniform_by_name(
                    sc.u_name, (const void*) &sc, sc.u_size);
            }

            if (sc.u_bindtype & Bind_Pixel_Shader) {
                efp_->get_pixel_shader()->set_raw_uniform_by_name(
                    sc.u_name, (const void*) &sc, sc.u_size);
            }
        }
    }

    R->RS_stage_set_state(raster_state_);
    efp_->bind_to_pipeline(R);
}