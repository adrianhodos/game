#include "pch_hdr.h"
//
//#include <v8/base/debug_helpers.h>
//#include <v8/math/matrix4X4.h>
//#include <v8/math/vector3.h>
//
//#include "directx_utils.h"
//#include "draw_context.h"
//#include "renderer.h"
//#include "mandelbrot.h"
//
//namespace {
//
//struct vertex_t {
//    v8::math::vector3F  vx_pos;
//};
//
//struct cbuff_vxshader_t {
//    v8::math::matrix_4X4F   world_view_projection;
//};
//
//struct cbuff_pxshader_t {
//    float kDefaultCenterX;
//    float kDefaultCenterY;
//    float kScaleFactor;
//    float kScrWidth;
//    float kScrHeight;
//    unsigned max_iterations;
//    char  pad__[8];
//};
//
//}
//
//fractal::fractal() 
//    : idx_buffer_(),
//      vx_shader_("vs_basic"), 
//      px_shader_("ps_mandelbrot"),
//      iter_max_(500),
//      scale_factor_(1.0f) {
//}
//
//bool fractal::initialize_color_map(const directx_renderer* rnd) {
//    using namespace v8::math;
//    using namespace v8::base;
//    using namespace std;
//
//    vector<v8::math::color> colormap_entries;
//    colormap_entries.reserve(iter_max_);
//
//    unsigned int i = 0;
//
//    for (i = 0; i < iter_max_; ++i) {
//        colormap_entries.push_back(rgb_from_wavelength(
//            380.0f + (i * 400.0f / iter_max_)));
//    }
//
//    D3D11_TEXTURE1D_DESC tex_description = {
//        iter_max_,
//        1,
//        1,
//        DXGI_FORMAT_R32G32B32A32_FLOAT,
//        D3D11_USAGE_IMMUTABLE,
//        D3D11_BIND_SHADER_RESOURCE,
//        0,
//        0
//    };
//
//    D3D11_SUBRESOURCE_DATA tex_data = { &colormap_entries[0], 0, 0 };
//
//    HRESULT ret_code;
//    CHECK_D3D(
//        &ret_code,
//        rnd->get_device()->CreateTexture1D(&tex_description, &tex_data, scoped_pointer_get_impl(color_map_))
//        );
//    if (FAILED(ret_code))
//        return false;
//
//    D3D11_SHADER_RESOURCE_VIEW_DESC resview_desc;
//    resview_desc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT,
//    resview_desc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE1D;
//    resview_desc.Texture1D.MipLevels = 1;
//    resview_desc.Texture1D.MostDetailedMip = 0;
//
//    CHECK_D3D(
//        &ret_code,
//        rnd->get_device()->CreateShaderResourceView(
//            scoped_pointer_get(color_map_), 
//            &resview_desc, 
//            scoped_pointer_get_impl(rsview_colormap_)));
//
//    return ret_code == S_OK;
//}
//
//bool fractal::initialize(const directx_renderer* rnd, float sw, float sh) {
//    using namespace v8::base;
//    using namespace v8::math;
//    using namespace std;
//
//    screen_width_ = sw;
//    screen_height_ = sh;
//
//    world_scale_matrix_.make_scale(screen_width_ / 2, screen_height_ / 2, 1.0f);
//
//    const vertex_t kVerts[] = {
//        { vector3F(-1.0f, -1.0f, 0.0f) }, { vector3F(-1.0f, 1.0f, 0.0f ) },
//        { vector3F(1.0f, 1.0f, 0.0f) }, { vector3F(1.0f, -1.0f, 0.0f) }
//    };
//
//    if (!vx_buffer_.initialize(rnd, 4 * sizeof(vertex_t), sizeof(vertex_t), kVerts))
//        return false;
//
//    const index_buffer16_t::index_type_t indices[] = { 0, 1, 3, 2 };
//    if (!idx_buffer_.initialize(rnd, _countof(indices), indices))
//        return false;
//
//    const char* const kVXShaderFile = "C:\\shadercache\\vs_basic.hlsl";
//    const unsigned kFlagsCompile = D3DCOMPILE_DEBUG 
//        | D3DCOMPILE_ENABLE_STRICTNESS | D3DCOMPILE_WARNINGS_ARE_ERRORS
//        | D3DCOMPILE_SKIP_OPTIMIZATION | D3DCOMPILE_PACK_MATRIX_ROW_MAJOR;
//
//    if (!vx_shader_.CompileShader(rnd, kVXShaderFile, "vs_main", "vs_5_0", 
//                                  kFlagsCompile))
//        return false;
//
//    D3D11_INPUT_ELEMENT_DESC vertex_format_desc[] = {
//        {
//            "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 
//            D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0
//        }
//    };
//
//    HRESULT ret_code;
//    CHECK_D3D(
//        &ret_code,
//        rnd->get_device()->CreateInputLayout(
//            vertex_format_desc, 
//            _countof(vertex_format_desc),
//            vx_shader_.GetCompiledBytecode()->GetBufferPointer(),
//            vx_shader_.GetCompiledBytecode()->GetBufferSize(),
//            scoped_pointer_get_impl(in_layout_)));
//
//    if (FAILED(ret_code))
//        return false;
//
//    const char* const kPSShaderFile = "C:\\shadercache\\ps_mandelbrot.hlsl";
//    if (!px_shader_.CompileShader(rnd, kPSShaderFile, "ps_main", "ps_5_0", 
//                                  kFlagsCompile))
//      return false;
//
//    D3D11_DEPTH_STENCIL_DESC depth_stencil_desc = {
//        true,
//        D3D11_DEPTH_WRITE_MASK_ALL,
//        D3D11_COMPARISON_LESS_EQUAL,
//        false,
//        D3D11_DEFAULT_STENCIL_READ_MASK,
//        D3D11_DEFAULT_STENCIL_WRITE_MASK,
//        D3D11_STENCIL_OP_KEEP,
//        D3D11_STENCIL_OP_KEEP
//    };
//
//    CHECK_D3D(
//        &ret_code,
//        rnd->get_device()->CreateDepthStencilState(
//            &depth_stencil_desc, 
//            scoped_pointer_get_impl(dss_less_eq_)));
//    if (FAILED(ret_code))
//        return false;
//
//    return initialize_color_map(rnd);
//}
//
//void fractal::draw(const draw_context_t* draw_context, const directx_renderer* rnd) {
//    using namespace v8::math;
//    using namespace v8::base;
//
//    cbuff_vxshader_t vxshader_globals = {
//        draw_context->projViewMatrix * matrix_4X4F(world_scale_matrix_)
//    };
//
//    cbuff_pxshader_t psshader_globals = {
//        -0.637011f,
//        -0.0395159f,
//        scale_factor_,
//        screen_width_ / 2,
//        screen_height_ / 2,
//       iter_max_
//    };
//
//    unsigned offsets = 0;
//    unsigned strides = sizeof(vertex_t);
//    rnd->IA_Stage_set_vertex_buffers(&vx_buffer_, 1, &offsets, &strides);
//    rnd->IA_Stage_set_index_buffer(&idx_buffer_);
//    rnd->get_device_context()->IASetInputLayout(
//        v8::base::scoped_pointer_get(in_layout_));
//    rnd->get_device_context()->IASetPrimitiveTopology(
//        D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP
//        );
//    rnd->get_device_context()->OMSetDepthStencilState(
//        scoped_pointer_get(dss_less_eq_), 0
//        );
//
//    vx_shader_.SetUniformByName(rnd, "globals", vxshader_globals);
//    px_shader_.SetUniformByName(rnd, "globals", psshader_globals);
//
//    vx_shader_.BindToPipelineStage(rnd);
//    rnd->get_device_context()->GSSetShader(nullptr, 0, 0);
//    px_shader_.BindToPipelineStage(rnd);
//
//    ID3D11ShaderResourceView* resources[] = { 
//        v8::base::scoped_pointer_get(rsview_colormap_) 
//    };
//
//    rnd->get_device_context()->PSSetShaderResources(
//        0, _countof(resources), resources
//        );
//
//    rnd->get_device_context()->DrawIndexed(
//        idx_buffer_.get_element_count(), 0, 0
//        );
//}
//
//v8::math::color fractal::rgb_from_wavelength(float wave) {
//    float r = 0.0;
//    float g = 0.0;
//    float b = 0.0;
//
//    if (wave >= 380.0 && wave <= 440.0) {
//        r = -1.0 * (wave - 440.0) / (440.0 - 380.0);
//        b = 1.0;
//    } else if (wave >= 440.0 && wave <= 490.0) {
//        g = (wave - 440.0) / (490.0 - 440.0);
//        b = 1.0;
//    } else if (wave >= 490.0 && wave <= 510.0) {
//        g = 1.0;
//        b = -1.0 * (wave - 510.0) / (510.0 - 490.0);
//    } else if (wave >= 510.0 && wave <= 580.0) {
//        r = (wave - 510.0) / (580.0 - 510.0);
//        g = 1.0;
//    } else if (wave >= 580.0 && wave <= 645.0) {
//        r = 1.0;
//        g = -1.0 * (wave - 645.0) / (645.0 - 580.0);
//    } else if (wave >= 645.0 && wave <= 780.0) {
//        r = 1.0;
//    }
//
//    float s = 1.0f;
//    if (wave > 700.0f)
//        s = 0.3f + 0.7f * (780.0f - wave) / (780.0f - 700.0f);
//    else if (wave <  420.0f)
//        s = 0.3f + 0.7f * (wave - 380.0f) / (420.0f - 380.0f);
//
//    r = pow(r * s, 0.8f);
//    g = pow(g * s, 0.8f);
//    b = pow(b * s, 0.8f);
//
//    return v8::math::color(r, g, b);
//}
