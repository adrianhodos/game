#include "pch_hdr.h"

#include <v8/base/count_of.h>
#include <v8/base/crt_handle_policies.h>
#include <v8/base/scoped_handle.h>

#include "app_specific.h"
#include "directx_shader.h"
#include "directx_renderer.h"
#include "resource_manager.h"
#include "scene_graph.h"
#include "simple_mesh.h"
#include "utility.h"
#include "vertex_formats.h"

simple_mesh::simple_mesh(
    const char* model_file, const char* layout_id, scene_graph_node* parent
    ) : scene_graph_node(parent), vbuff_(nullptr), ibuff_(nullptr),
        input_layout_(nullptr), efpass_(), model_file_(model_file),
        layout_name_(layout_id) {}

void simple_mesh::update(scene_graph* s, float delta_ms) {
    super_t::update(s, delta_ms);
}

void simple_mesh::load_mesh_params() {
    using namespace v8::base;

    std::string file_path;
    string_vprintf(&file_path, "%s\\%s\\%s.json", app_dirs::kAppAssetsDir, 
                   app_dirs::kMeshesDir, model_file_.c_str());
    scoped_handle<crt_file_handle> fp(fopen(file_path.c_str(), "r"));
    if (!fp)
        return;

    using namespace rapidjson;
    Document json_doc;
    char rdbuff[10240];
    FileReadStream fsr(v8::base::scoped_handle_get(fp), rdbuff, 
                       v8::base::count_of_array(rdbuff));

    if (json_doc.ParseStream<0, Document::EncodingType>(fsr).HasParseError()) {
        return;
    }

    float model_scale = static_cast<float>(json_doc["scale_factor"].GetDouble());
    local_transform_.set_scale_component(model_scale);
    transform_dirty_flag_ = true;
}

bool simple_mesh::initialize(scene_graph* sg) {
    //vertex_PN vertices[] = {
    //    vertex_PN(-1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f),
    //    vertex_PN(-1.0f,  1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f),
    //    vertex_PN( 1.0f,  1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f),
    //    vertex_PN( 1.0f, -1.0f, -1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f),

    //    // Fill in the back face  data.
    //    vertex_PN(-1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f),
    //    vertex_PN( 1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f),
    //    vertex_PN( 1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f),
    //    vertex_PN(-1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f),

    //    // Fill in the top face  data.
    //    vertex_PN(-1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f),
    //    vertex_PN(-1.0f, 1.0f,  1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f),
    //    vertex_PN( 1.0f, 1.0f,  1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f),
    //    vertex_PN( 1.0f, 1.0f, -1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f),

    //    // Fill in the bottom face  data.
    //    vertex_PN(-1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f),
    //    vertex_PN( 1.0f, -1.0f, -1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f),
    //    vertex_PN( 1.0f, -1.0f,  1.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f),
    //    vertex_PN(-1.0f, -1.0f,  1.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f),

    //    // Fill in the left face  data.
    //    vertex_PN(-1.0f, -1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f),
    //    vertex_PN(-1.0f,  1.0f,  1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f),
    //    vertex_PN(-1.0f,  1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f),
    //    vertex_PN(-1.0f, -1.0f, -1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f),

    //    // Fill in the right face  data.
    //    vertex_PN( 1.0f, -1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f),
    //    vertex_PN( 1.0f,  1.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f),
    //    vertex_PN( 1.0f,  1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f),
    //    vertex_PN( 1.0f, -1.0f,  1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f),
    //};

    //vbuff_ = new vertex_buffer();
    //if (!vbuff_->initialize(sg->get_renderer(), sizeof(vertex_PN), _countof(vertices), vertices))
    //    return false;

    //DWORD i[36];

    //// Fill in the front face index data
    //i[0] = 0; i[1] = 1; i[2] = 2;
    //i[3] = 0; i[4] = 2; i[5] = 3;

    //// Fill in the back face index data
    //i[6] = 4; i[7]  = 5; i[8]  = 6;
    //i[9] = 4; i[10] = 6; i[11] = 7;

    //// Fill in the top face index data
    //i[12] = 8; i[13] =  9; i[14] = 10;
    //i[15] = 8; i[16] = 10; i[17] = 11;

    //// Fill in the bottom face index data
    //i[18] = 12; i[19] = 13; i[20] = 14;
    //i[21] = 12; i[22] = 14; i[23] = 15;

    //// Fill in the left face index data
    //i[24] = 16; i[25] = 17; i[26] = 18;
    //i[27] = 16; i[28] = 18; i[29] = 19;

    //// Fill in the right face index data
    //i[30] = 20; i[31] = 21; i[32] = 22;
    //i[33] = 20; i[34] = 22; i[35] = 23;

    //ibuff_ = new index_buffer32_t();
    //if (!ibuff_->initialize(sg->get_renderer(), _countof(i), (uint32_t*) i))
    //    return false;
    

    mesh_info_t mesh_info = sg->get_rsmgr()->get_mesh(model_file_.c_str());
    if (!mesh_info.vbuff)
        return false;

    vbuff_ = mesh_info.vbuff;
    ibuff_ = mesh_info.ibuff;

    input_layout_ = sg->get_rsmgr()->get_inputlayout(layout_name_.c_str());
    if (!input_layout_)
        return false;

    vertex_shader_t* vsh = sg->get_rsmgr()->get_vertexshader(
        "vs_basic/vs_per_vertex_colors"
        );
    if (!vsh)
        return false;
    pixel_shader_t* psh = sg->get_rsmgr()->get_pixelshader(
        "ps_light/ps_per_pixel_lighting_no_texturing"
        );
    if (!psh)
        return false;

    efpass_.set_vertex_shader(vsh);
    efpass_.set_pixel_shader(psh);

    D3D11_RASTERIZER_DESC raster_desc = {
        D3D11_FILL_SOLID,
        D3D11_CULL_BACK,
        true,
        0,
        0.0f,
        0.0f,
        true,
        false,
        false,
        false
    };

    HRESULT ret_code;
    CHECK_D3D(
        &ret_code,
        sg->get_renderer()->get_device()->CreateRasterizerState(
            &raster_desc, 
            v8::base::scoped_pointer_get_impl(rstate_)));

    if (ret_code != S_OK)
        return false;

    load_mesh_params();

    return true;
}

void simple_mesh::own_draw(scene_graph* sg) {
    renderer* GR = sg->get_renderer();

    GR->IA_Stage_set_vertex_buffers(vbuff_, 1);
    GR->IA_Stage_set_index_buffer(ibuff_);
    GR->IA_Stage_set_input_layout(input_layout_);
    GR->IA_Stage_set_primitive_topology_type(renderer::Topology_Type_TriangleList);

    const auto& world_view_proj_transform = 
        sg->get_scene_cam()->get_camera()->get_projection_wiew_transform()
        * world_transform_.get_transform_matrix();

    efpass_.get_vertex_shader()->set_uniform_by_name(
        "tr_world_view_proj", world_view_proj_transform);

    efpass_.get_vertex_shader()->set_uniform_by_name(
        "tr_world", world_transform_.get_transform_matrix()
        );

    efpass_.get_vertex_shader()->set_uniform_by_name(
        "diffuse", v8::math::color::DarkKhaki
        );
    efpass_.get_vertex_shader()->set_uniform_by_name(
        "specular", v8::math::color::DarkKhaki
        );
    
    v8::math::matrix_4X4F normal_transform;
    local_transform_.compute_inverse(&normal_transform);

    efpass_.get_vertex_shader()->set_uniform_by_name(
        "tr_normal", 
        normal_transform
        );

    pixel_shader_t* psh = efpass_.get_pixel_shader();
    auto light_data = sg->get_active_lights();
    psh->set_raw_uniform_by_name(
        "light_sources", std::get<1>(light_data), 
        std::get<0>(light_data) * sizeof(v8::math::light)
        );
    psh->set_uniform_by_name(
        "eye_pos", sg->get_scene_cam()->get_camera()->get_origin()
        );
    psh->set_uniform_by_name("light_count", std::get<0>(light_data));
    psh->set_uniform_by_name("spec_coeff", 128.0f);

    efpass_.bind_to_pipeline(GR);

    /*GR->get_device_context()->RSSetState(v8::base::scoped_pointer_get(rstate_));*/
    GR->draw_indexed(ibuff_->get_element_count());
    /*GR->get_device_context()->RSSetState(nullptr);*/
}