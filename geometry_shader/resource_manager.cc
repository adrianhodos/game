#include "pch_hdr.h"

#include <v8/base/count_of.h>
#include <v8/base/crt_handle_policies.h>
#include <v8/base/debug_helpers.h>
#include <v8/base/scoped_handle.h>
#include <v8/math/color.h>
#include <v8/math/vector3.h>

#include "app_specific.h"
#include "directx_effect_pass.h"
#include "directx_utils.h"
#include "ifs_loader.h"
#include "renderer.h"
#include "vertex_formats.h"
#include "wavefront_loader.h"

#include "resource_manager.h"

namespace {

void vertex3F_to_vector3F(
    const tools::mesh_loaders::vertex3F& vert,
    v8::math::vector3F* vec
    ) {
    vec->x_ = vert.x_;
    vec->y_ = vert.y_;
    vec->z_ = vert.z_ * -1.0f;
}

class mesh_pnds_builder {
public :
    const tools::mesh_loaders::wavefront_loader*    ldr_;
    std::vector<vertex_PN>                          vertices_;
    std::vector<uint32_t>                           indices_;

    void extract_vertices_from_face(
        const tools::mesh_loaders::obj_face_t& face,
        uint32_t start_vertex,
        uint32_t end_vertex,
        vertex_PN* vert_data       
        );

    void insert_indices() {
        uint32_t vertex_base = vertices_.size();
        const uint32_t new_indices[6] = {
            vertex_base, vertex_base + 1, vertex_base + 2,
            vertex_base, vertex_base + 2, vertex_base + 3
        };
        std::copy(std::begin(new_indices), std::end(new_indices), 
                  std::back_inserter(indices_));
    }

public :
    mesh_pnds_builder(tools::mesh_loaders::wavefront_loader* ldr)
        : ldr_(ldr) {
            vertices_.reserve(ldr_->vertices_.size());
            indices_.reserve(ldr_->vertices_.size());
    }

    void visit_face(const tools::mesh_loaders::obj_face_t& f);

    void visit_quad(
        const tools::mesh_loaders::obj_face_t& f0,
        const tools::mesh_loaders::obj_face_t& f1
        );

    void visit_quad(
        const tools::mesh_loaders::obj_face_t& quad
        );
};

void mesh_pnds_builder::visit_face(
    const tools::mesh_loaders::obj_face_t& f
    ) {
    uint32_t offset = vertices_.size();
    for (int32_t i = 0; i < f.element_count(); ++i) {
        vertex_PN v;
        auto vx_id = f.get_vertex(i);
        auto nm_id = f.get_normal(i);
        vertex3F_to_vector3F(ldr_->vertices_[vx_id], &v.vt_pos);
        vertex3F_to_vector3F(ldr_->normals_[nm_id], &v.vt_norm);
        vertices_.push_back(v);
    }
    int32_t i = 3;
    uint32_t indices[6] = { offset, offset + 1, offset + 2 };
    if (f.element_count() == 4) {
        i = 6;
        indices[3] = offset;
        indices[4] = offset + 2;
        indices[5] = offset + 3;
    }

    using namespace std;
    reverse_copy(begin(indices), begin(indices) + i, back_inserter(indices_));
}

void mesh_pnds_builder::extract_vertices_from_face(
    const tools::mesh_loaders::obj_face_t& face,
        uint32_t start_vertex,
        uint32_t end_vertex,
        vertex_PN* vert_data
    ) {
    assert(end_vertex <= static_cast<uint32_t>(face.element_count()));
    uint32_t i = start_vertex;
    while (i != end_vertex) {
        auto vx_id = face.get_vertex(i);
        auto nm_id = face.get_normal(i);
        vertex3F_to_vector3F(ldr_->vertices_[vx_id], &vert_data->vt_pos);
        vertex3F_to_vector3F(ldr_->normals_[nm_id], &vert_data->vt_norm);
        ++i;
        ++vert_data;
    }
}

void mesh_pnds_builder::visit_quad(
    const tools::mesh_loaders::obj_face_t& f0,
    const tools::mesh_loaders::obj_face_t& f1
    ) {
    vertex_PN new_vertices[4];
    extract_vertices_from_face(f0, 0, 3, new_vertices);
    uint32_t start_vertex = 0;
    uint32_t end_vertex = 1;
    if (f0.get_vertex(0) == f1.get_vertex(0)) {
        start_vertex = 2;
        end_vertex = 3;
    }
    extract_vertices_from_face(f1, start_vertex, end_vertex, new_vertices + 3);
    insert_indices();
    std::copy(std::begin(new_vertices), std::end(new_vertices), 
              std::back_inserter(vertices_));
    /*std::copy(std::begin(f0.indices), std::begin(f0.indices) + 3, 
              std::back_inserter(indices_));
    std::copy(std::begin(f1.indices), std::begin(f1.indices) + 3,
              std::back_inserter(indices_));*/
}

void mesh_pnds_builder::visit_quad(
    const tools::mesh_loaders::obj_face_t& quad
    ) {
    vertex_PN new_vertices[4];
    extract_vertices_from_face(quad, 0, 4, new_vertices);
    insert_indices();
    std::copy(std::begin(new_vertices), std::end(new_vertices), 
              std::back_inserter(vertices_));
}

}

resource_manager::resource_manager() 
    : renderer_(nullptr), default_mtl_("default", nullptr, 1.0f) {}

resource_manager::~resource_manager() {}

bool resource_manager::initialize(renderer* rd) {
    assert(!renderer_);
    renderer_ = rd;

    default_mtl_ = load_and_cache_material("default");
    if (!default_mtl_.mt_map) {
        OUTPUT_DBG_MSGA("Failed to load default material!");
        return false;
    }
    return true;
}

template<typename FWDIter, typename Skip_Predicate>
FWDIter skip_sequence(FWDIter itr, Skip_Predicate pr__) {
    while (pr__(*itr))
        ++itr;

    return itr;
}

bool resource_manager::load_shader_resource_description(
    const char* name,
    resource_manager::shader_descriptor_t* shd
    ) {

    char file_name[MAX_PATH + 1];
    const char* p = name;
    uint32_t i = 0;
    while (*p && *p != '/') {
        file_name[i++] = *p++;
    }
    file_name[i] = '\0';
    if (!*p || !(*(++p)))
        return false;

    std::string file_path;
    string_vprintf(&file_path, "%s\\%s\\%s_desc.json", app_dirs::kAppAssetsDir, 
                   app_dirs::kShaderDescSubdir, file_name);

    using namespace v8::base;
    scoped_handle<crt_file_handle> fp(fopen(file_path.c_str(), "r"));
    if (!fp) {
        OUTPUT_DBG_MSGA("Failed to open file %s", file_path.c_str());
        return false;
    }

    using namespace rapidjson;
    char rbuff[65535];
    rapidjson::FileReadStream frs(scoped_handle_get(fp), rbuff, sizeof(rbuff));
    Document config_file;
    if (config_file.ParseStream<0, Document::EncodingType>(
        frs).HasParseError()) {
        OUTPUT_DBG_MSGA("File %s, parse error %s", file_path.c_str(), 
                        config_file.GetParseError());
        return false;
    }

    assert(config_file.HasMember(p));
    const Value& shader_entry = config_file[p];
    
    shd->compile_flags = shader_entry["compile_flags"].GetUint();
    shd->entry_pt = shader_entry["entry_pt"].GetString();
    shd->source_file = shader_entry["source_file"].GetString();
    shd->profile = shader_entry["profile"].GetInt();
    shd->shader_type = shader_entry["shader_type"].GetInt();
    return true;
}

mesh_info_t resource_manager::get_mesh(const char* name) {
    using namespace std;
    using namespace v8::base;
    auto itr_mesh = meshes_.find(name);
    mesh_info_t mesh_data;

    if (itr_mesh != end(meshes_)) {
        mesh_data.vbuff = scoped_pointer_get(itr_mesh->second.vbuff_);
        mesh_data.ibuff = scoped_pointer_get(itr_mesh->second.ibuff_);
        return mesh_data;
    }
    
    std::string mesh_file_path;
    string_vprintf(&mesh_file_path, "%s\\%s\\%s.*", app_dirs::kAppAssetsDir, 
                   app_dirs::kMeshesDir, name);
    file_info_t fi;
    if (!find_file(mesh_file_path, &fi) || fi.fi_file_name.size() < 5) {
        return mesh_data;
    }

    string_vprintf(&mesh_file_path, "%s\\%s\\%s", app_dirs::kAppAssetsDir, 
                   app_dirs::kMeshesDir, fi.fi_file_name.c_str());

    if (mesh_file_path.rfind(".obj") != std::string::npos) {
        //
        // load obj format mesh
        return load_and_cache_obj_mesh(name, mesh_file_path);
    } else if (mesh_file_path.rfind(".ifs") != std::string::npos) {
        //
        // load ifs
        return load_and_cache_ifs_mesh(name, mesh_file_path);
    } else {
        //
        // 
        return mesh_data;
    } 
}

mesh_info_t resource_manager::create_mesh_entry(
    const char* mesh_name,
    const vertex_PN* vertices, 
    uint32_t vertices_count,
    const uint32_t* indices,
    uint32_t indices_count
    ) {

    mesh_info_t mesh_data;

    scoped_vertexbuffer_t vxbuff(new vertex_buffer(
        renderer_, sizeof(vertex_PN), vertices_count, vertices));

    if (!(*vxbuff))
        return mesh_data;

    scoped_ibuff32_t idxbuff(new index_buffer32_t(
        renderer_, indices_count, indices)); 
    if (!(*idxbuff))
        return mesh_data;

    auto insert_result = meshes_.insert(
        cached_mesh_t(mesh_name, scoped_meshdata_t(std::move(vxbuff), 
                                                   std::move(idxbuff))));
    assert(insert_result.second == true);
    mesh_data.vbuff = scoped_pointer_get(insert_result.first->second.vbuff_);
    mesh_data.ibuff = scoped_pointer_get(insert_result.first->second.ibuff_);
    return mesh_data;
}

mesh_info_t resource_manager::load_and_cache_obj_mesh(
    const char* mesh_name,
    const std::string& mesh_file_path
    ) {

    mesh_info_t mesh_data;
    tools::mesh_loaders::wavefront_loader mesh_loader;
    mesh_loader.parse_file(mesh_file_path.c_str());
    if (mesh_loader.has_parse_error()) {
        return mesh_data;
    }

    mesh_pnds_builder mesh_builder(&mesh_loader);
    mesh_loader.visit_faces(mesh_builder);

    return create_mesh_entry(
        mesh_name, &mesh_builder.vertices_[0], mesh_builder.vertices_.size(),
        &mesh_builder.indices_[0], mesh_builder.indices_.size());
}

mesh_info_t resource_manager::load_and_cache_ifs_mesh(
    const char* mesh_name,
    const std::string& mesh_file_path
    ) {
    mesh_info_t mesh_data;
    tools::mesh_loaders::ifs_loader mesh_loader;
    if (!mesh_loader.loadModel(mesh_file_path.c_str())) {
        return mesh_data;
    }

    return create_mesh_entry(
        mesh_name, &mesh_loader.vertexData_[0], mesh_loader.vertexData_.size(),
        &mesh_loader.indexData_[0], mesh_loader.indexData_.size());
}

input_layout* resource_manager::get_inputlayout(const char* name) {
    auto itr_layoutdata = layouts_.find(name);
    if (itr_layoutdata != std::end(layouts_)) {
        return v8::base::scoped_pointer_get(itr_layoutdata->second);
    }

    v8::base::scoped_ptr<input_layout> new_layout(new input_layout(name));
    if (!new_layout->initialize(renderer_))
        return nullptr;

    auto ins_iter = layouts_.insert(
        cached_layout_data_t(name, std::move(new_layout)));
    return v8::base::scoped_pointer_get(ins_iter.first->second);
}

material_t resource_manager::get_material(
    const std::string& mat_name
    ) {
    auto itr_mat = mtl_cache_.find(mat_name);
    if (itr_mat != std::end(mtl_cache_)) {
        return material_t(mat_name.c_str(), itr_mat->second.mtl_map,
                          itr_mat->second.mtl_specpow);
    }

    return load_and_cache_material(mat_name);
}

bool load_json_file_contents(const std::string& file_path, std::string* dst_str) {
    dst_str->clear();
    using namespace v8::base;
    scoped_handle<crt_file_handle> fp(fopen(file_path.c_str(), "r"));
    if (!fp)
        return false;

    char tmp_buff[1024];
    while (fgets(tmp_buff, count_of_array(tmp_buff), scoped_handle_get(fp)))
        dst_str->append(tmp_buff);

    return true;
}

material_t resource_manager::load_and_cache_material(
    const std::string& mat_name
    ) {

    std::string mtl_desc_file;
    string_vprintf(&mtl_desc_file, "%s\\%s\\%s.json", app_dirs::kAppAssetsDir, 
                   app_dirs::kMtlDescSubdir, mat_name.c_str());

    std::string file_contents;
    if (!load_json_file_contents(mtl_desc_file, &file_contents))
        return default_mtl_;

    using namespace rapidjson;
    Document cfg_file;
    if (cfg_file.Parse<0>(file_contents.c_str()).HasParseError()) {
        OUTPUT_DBG_MSGA("Material description file [%s] parsing error [%s]",
            mtl_desc_file.c_str(), cfg_file.GetParseError());
        return default_mtl_;
    }

    std::string mtl_map = cfg_file["map"].GetString();
    ID3D11ShaderResourceView* tx_diffuse = get_texture2D(mtl_map);
    if (!tx_diffuse)
        return default_mtl_;

    float spec_pow = static_cast<float>(cfg_file["spec_pow"].GetDouble());

    mtl_entry_t new_mat_entry(tx_diffuse, spec_pow);
    mtl_cache_.insert(std::make_pair(mat_name, new_mat_entry));
    return material_t(mat_name.c_str(), tx_diffuse, spec_pow);
}

ID3D11ShaderResourceView* resource_manager::get_texture2D(
    const std::string& tex_name
    ) {
    auto itr_tex = tex2D_cache_.find(tex_name);
    if (itr_tex != std::end(tex2D_cache_))
        return v8::base::scoped_pointer_get(itr_tex->second);

    std::string tex_file_path;
    string_vprintf(&tex_file_path, "%s\\%s\\%s", app_dirs::kAppAssetsDir, 
                   app_dirs::kTexturesDir, tex_name.c_str());

    ID3D11ShaderResourceView* rsrc_view = nullptr;
    HRESULT ret_code;
    CHECK_D3D(
        &ret_code,
        D3DX11CreateShaderResourceViewFromFileA(
            renderer_->get_device(), tex_file_path.c_str(), nullptr, nullptr,
            &rsrc_view, nullptr));

    if (FAILED(ret_code))
        return nullptr;
    tex2D_cache_.insert(cached_tex2D_t(tex_name, scoped_tex2D_t(rsrc_view)));
    return rsrc_view;
}

ID3D11RasterizerState* resource_manager::get_raster_state(
    const raster_state_info_t& rstate_info
    ) {
    auto itr_rs = rsstate_tbl_.find(rstate_info);
    if (itr_rs != std::end(rsstate_tbl_))
        return v8::base::scoped_pointer_get(itr_rs->second);

    const D3D11_FILL_MODE fill_mode[] = { D3D11_FILL_SOLID, D3D11_FILL_WIREFRAME };
    const D3D11_CULL_MODE cull_mode[] = { D3D11_CULL_NONE, D3D11_CULL_FRONT, D3D11_CULL_BACK };

    D3D11_RASTERIZER_DESC rs_desc = {
        fill_mode[rstate_info.rsi_fill_mode],
        cull_mode[rstate_info.rsi_cull_mode],
        rstate_info.rsi_front_face_is_ccw,
        rstate_info.rsi_depth_bias,
        rstate_info.rsi_depth_bias_clamp,
        rstate_info.rsi_slope_bias,
        rstate_info.rsi_depth_clip,
        rstate_info.rsi_scissor,
        rstate_info.rsi_multisample,
        rstate_info.rsi_antialias
    };

    HRESULT ret_code;
    ID3D11RasterizerState* r_state = nullptr;
    CHECK_D3D(
        &ret_code,
        renderer_->get_device()->CreateRasterizerState(&rs_desc, &r_state));
    if (FAILED(ret_code))
        return nullptr;

    rsstate_tbl_.insert(cached_rsstate_t(rstate_info, scoped_rsstate_t(r_state)));
    return r_state;    
}

ID3D11SamplerState* resource_manager::get_sampler_state(
    const sampler_descriptor_t& sstate_desc
    ) {
    auto itr_tbl = sstate_tbl_.find(sstate_desc);
    if (itr_tbl != std::end(sstate_tbl_))
        return v8::base::scoped_pointer_get(itr_tbl->second);

    const D3D11_FILTER filtering_modes[] = {
        D3D11_FILTER_MIN_MAG_MIP_POINT,
        D3D11_FILTER_MIN_MAG_POINT_MIP_LINEAR,
        D3D11_FILTER_MIN_POINT_MAG_LINEAR_MIP_POINT,
        D3D11_FILTER_MIN_POINT_MAG_MIP_LINEAR,
        D3D11_FILTER_MIN_LINEAR_MAG_MIP_POINT,
        D3D11_FILTER_MIN_LINEAR_MAG_POINT_MIP_LINEAR,
        D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT,
        D3D11_FILTER_MIN_MAG_MIP_LINEAR,
        D3D11_FILTER_ANISOTROPIC,
        D3D11_FILTER_COMPARISON_MIN_MAG_MIP_POINT,
        D3D11_FILTER_COMPARISON_MIN_MAG_POINT_MIP_LINEAR,
        D3D11_FILTER_COMPARISON_MIN_POINT_MAG_LINEAR_MIP_POINT,
        D3D11_FILTER_COMPARISON_MIN_POINT_MAG_MIP_LINEAR,
        D3D11_FILTER_COMPARISON_MIN_LINEAR_MAG_MIP_POINT,
        D3D11_FILTER_COMPARISON_MIN_LINEAR_MAG_POINT_MIP_LINEAR,
        D3D11_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT,
        D3D11_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR,
        D3D11_FILTER_COMPARISON_ANISOTROPIC
    };

    const D3D11_TEXTURE_ADDRESS_MODE address_modes[] = {
        D3D11_TEXTURE_ADDRESS_WRAP,
        D3D11_TEXTURE_ADDRESS_MIRROR,
        D3D11_TEXTURE_ADDRESS_CLAMP,
        D3D11_TEXTURE_ADDRESS_BORDER,
        D3D11_TEXTURE_ADDRESS_MIRROR_ONCE 
    };

    const D3D11_COMPARISON_FUNC comp_funcs[] = {
        D3D11_COMPARISON_NEVER,
        D3D11_COMPARISON_LESS,
        D3D11_COMPARISON_EQUAL,
        D3D11_COMPARISON_LESS_EQUAL,
        D3D11_COMPARISON_GREATER,
        D3D11_COMPARISON_NOT_EQUAL,
        D3D11_COMPARISON_GREATER_EQUAL,
        D3D11_COMPARISON_ALWAYS
    };

    const D3D11_SAMPLER_DESC sampler_state_desc = {
        filtering_modes[sstate_desc.sdt_filter_type],
        address_modes[sstate_desc.sdt_texture_address_u],
        address_modes[sstate_desc.sdt_texture_address_v],
        address_modes[sstate_desc.sdt_texture_address_w],
        sstate_desc.sdt_mip_lod_bias,
        sstate_desc.sdt_max_anisotropy_level,
        comp_funcs[sstate_desc.sdt_compare_func],
        { 
            sstate_desc.sdt_border_color.r_, sstate_desc.sdt_border_color.g_,
            sstate_desc.sdt_border_color.b_, sstate_desc.sdt_border_color.a_
        },
        sstate_desc.sdt_min_lod,
        sstate_desc.sdt_max_lod
    };

    ID3D11SamplerState* sstate = nullptr;
    HRESULT ret_code;
    CHECK_D3D(
        &ret_code,
        renderer_->get_device()->CreateSamplerState(&sampler_state_desc, &sstate));
    if (FAILED(ret_code))
        return nullptr;

    sstate_tbl_.insert(cached_samplerstate_t(sstate_desc, sstate));
    return sstate;
}

effect_pass* resource_manager::get_effect(
    const std::string& effect_name
    ) {
    auto itr_eff_tbl = effect_tbl_.find(effect_name);
    if (itr_eff_tbl != std::end(effect_tbl_))
        return v8::base::scoped_pointer_get(itr_eff_tbl->second);

    vertex_shader_t*    vsh = nullptr;
    geometry_shader_t*  gsh = nullptr;
    pixel_shader_t*     psh = nullptr;

    std::string cfg_file_contents;
    std::string cfg_file_path;
    string_vprintf(
        &cfg_file_path, "%s\\%s\\%s.json", app_dirs::kAppAssetsDir,
        app_dirs::kEffectDescSubdir, effect_name.c_str());
    if (!load_json_file_contents(cfg_file_path, &cfg_file_contents))
        return nullptr;

    using namespace rapidjson;
    Document cfg_file;
    if (cfg_file.Parse<0>(cfg_file_contents.c_str()).HasParseError()) {
        OUTPUT_DBG_MSGA("Failed to parse effect config file [%s], error [%s]",
            cfg_file_path.c_str(), cfg_file.GetParseError());
        return nullptr;
    }

    assert(cfg_file.HasMember("vertex_shader"));
    assert(cfg_file.HasMember("pixel_shader"));
    vsh = get_vertexshader(cfg_file["vertex_shader"].GetString());
    psh = get_pixelshader(cfg_file["pixel_shader"].GetString());
    if (!vsh || !psh) {
        return nullptr;
    }

    if (cfg_file.HasMember("geometry_shader")) {
        gsh = get_geometryshader(cfg_file["geometry_shader"].GetString());
        if (!gsh)
            return nullptr;
    }

    scoped_effect_t new_effect(new effect_pass(vsh, psh, gsh));
    auto itr_ins_value = effect_tbl_.insert(cached_effect_t(effect_name, std::move(new_effect)));
    return v8::base::scoped_pointer_get(itr_ins_value.first->second);
}