#pragma once

enum Mesh_Type {
    Mesh_Default,
    Mesh_Procedural_Water,
    Mesh_Procedural_Terrain
};