#pragma once

#include <D3D11.h>
#include <cstdint>
#include <v8/base/string_util.h>

const uint32_t kMaxMatName = 64;

struct material_t {
    char                            mt_name[kMaxMatName];
    ID3D11ShaderResourceView*       mt_map;
    float                           mt_spec_pow;

    material_t(
        const char* name, 
        ID3D11ShaderResourceView* map,
        float spec_pow
        ) : mt_map(map), mt_spec_pow(spec_pow) {
            v8::base::snprintf(mt_name, kMaxMatName, "%s", name);
    }
};

inline bool operator<(const material_t& left, const material_t& right) {
    return strcmp(left.mt_name, right.mt_name) < 0;
}

inline bool operator==(const material_t& left, const material_t& right) {
    return strcmp(left.mt_name, right.mt_name) == 0;
}

inline bool operator!=(const material_t& left, const material_t& right) {
    return !(left == right);
}