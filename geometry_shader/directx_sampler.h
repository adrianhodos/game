#pragma once

#include <d3d11.h>
#include <string>
#include <v8/base/scoped_pointer.h>

class directx_renderer;
struct sampler_descriptor_t;

class directx_sampler {
private :
    std::string smp_name_;
    v8::base::scoped_ptr<ID3D11SamplerState, v8::base::com_storage> smp_handle_;
public :
    typedef ID3D11SamplerState* sampler_handle_t;

    directx_sampler(const char* name) : smp_name_(name) {}

    bool initialize(const directx_renderer* R, const sampler_descriptor_t& smp_desc);

    sampler_handle_t get_handle() const {
        return v8::base::scoped_pointer_get(smp_handle_);
    }

    const std::string& get_name() const {
        return smp_name_;
    }
};

typedef directx_sampler sampler_t;

inline bool operator==(const directx_sampler& left, const directx_sampler& right) {
    return left.get_name() == right.get_name();
}

inline bool operator!=(const directx_sampler& left, const directx_sampler& right) {
    return !(left == right);
}