#include "pch_hdr.h"
#include "directx_shader.h"
#include "directx_renderer.h"
#include "effect_pass.h"

effect_pass::effect_pass( 
    vertex_shader_t* vxshader /* = nullptr */, 
    pixel_shader_t* pxshader /* = nullptr */, 
    geometry_shader_t* gsshader /* = nullptr */ 
    ) : vertexshader_(vxshader), geometryshader_(gsshader), 
        fragmentshader_(pxshader) {
}

void effect_pass::bind_to_pipeline(
    const renderer* R
    ) {
    if (vertexshader_)
        vertexshader_->bind_to_pipeline(R);

    if (geometryshader_)
        geometryshader_->bind_to_pipeline(R);
    else
        R->GS_Stage_clear_shader();

    if (fragmentshader_)
        fragmentshader_->bind_to_pipeline(R);

}