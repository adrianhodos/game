#pragma once

#include <stdint.h>

#include <v8/base/scoped_pointer.h>
#include <v8/math/vector2.h>

#include "directx_shader.h"
#include "index_buffer.h"
#include "sampler.h"
#include "vertex_buffer.h"
#include "vertex_formats.h"

struct draw_context_t;

//class fluid_surface {
//private :
//    float               timeDelta_;
//    float               timeFactor_;
//    float               waveSpeed_;
//    float               dampingFactor_;
//    float               cellDist_;
//    int                 trisXAxis_;
//    int                 trisZAxis_;  
//    int                 currentBuffer_;
//    size_t              indexCount_;
//    vertex_buffer       vertexBuffer_;
//    index_buffer32_t    indexBuffer_;
//    vertex_shader_t     vtx_shader_;
//    pixel_shader_t      frag_shader_;
//    v8::base::scoped_ptr<ID3D11InputLayout, v8::base::com_storage> fluidInputLayout_;
//    float coefficientOne_;
//    float coefficientTwo_;
//    float coefficientThree_;
//    v8::base::scoped_ptr<vertex_PNT, v8::base::default_array_storage> buffVertices_;
//    vertex_PNT* previousSolution_;
//    vertex_PNT* currentSolution_;
//    bool        freezeTime_;
//    sampler_t   sampler_;
//    v8::base::scoped_ptr<ID3D11ShaderResourceView, v8::base::com_storage>   rsview_fluid_texture_;
//    v8::math::vector2F      tex_offset_;
//
//    void precompute_coefficients();
//
//    void evaluate_surface(const graphics_render_t* R);
//
//    void generate_surface_grid();
//
//    bool load_materials(const graphics_render_t* R);
//
//    bool setup_grid_layout(const graphics_render_t* R);
//
//    bool setup_vertex_index_buffers(const graphics_render_t* R);
//
//public :
//    fluid_surface(
//        float time_factor, 
//        float wave_speed, 
//        float damping_factor,
//        float cell_size,
//        int quads_x_axis,
//        int quads_z_axis
//        );
//
//    bool initialize(const graphics_render_t* R);
//
//    void update(float delta_time, const graphics_render_t* R);
//
//    void disturb(int row, int column, float wave_magnitude);
//
//    void draw(const draw_context_t*, const graphics_render_t*);
//
//    void freeze_surface() { freezeTime_ = true; }
//
//    void unfreeze_surface() { freezeTime_ = false; }
//};